#### *Psalm 90. Domine, refugium*
LORD, thou hast been our refuge<span> * </span>from one generation to another.

<sub>2</sub> Before the mountains were brought forth, or ever the earth and the world were made<span> * </span>thou art God from everlasting, and world without end.

<sub>3</sub> Thou turnest man to destruction<span> * </span>again thou sayest, Come again, ye children of men.

<sub>4</sub> For a thousand years in thy sight are but as yesterday<span> * </span>seeing that is past as a watch in the night.

<sub>5</sub> As soon as thou scatterest them they are even as a sleep<span> * </span>and fade away suddenly like the grass.

<sub>6</sub> In the morning it is green, and groweth up<span> * </span>but in the evening it is cut down, dried up, and withered.

<sub>7</sub> For we consume away in thy displeasure<span> * </span>and are afraid at thy wrathful indignation.

<sub>8</sub> Thou hast set our misdeeds before thee<span> * </span>and our secret sins in the light of thy countenance.

<sub>9</sub> For when thou art angry all our days are gone<span> * </span>we bring our years to an end, as it were a tale that is told.

<sub>10</sub> The days of our age are threescore years and ten; and though men be so strong that they come to fourscore years<span> * </span>yet is their strength then but labour and sorrow; so soon passeth it away, and we are gone.

<sub>11</sub> But who regardeth the power of thy wrath<span> * </span>for even thereafter as a man feareth, so is thy displeasure.

<sub>12</sub> So teach us to number our days<span> * </span>that we may apply our hearts unto wisdom.

<sub>13</sub> Turn thee again, O Lord, at the last<span> * </span>and be gracious unto thy servants.

<sub>14</sub> O satisfy us with thy mercy, and that soon<span> * </span>so shall we rejoice and be glad all the days of our life.

<sub>15</sub> Comfort us again now after the time that thou hast plagued us<span> * </span>and for the years wherein we have suffered adversity.

<sub>16</sub> Shew thy servants thy work<span> * </span>and their children thy glory.

<sub>17</sub> And the glorious majesty of the Lord our God be upon us<span> * </span>prosper thou the work of our hands upon us, O prosper thou our handywork.

#### *Psalm 91. Qui habitat*
WHOSO dwelleth under the defence of the most High<span> * </span>shall abide under the shadow of the Almighty.

<sub>2</sub> I will say unto the Lord, Thou art my hope, and my strong hold<span> * </span>my God, in him will I trust.

<sub>3</sub> For he shall deliver thee from the snare of the hunter<span> * </span>and from the noisome pestilence.

<sub>4</sub> He shall defend thee under his wings, and thou shalt be safe under his feathers<span> * </span>his faithfulness and truth shall be thy shield and buckler.

<sub>5</sub> Thou shalt not be afraid for any terror by night<span> * </span>nor for the arrow that flieth by day;

<sub>6</sub> For the pestilence that walketh in darkness<span> * </span>nor for the sickness that destroyeth in the noon-day.

<sub>7</sub> A thousand shall fall beside thee, and ten thousand at thy right hand<span> * </span>but it shall not come nigh thee.

<sub>8</sub> Yea, with thine eyes shalt thou behold<span> * </span>and see the reward of the ungodly.

<sub>9</sub> For thou, Lord, art my hope<span> * </span>thou hast set thine house of defence very high.

<sub>10</sub> There shall no evil happen unto thee<span> * </span>neither shall any plague come nigh thy dwelling.

<sub>11</sub> For he shall give his angels charge over thee<span> * </span>to keep thee in all thy ways.

<sub>12</sub> They shall bear thee in their hands<span> * </span>that thou hurt not thy foot against a stone.

<sub>13</sub> Thou shalt go upon the lion and adder<span> * </span>the young lion and the dragon shalt thou tread under thy feet.

<sub>14</sub> Because he hath set his love upon me, therefore will I deliver him<span> * </span>I will set him up, because he hath known my Name.

<sub>15</sub> He shall call upon me, and I will hear him<span> * </span>yea, I am with him in trouble; I will deliver him, and bring him to honour.

<sub>16</sub> With long life will I satisfy him<span> * </span>and shew him my salvation.

#### *Psalm 92. Bonum est confiteri*
IT IS a good thing to give thanks unto the Lord<span> * </span>and to sing praises unto thy Name, O most Highest;

<sub>2</sub> To tell of thy loving-kindness early in the morning<span> * </span>and of thy truth in the night-season;

<sub>3</sub> Upon an instrument of ten strings, and upon the lute<span> * </span>upon a loud instrument, and upon the harp.

<sub>4</sub> For thou, Lord, hast made me glad through thy works<span> * </span>and I will rejoice in giving praise for the operations of thy hands.

<sub>5</sub> O Lord, how glorious are thy works<span> * </span>thy thoughts are very deep.

<sub>6</sub> An unwise man doth not well consider this<span> * </span>and a fool doth not understand it.

<sub>7</sub> When the ungodly are green as the grass, and when all the workers of wickedness do flourish<span> * </span>then shall they be destroyed for ever; but thou, Lord, art the most Highest for evermore.

<sub>8</sub> For lo, thine enemies, O Lord, lo, thine enemies shall perish<span> * </span>and all the workers of wickedness shall be destroyed.

<sub>9</sub> But mine horn shall be exalted like the horn of an unicorn<span> * </span>for I am anointed with fresh oil.

<sub>10</sub> Mine eye also shall see his lust of mine enemies<span> * </span>and mine ear shall hear his desire of the wicked that arise up against me.

<sub>11</sub> The righteous shall flourish like a palm-tree<span> * </span>and shall spread abroad like a cedar in Libanus.

<sub>12</sub> Such as are planted in the house of the Lord<span> * </span>shall flourish in the courts of the house of our God.

<sub>13</sub> They also shall bring forth more fruit in their age<span> * </span>and shall be fat and well-liking.

<sub>14</sub> That they may shew how true the Lord my strength is<span> * </span>and that there is no unrighteousness in him.

