#### *Psalm 27. Dominus illuminatio*
THE Lord is my light and my salvation ; whom then shall I fear<span> * </span>the Lord is the strength of my life; of whom then shall I be afraid?

<sub>2</sub> When the wicked, even mine enemies and my foes, came upon me to eat up my flesh<span> * </span>they stumbled and fell.

<sub>3</sub> Though an host of men were laid against me, yet shall not my heart be afraid<span> * </span>and though there rose up war against me, yet will I put my trust in him.

<sub>4</sub> One thing have I desired of the Lord, which I will require<span> * </span>even that I may dwell in the house of the Lord all the days of my life, to behold the fair beauty of the Lord, and to visit his temple.

<sub>5</sub> For in the time of trouble he shall hide me in his tabernacle<span> * </span>yea, in the secret place of his dwelling shall he hide me, and set me up upon a rock of stone.

<sub>6</sub> And now shall he lift up mine head<span> * </span>above mine enemies round about me.

<sub>7</sub> Therefore will I offer in his dwelling an oblation with great gladness<span> * </span>I will sing, and speak praises unto the Lord.

<sub>8</sub> Hearken unto my voice, O Lord, when I cry unto thee<span> * </span>have mercy upon me, and hear me.

<sub>9</sub> My heart hath talked of thee, Seek ye my face<span> * </span>Thy face, Lord, will I seek.

<sub>10</sub> O hide not thou thy face from me<span> * </span>nor cast thy servant away in displeasure.

<sub>11</sub> Thou hast been my succour<span> * </span>leave me not, neither forsake me, O God of my salvation.

<sub>12</sub> When my father and my mother forsake me<span> * </span>the Lord taketh me up.

<sub>13</sub> Teach me thy way, O Lord<span> * </span>and lead me in the right way, because of mine enemies.

<sub>14</sub> Deliver me not over into the will of mine adversaries<span> * </span>for there are false witnesses risen up against me, and such as speak wrong.

<sub>15</sub> I should utterly have fainted<span> * </span>but that I believe verily to see the goodness of the Lord in the land of the living.

<sub>16</sub> O tarry thou the Lord's leisure<span> * </span>be strong, and he shall comfort thine heart ; and put thou thy trust in the Lord.

#### *Psalm 28. Ad te, Domine*
UNTO thee will I cry, O Lord my strength<span> * </span>think no scorn of me; lest, if thou make as though thou hearest not, I become like them that go down into the pit.

<sub>2</sub> Hear the voice of my humble petitions, when I cry unto thee<span> * </span>when I hold up my hands towards the mercy-seat of thy holy temple.

<sub>3</sub> O pluck me not away, neither destroy me, with the ungodly and wicked doers<span> * </span>which speak friendly to their neighbours, but imagine mischief in their hearts.

<sub>4</sub> Reward them according to their deeds<span> * </span>and according to the wickedness of their own inventions.

<sub>5</sub> Recompense them after the work of their hands<span> * </span>pay them that they have deserved.

<sub>6</sub> For they regard not in their mind the works of the Lord, nor the operation of his hands<span> * </span>therefore shall he break them down, and not build them up.

<sub>7</sub> Praised be the Lord<span> * </span>for he hath heard the voice of my humble petitions.

#### *8. The Lord is my strength and my shield; my heart hath trusted in him, and I am helped: therefore my heart danceth for joy, and in my song will I praise him.*
<sub>9</sub> The Lord is my strength<span> * </span>and he is the wholesome defence of his Anointed.

<sub>10</sub> O save thy people, and give thy blessing unto thine inheritance<span> * </span>feed them , and set them up for ever.

#### *Psalm 29. Afferte Domino*
BRING unto the Lord, O ye mighty, bring young rams unto the Lord<span> * </span>ascribe unto the Lord worship and strength.

<sub>2</sub> Give the Lord the honour due unto his Name<span> * </span>worship the Lord with holy worship.

<sub>3</sub> It is the Lord that commandeth the waters<span> * </span>it is the glorious God that maketh the thunder.

<sub>4</sub> It is the Lord that ruleth the sea; the voice of the Lord is mighty in operation<span> * </span>the voice of the Lord is a glorious voice.

<sub>5</sub> The voice of the Lord breaketh the cedar-trees<span> * </span>yea, the Lord breaketh the cedars of Libanus.

<sub>6</sub> He maketh them also to skip like a calf<span> * </span>Libanus also, and Sirion, like a young unicorn.

<sub>7</sub> The voice of the Lord divideth the flames of fire; the voice of the Lord shaketh the wilderness<span> * </span>yea, the Lord shaketh the wilderness of Cades.

<sub>8</sub> The voice of the Lord maketh the hinds to bring forth young, and discovereth the thick bushes<span> * </span>in his temple doth every man speak of his honour.

<sub>9</sub> The Lord sitteth above the water-flood<span> * </span>and the Lord remaineth a King for ever.

<sub>10</sub> The Lord shall give strength unto his people<span> * </span>the Lord shall give his people the blessing of peace.

