#### *Psalm 6. Domine, ne in furore*
OLORD, rebuke me not in thine indignation<span> * </span>neither chasten me in thy displeasure.

<sub>2</sub> Have mercy upon me, O Lord, for I am weak<span> * </span>O Lord, heal me, for my bones are vexed.

<sub>3</sub> My soul also is sore troubled<span> * </span>but, Lord, how long wilt thou punish me?

<sub>4</sub> Turn thee, O Lord, and deliver my soul<span> * </span>O save me for thy mercy's sake.

<sub>5</sub> For in death no man remembereth thee<span> * </span>and who will give thee thanks in the pit?

<sub>6</sub> I am weary of my groaning; every night wash I my bed<span> * </span>and water my couch with my tears.

<sub>7</sub> My beauty is gone for very trouble<span> * </span>and worn away because of all mine enemies.

<sub>8</sub> Away from me, all ye that work vanity<span> * </span>for the Lord hath heard the voice of my weeping.

<sub>9</sub> The Lord hath heard my petition<span> * </span>the Lord will receive my prayer.

<sub>10</sub> All mine enemies shall be confounded, and sore vexed<span> * </span>they shall be turned back, and put to shame suddenly.

#### *Psalm 7. Domine, Deus meus*
OLORD my God, in thee have I put my trust<span> * </span>save me from all them that persecute me, and deliver me;

<sub>2</sub> Lest he devour my soul, like a lion, and tear it in pieces<span> * </span>while there is none to help.

<sub>3</sub> O Lord my God, if I have done any such thing<span> * </span>or if there be any wickedness in my hands;

<sub>4</sub> If I have rewarded evil unto him that dealt friendly with me<span> * </span>yea, I have delivered him that without any cause is mine enemy,

<sub>5</sub> Then let mine enemy persecute my soul, and take me<span> * </span>yea, let him tread my life down upon the earth, and lay mine honour in the dust.

<sub>6</sub> Stand up, O Lord, in thy wrath, and lift up thyself, because of the indignation of mine enemies<span> * </span>arise up for me in the judgement that thou hast commanded.

<sub>7</sub> And so shall the congregation of the people come about thee<span> * </span>for their sakes therefore lift up thyself again.

<sub>8</sub> The Lord shall judge the people; give sentence with me, O Lord<span> * </span>according to my righteousness, and according to the innocency that is in me.

<sub>9</sub> O let the wickedness of the ungodly come to an end<span> * </span>but guide thou the just.

<sub>10</sub> For the righteous God<span> * </span>trieth the very hearts and reins.

<sub>11</sub> My help cometh of God<span> * </span>who preserveth them that are true of heart.

<sub>12</sub> God is a righteous Judge, strong and patient<span> * </span>and God is provoked every day.

<sub>13</sub> If a man will not turn, he will whet his sword<span> * </span>he hath bent his bow, and made it ready.

<sub>14</sub> He hath prepared for him the instruments of death<span> * </span>he ordaineth his arrows against the persecutors

<sub>15</sub> Behold, he travaileth with mischief<span> * </span>he hath conceived sorrow, and brought forth ungodliness.

<sub>16</sub> He hath graven and digged up a pit<span> * </span>and is fallen on himself into the destruction that he made for other.

<sub>17</sub> For his travail shall come upon his own head<span> * </span>and his wickedness shall fall on his own pate.

<sub>18</sub> I will give thanks unto the Lord, according to his righteousness<span> * </span>and I will praise the Name of the Lord most High.

#### *Psalm 8. Domine, Dominus noster*
OLORD our Governor, how excellent is thy Name in all the world<span> * </span>thou that hast set thy glory above the heavens!

<sub>2</sub> Out of the mouth of very babes and sucklings hast thou ordained strength, because of thine enemies<span> * </span>that thou mightest still the enemy and the avenger.

<sub>3</sub> For I will consider thy heavens, even the works of thy fingers<span> * </span>the moon and the stars, which thou hast ordained.

<sub>4</sub> What is man, that thou art mindful of him<span> * </span>and the son of man, that thou visitest him?

<sub>5</sub> Thou madest him lower than the angels<span> * </span>to crown him with glory and worship.

<sub>6</sub> Thou makest him to have dominion of the works of thy hands<span> * </span>and thou hast put all things in subjection under his feet;

<sub>7</sub> All sheep and oxen<span> * </span>yea, and the beasts of the field;

<sub>8</sub> The fowls of the air, and the fishes of the sea<span> * </span>and whatsoever walketh through the paths of the seas.

<sub>9</sub> O Lord our Governor<span> * </span>how excellent is thy Name in all the world!

