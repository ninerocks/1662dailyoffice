#### *Psalm 1. Beatus vir, qui non abiit, &c.*
BLESSED is the man that hath not walked in the counsel of the ungodly, nor stood in the way of sinners<span> * </span>and hath not sat in the seat of the scornful.

<sub>2</sub> But his delight is in the law of the Lord<span> * </span>and in his law will he exercise himself day and night.

<sub>3</sub> And he shall be like a tree planted by the water-side<span> * </span>that will bring forth his fruit in due season.

<sub>4</sub> His leaf also shall not wither<span> * </span>and look, whatsoever he doeth, it shall prosper.

<sub>5</sub> As for the ungodly, it is not so with them<span> * </span>but they are like the chaff, which the wind scattereth away from the face of the earth.

<sub>6</sub> Therefore the ungodly shall not be able to stand in the judgement<span> * </span>neither the sinners in the congregation of the righteous.

<sub>7</sub> But the Lord knoweth the way of the righteous<span> * </span>and the way of the ungodly shall perish.

#### *Psalm 2. Quare fremuerunt gentes?*
WHY do the heathen so furiously rage together<span> * </span>and why do the people imagine a vain thing?

<sub>2</sub> The kings of the earth stand up, and the rulers take counsel together<span> * </span>against the Lord, and against his Anointed.

<sub>3</sub> Let us break their bonds asunder<span> * </span>and cast away their cords from us.

<sub>4</sub> He that dwelleth in heaven shall laugh them to scorn<span> * </span>the Lord shall have them in derision.

<sub>5</sub> Then shall he speak unto them in his wrath<span> * </span>and vex them in his sore displeasure.

<sub>6</sub> Yet have I set my King<span> * </span>upon my holy hill of Sion.

<sub>7</sub> I will preach the law, whereof the Lord hath said unto me<span> * </span>Thou art my Son, this day have I begotten thee.

#### *8. Desire of me, and I shall give thee the heathen for thine inheritance: and the utmost parts of the earth for thy possession.*
<sub>9</sub> Thou shalt bruise them with a rod of iron<span> * </span>and break them in pieces like a potter's vessel.

<sub>10</sub> Be wise now therefore, O ye kings<span> * </span>be learned, ye that are judges of the earth.

<sub>11</sub> Serve the Lord in fear<span> * </span>and rejoice unto him with reverence.

<sub>12</sub> Kiss the Son, lest he be angry, and so ye perish from the right way<span> * </span>if his wrath be kindled, (yea, but a little,) blessed are all they that put their trust in him.

#### *Psalm 3. Domine, quid multiplicati,*
LORD, how are they increased that trouble me<span> * </span>many are they that rise against me.

<sub>2</sub> Many one there be that say of my soul<span> * </span>There is no help for him in his God.

<sub>3</sub> But thou, O Lord, art my defender<span> * </span>thou art my worship, and the lifter up of my head.

<sub>4</sub> I did call upon the Lord with my voice<span> * </span>and he heard me out of his holy hill.

<sub>5</sub> I laid me down and slept, and rose up again<span> * </span>for the Lord sustained me.

<sub>6</sub> I will not be afraid for ten thousands of the people<span> * </span>that have set themselves against me round about.

<sub>7</sub> Up, Lord, and help me, O my God<span> * </span>for thou smitest all mine enemies upon the cheekbone; thou hast broken the teeth of the ungodly.

<sub>8</sub> Salvation belongeth unto the Lord<span> * </span>and thy blessing is upon thy people.

#### *Psalm 4. Cum invocarem*
HEAR me when I call, O God of my righteousness<span> * </span>thou hast set me at liberty when I was in trouble; have mercy upon me, and hearken unto my prayer.

<sub>2</sub> O ye sons of men, how long will ye blaspheme mine honour<span> * </span>and have such pleasure in vanity, and seek after leasing?

#### *3. Know this also, that the Lord hath chosen to himself the man that is godly: when I call upon the Lord, he will hear me.*
<sub>4</sub> Stand in awe, and sin not<span> * </span>commune with your own heart, and in your chamber, and be still.

<sub>5</sub> Offer the sacrifice of righteousness<span> * </span>and put your trust in the Lord.

<sub>6</sub> There be many that say<span> * </span>Who will shew us any good?

<sub>7</sub> Lord, lift thou up<span> * </span>the light of thy countenance upon us.

<sub>8</sub> Thou hast put gladness in my heart<span> * </span>since the time that their corn and wine and oil increased.

<sub>9</sub> I will lay me down in peace, and take my rest<span> * </span>for it is thou, Lord, only, that makest me dwell in safety.

#### *Psalm 5. Verba mea auribus.*
PONDER my words, O Lord<span> * </span>consider my meditation

<sub>2</sub> O hearken thou unto the voice of my calling, my King, and my God<span> * </span>for unto thee will I make my prayer.

<sub>3</sub> My voice shalt thou hear betimes, O Lord<span> * </span>early in the morning will I direct my prayer unto thee, and will look up.

<sub>4</sub> For thou art the God that hast no pleasure in wickedness<span> * </span>neither shall any evil dwell with thee.

<sub>5</sub> Such as be foolish shall not stand in thy sight<span> * </span>for thou hatest all them that work vanity.

<sub>6</sub> Thou shalt destroy them that speak leasing<span> * </span>the Lord will abhor both the blood-thirsty and deceitful man.

<sub>7</sub> But as for me, I will come into thine house, even upon the multitude of thy mercy<span> * </span>and in thy fear will I worship toward thy holy temple.

<sub>8</sub> Lead me, O Lord, in thy righteousness, because of mine enemies<span> * </span>make thy way plain before my face.

<sub>9</sub> For there is no faithfulness in his mouth<span> * </span>their inward parts are very wickedness.

<sub>10</sub> Their throat is an open sepulchre<span> * </span>they flatter with their tongue.

<sub>11</sub> Destroy thou them, O God; let them perish through their own imaginations<span> * </span>cast them out in the multitude of their ungodliness; for they have rebelled against thee.

<sub>12</sub> And let all them that put their trust in thee rejoice<span> * </span>they shall ever be giving of thanks, because thou defendest them; they that love thy Name shall be joyful in thee;

<sub>13</sub> For thou, Lord, wilt give thy blessing unto the righteous<span> * </span>and with thy favourable kindness wilt thou defend him as with a shield.

