#### *Psalm 19. Caeli enarrant*
THE heavens declare the glory of God<span> * </span>and the firmament sheweth his handywork.

<sub>2</sub> One day telleth another<span> * </span>and one night certifieth another.

<sub>3</sub> There is neither speech nor language<span> * </span>but their voices are heard among them.

<sub>4</sub> Their sound is gone out into all lands<span> * </span>and their words into the ends of the world.

<sub>5</sub> In them hath he set a tabernacle for the sun<span> * </span>which cometh forth as a bridegroom out of his chamber, and rejoiceth as a giant to run his course.

<sub>6</sub> It goeth forth from the uttermost part of the heaven, and runneth about unto the end of it again<span> * </span>and there is nothing hid from the heat thereof.

<sub>7</sub> The law of the Lord is an undefiled law, converting the soul<span> * </span>the testimony of the Lord is sure, and giveth wisdom unto the simple.

<sub>8</sub> The statutes of the Lord are right, and rejoice the heart<span> * </span>the commandment of the Lord is pure, and giveth light unto the eyes.

<sub>9</sub> The fear of the Lord is clean, and endureth for ever<span> * </span>the judgements of the Lord are true, and righteous altogether.

<sub>10</sub> More to be desired are they than gold, yea, than much fine gold<span> * </span>sweeter also than honey, and the honey-comb.

<sub>11</sub> Moreover, by them is thy servant taught<span> * </span>and in keeping of them there is great reward.

<sub>12</sub> Who can tell how oft he offendeth<span> * </span>O cleanse thou me from my secret faults.

<sub>13</sub> Keep thy servant also from presumptuous sins, lest they get the dominion over me<span> * </span>so shall I be undefiled, and innocent from the great offence.

<sub>14</sub> Let the words of my mouth, and the meditation of my heart<span> * </span>be alway acceptable in thy sight,

<sub>15</sub> O Lord<span> * </span>my strength, and my redeemer.

#### *Psalm 20. Exaudiat te Dominus*
THE Lord hear thee in the day of trouble<span> * </span>the Name of the God of Jacob defend thee;

<sub>2</sub> Send thee help from the sanctuary<span> * </span>and strengthen thee out of Sion;

<sub>3</sub> Remember all thy offerings<span> * </span>and accept thy burnt-sacrifice;

<sub>4</sub> Grant thee thy heart's desire<span> * </span>and fulfil all thy mind.

<sub>5</sub> We will rejoice in thy salvation, and triumph in the Name of the Lord our God<span> * </span>the Lord perform all thy petitions.

<sub>6</sub> Now know I that the Lord helpeth his Anointed, and will hear him from his holy heaven<span> * </span>even with the wholesome strength of his right hand.

<sub>7</sub> Some put their trust in chariots, and some in horses<span> * </span>but we will remember the Name of the Lord our God.

<sub>8</sub> They are brought down, and fallen<span> * </span>but we are risen, and stand upright.

<sub>9</sub> Save, Lord, and hear us, O King of heaven<span> * </span>when we call upon thee.

#### *Psalm 21. Domine, in virtute tua*
THE King shall rejoice in thy strength, O Lord<span> * </span>exceeding glad shall he be of thy salvation.

<sub>2</sub> Thou hast given him his heart's desire<span> * </span>and hast not denied him the request of his lips.

<sub>3</sub> For thou shalt prevent him with the blessings of goodness<span> * </span>and shalt set a crown of pure gold upon his head.

<sub>4</sub> He asked life of thee, and thou gavest him a long life<span> * </span>even for ever and ever.

<sub>5</sub> His honour is great in thy salvation<span> * </span>glory and great worship shalt thou lay upon him.

<sub>6</sub> For thou shalt give him everlasting felicity<span> * </span>and make him glad with the joy of thy countenance.

<sub>7</sub> And why? because the King putteth his trust in the Lord<span> * </span>and in the mercy of the most Highest he shall not miscarry.

<sub>8</sub> All thine enemies shall feel thine hand<span> * </span>thy right hand shall find out them that hate thee.

<sub>9</sub> Thou shalt make them like a fiery oven in time of thy wrath<span> * </span>the Lord shall destroy them in his displeasure, and the fire shall consume them.

<sub>10</sub> Their fruit shalt thou root out of the earth<span> * </span>and their seed from among the children of men.

<sub>11</sub> For they intended mischief against thee<span> * </span>and imagined such a device as they are not able to perform.

<sub>12</sub> Therefore shalt thou put them to flight<span> * </span>and the strings of thy bow shalt thou make ready against the face of them.

<sub>13</sub> Be thou exalted, Lord, in thine own strength<span> * </span>so we will sing, and praise thy power.

