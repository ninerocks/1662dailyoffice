#### *Psalm 95. Venite, exultemus*
OCOME, let us sing unto the Lord<span> * </span>let us heartily rejoice in the strength of our salvation.

<sub>2</sub> Let us come before his presence with thanksgiving<span> * </span>and shew ourselves glad in him with psalms.

<sub>3</sub> For the Lord is a great God<span> * </span>and a great King above all gods.

<sub>4</sub> In his hand are all the corners of the earth<span> * </span>and the strength of the hills is his also.

<sub>5</sub> The sea is his, and he made it<span> * </span>and his hands prepared the dry land.

<sub>6</sub> O come, let us worship and fall down<span> * </span>and kneel before the Lord our Maker.

<sub>7</sub> For he is the Lord our God<span> * </span>and we are the people of his pasture, and the sheep of his hand.

<sub>8</sub> To-day if ye will hear his voice, harden not your hearts<span> * </span>as in the provocation, and as in the day of temptation in the wilderness.

<sub>9</sub> When your fathers tempted me<span> * </span>proved me, and saw my works.

<sub>10</sub> Forty years long was I grieved with this generation, and said<span> * </span>It is a people that do err in their hearts, for they have not known my ways;

<sub>11</sub> Unto whom I sware in my wrath<span> * </span>that they should not enter into my rest.

#### *Psalm 96. Cantate Domino*
OSING unto the Lord a new song<span> * </span>sing unto the Lord, all the whole earth.

<sub>2</sub> Sing unto the Lord, and praise his Name<span> * </span>be telling of his salvation from day to day.

<sub>3</sub> Declare his honour unto the heathen<span> * </span>and his wonders unto all people.

<sub>4</sub> For the Lord is great, and cannot worthily be praised<span> * </span>he is more to be feared than all gods.

<sub>5</sub> As for all the gods of the heathen, they are but idols<span> * </span>but it is the Lord that made the heavens.

<sub>6</sub> Glory and worship are before him<span> * </span>power and honour are in his sanctuary.

<sub>7</sub> Ascribe unto the Lord, O ye kindreds of the people<span> * </span>ascribe unto the Lord worship and power.

<sub>8</sub> Ascribe unto the Lord the honour due unto his Name<span> * </span>bring presents, and come into his courts.

<sub>9</sub> O worship the Lord in the beauty of holiness<span> * </span>let the whole earth stand in awe of him.

<sub>10</sub> Tell it out among the heathen that the Lord is King<span> * </span>and that it is he who hath made the round world so fast that it cannot be moved; and how that he shall judge the people righteously.

<sub>11</sub> Let the heavens rejoice, and let the earth be glad<span> * </span>let the sea make a noise, and all that therein is.

<sub>12</sub> Let the field be joyful, and all that is in it<span> * </span>then shall all the trees of the wood rejoice before the Lord.

<sub>13</sub> For he cometh, for he cometh to judge the earth<span> * </span>and with righteousness to judge the world, and the people with his truth.

#### *Psalm 97. Dominus regnavit*
THE Lord is King, the earth may be glad thereof<span> * </span>yea, the multitude of the isles may be glad thereof.

<sub>2</sub> Clouds and darkness are round about him<span> * </span>righteousness and judgement are the habitation of his seat.

<sub>3</sub> There shall go a fire before him<span> * </span>and burn up his enemies on every side.

<sub>4</sub> His lightnings gave shine unto the world<span> * </span>the earth saw it, and was afraid.

<sub>5</sub> The hills melted like wax at the presence of the Lord<span> * </span>at the presence of the Lord of the whole earth.

<sub>6</sub> The heavens have declared his righteousness<span> * </span>and all the people have seen his glory.

<sub>7</sub> Confounded be all they that worship carved images, and that delight in vain gods<span> * </span>worship him, all ye gods.

<sub>8</sub> Sion heard of it, and rejoiced<span> * </span>and the daughters of Judah were glad, because of thy judgements, O Lord.

<sub>9</sub> For thou, Lord, art higher than all that are in the earth<span> * </span>thou art exalted far above all gods.

<sub>10</sub> O ye that love the Lord, see that ye hate the thing which is evil<span> * </span>the Lord preserveth the souls of his saints; he shall deliver them from the hand of the ungodly.

<sub>11</sub> There is sprung up a light for the righteous<span> * </span>and joyful gladness for such as are true-hearted.

<sub>12</sub> Rejoice in the Lord, ye righteous<span> * </span>and give thanks for a remembrance of his holiness.

