#### *Legem pone*
TEACH me, O Lord, the way of thy statutes<span> * </span>and I shall keep it unto the end.

<sub>34</sub> Give me understanding, and I shall keep thy law<span> * </span>yea, I shall keep it with my whole heart.

<sub>35</sub> Make me to go in the path of thy commandments<span> * </span>for therein is my desire.

<sub>36</sub> Incline my heart unto thy testimonies<span> * </span>and not to covetousness.

<sub>37</sub> O turn away mine eyes, lest they behold vanity<span> * </span>and quicken thou me in thy way.

<sub>38</sub> O stablish thy word in thy servant<span> * </span>that I may fear thee.

<sub>39</sub> Take away the rebuke that I am afraid of<span> * </span>for thy judgements are good.

<sub>40</sub> Behold, my delight is in thy commandments<span> * </span>O quicken me in thy righteousness.

#### *Et veniat super me*
LET thy loving mercy come also unto me, O Lord<span> * </span>even thy salvation, according unto thy word.

<sub>42</sub> So shall I make answer unto my blasphemers<span> * </span>for my trust is in thy word.

<sub>43</sub> O take not the word of thy truth utterly out of my mouth<span> * </span>for my hope is in thy judgements.

<sub>44</sub> So shall I alway keep thy law<span> * </span>yea, for ever and ever.

<sub>45</sub> And I will walk at liberty<span> * </span>for I seek thy commandments.

<sub>46</sub> I will speak of thy testimonies also, even before kings<span> * </span>and will not be ashamed.

<sub>47</sub> And my delight shall be in thy commandments<span> * </span>which I have loved.

<sub>48</sub> My hands also will I lift up unto thy commandments, which I have loved<span> * </span>and my study shall be in thy statutes.

#### *Memor esto servi tui*
OTHINK upon thy servant, as concerning thy word<span> * </span>wherein thou hast caused me to put my trust.

<sub>50</sub> The same is my comfort in my trouble<span> * </span>for thy word hath quickened me.

<sub>51</sub> The proud have had me exceedingly in derision<span> * </span>yet have I not shrinked from thy law.

<sub>52</sub> For I remembered thine everlasting judgements, O Lord<span> * </span>and received comfort.

<sub>53</sub> I am horribly afraid<span> * </span>for the ungodly that forsake thy law.

<sub>54</sub> Thy statutes have been my songs<span> * </span>in the house of my pilgrimage.

<sub>55</sub> I have thought upon thy Name, O Lord, in the night-season<span> * </span>and have kept thy law.

<sub>56</sub> This I had<span> * </span>because I kept thy commandments.

#### *Portio mea, Domine*
THOU art my portion, O Lord<span> * </span>I have promised to keep thy law.

<sub>58</sub> I made my humble petition in thy presence with my whole heart<span> * </span>O be merciful unto me, according to thy word.

<sub>59</sub> I called mine own ways to remembrance<span> * </span>and turned my feet unto thy testimonies.

<sub>60</sub> I made haste, and prolonged not the time<span> * </span>to keep thy commandments.

<sub>61</sub> The congregations of the ungodly have robbed me<span> * </span>but I have not forgotten thy law.

<sub>62</sub> At midnight I will rise to give thanks unto thee<span> * </span>because of thy righteous judgements.

<sub>63</sub> I am a companion of all them that fear thee<span> * </span>and keep thy commandments.

<sub>64</sub> The earth, O Lord, is full of thy mercy<span> * </span>O teach me thy statutes.

#### *Bonitatem fecisti*
OLORD, thou hast dealt graciously with thy servant<span> * </span>according unto thy word.

<sub>66</sub> O learn me true understanding and knowledge<span> * </span>for I have believed thy commandments.

<sub>67</sub> Before I was troubled, I went wrong<span> * </span>but now have I kept thy word.

<sub>68</sub> Thou art good and gracious<span> * </span>O teach me thy statutes.

<sub>69</sub> The proud have imagined a lie against me<span> * </span>but I will keep thy commandments with my whole heart.

<sub>70</sub> Their heart is as fat as brawn<span> * </span>but my delight hath been in thy law.

<sub>71</sub> It is good for me that I have been in trouble<span> * </span>that I may learn thy statutes.

<sub>72</sub> The law of thy mouth is dearer unto me<span> * </span>than thousands of gold and silver.

