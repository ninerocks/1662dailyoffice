#### *Psalm 144. Benedictus Dominus*
BLESSED be the Lord my strength<span> * </span>who teacheth my hands to war, and my fingers to fight;

<sub>2</sub> My hope and my fortress, my castle and deliverer, my defender in whom I trust<span> * </span>who subdueth my people that is under me.

<sub>3</sub> Lord, what is man, that thou hast such respect unto him<span> * </span>or the son of man, that thou so regardest him?

<sub>4</sub> Man is like a thing of nought<span> * </span>his time passeth away like a shadow.

<sub>5</sub> Bow thy heavens, O Lord, and come down<span> * </span>touch the mountains, and they shall smoke.

<sub>6</sub> Cast forth thy lightning, and tear them<span> * </span>shoot out thine arrows, and consume them.

<sub>7</sub> Send down thine hand from above<span> * </span>deliver me, and take me out of the great waters, from the hand of strange children;

<sub>8</sub> Whose mouth talketh of vanity<span> * </span>and their right hand is a right hand of wickedness.

<sub>9</sub> I will sing a new song unto thee, O God<span> * </span>and sing praises unto thee upon a ten-stringed lute.

<sub>10</sub> Thou hast given victory unto kings<span> * </span>and hast delivered David thy servant from the peril of the sword.

<sub>11</sub> Save me, and deliver me from the hand of strange children<span> * </span>whose mouth talketh of vanity, and their right hand is a right hand of iniquity.

<sub>12</sub> That our sons may grow up as the young plants<span> * </span>and that our daughters may be as the polished corners of the temple.

<sub>13</sub> That our garners may be full and plenteous with all manner of store<span> * </span>that our sheep may bring forth thousands and ten thousands in our streets.

<sub>14</sub> That our oxen may be strong to labour, that there be no decay<span> * </span>no leading into captivity, and no complaining in our streets.

<sub>15</sub> Happy are the people that are in such a case<span> * </span>yea, blessed are the people who have the Lord for their God.

#### *Psalm 145. Exaltabo te, Deus*
IWILL magnify thee, O God, my King<span> * </span>and I will praise thy Name for ever and ever.

<sub>2</sub> Every day will I give thanks unto thee<span> * </span>and praise thy Name for ever and ever.

<sub>3</sub> Great is the Lord, and marvellous worthy to be praised<span> * </span>there is no end of his greatness.

<sub>4</sub> One generation shall praise thy works unto another<span> * </span>and declare thy power.

<sub>5</sub> As for me, I will be talking of thy worship<span> * </span>thy glory, thy praise, and wondrous works;

<sub>6</sub> So that men shall speak of the might of thy marvellous acts<span> * </span>and I will also tell of thy greatness.

<sub>7</sub> The memorial of thine abundant kindness shall be shewed<span> * </span>and men shall sing of thy righteousness.

<sub>8</sub> The Lord is gracious and merciful<span> * </span>long-suffering and of great goodness.

<sub>9</sub> The Lord is loving unto every man<span> * </span>and his mercy is over all his works.

<sub>10</sub> All thy works praise thee, O Lord<span> * </span>and thy saints give thanks unto thee.

<sub>11</sub> They shew the glory of thy kingdom<span> * </span>and talk of thy power;

<sub>12</sub> That thy power, thy glory, and mightiness of thy kingdom<span> * </span>might be known unto men.

<sub>13</sub> Thy kingdom is an everlasting kingdom<span> * </span>and thy dominion endureth throughout all ages.

<sub>14</sub> The Lord upholdeth all such as fall<span> * </span>and lifteth up all those that are down.

<sub>15</sub> The eyes of all wait upon thee, O Lord<span> * </span>and thou givest them their meat in due season.

<sub>16</sub> Thou openest thine hand<span> * </span>and fillest all things living with plenteousness.

<sub>17</sub> The Lord is righteous in all his ways<span> * </span>and holy in all his works.

<sub>18</sub> The Lord is nigh unto all them that call upon him<span> * </span>yea, all such as call upon him faithfully.

<sub>19</sub> He will fulfil the desire of them that fear him<span> * </span>he also will hear their cry, and will help them.

<sub>20</sub> The Lord preserveth all them that love him<span> * </span>but scattereth abroad all the ungodly.

<sub>21</sub> My mouth shall speak the praise of the Lord<span> * </span>and let all flesh give thanks unto his holy Name for ever and ever.

#### *Psalm 146. Lauda, anima mea*
PRAISE the Lord, O my soul; while I live will I praise the Lord<span> * </span>yea, as long as I have any being, I will sing praises unto my God.

<sub>2</sub> O put not your trust in princes, nor in any child of man<span> * </span>for there is no help in them.

<sub>3</sub> For when the breath of man goeth forth he shall turn again to his earth<span> * </span>and then all his thoughts perish.

<sub>4</sub> Blessed is he that hath the God of Jacob for his help<span> * </span>and whose hope is in the Lord his God;

<sub>5</sub> Who made heaven and earth, the sea, and all that therein is<span> * </span>who keepeth his promise for ever;

<sub>6</sub> Who helpeth them to right that suffer wrong<span> * </span>who feedeth the hungry.

<sub>7</sub> The Lord looseth men out of prison<span> * </span>the Lord giveth sight to the blind.

<sub>8</sub> The Lord helpeth them that are fallen<span> * </span>the Lord careth for the righteous.

<sub>9</sub> The Lord careth for the strangers, he defendeth the fatherless and widow<span> * </span>as for the way of the ungodly, he turneth it upside down.

<sub>10</sub> The Lord thy God, O Sion, shall be King for evermore<span> * </span>and throughout all generations.

