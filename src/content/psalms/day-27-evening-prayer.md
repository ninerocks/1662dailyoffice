#### *Psalm 126. In convertendo*
WHEN the Lord turned again the captivity of Sion<span> * </span>then were we like unto them that dream.

<sub>2</sub> Then was our mouth filled with laughter<span> * </span>and our tongue with joy.

<sub>3</sub> Then said they among the heathen<span> * </span>The Lord hath done great things for them.

<sub>4</sub> Yea, the Lord hath done great things for us already<span> * </span>whereof we rejoice.

<sub>5</sub> Turn our captivity, O Lord<span> * </span>as the rivers in the south.

<sub>6</sub> They that sow in tears<span> * </span>shall reap in joy.

<sub>7</sub> He that now goeth on his way weeping, and beareth forth good seed<span> * </span>shall doubtless come again with joy, and bring his sheaves with him.

#### *Psalm 127. Nisi Dominus*
EXCEPT the Lord build the house<span> * </span>their labour is but lost that build it.

<sub>2</sub> Except the Lord keep the city<span> * </span>the watchman waketh but in vain.

<sub>3</sub> It is but lost labour that ye haste to rise up early, and so late take rest, and eat the bread of carefulness<span> * </span>for so he giveth his beloved sleep.

<sub>4</sub> Lo, children and the fruit of the womb<span> * </span>are an heritage and gift that cometh of the Lord.

<sub>5</sub> Like as the arrows in the hand of the giant<span> * </span>even so are the young children.

<sub>6</sub> Happy is the man that hath his quiver full of them<span> * </span>they shall not be ashamed when they speak with their enemies in the gate.

#### *Psalm 128. Beati omnes*
BLESSED are all they that fear the Lord<span> * </span>and walk in his ways.

<sub>2</sub> For thou shalt eat the labours of thine hands<span> * </span>O well is thee, and happy shalt thou be.

<sub>3</sub> Thy wife shall be as the fruitful vine<span> * </span>upon the walls of thine house.

<sub>4</sub> Thy children like the olive-branches<span> * </span>round about thy table.

<sub>5</sub> Lo, thus shall the man be blessed<span> * </span>that feareth the Lord.

<sub>6</sub> The Lord from out of Sion shall so bless thee<span> * </span>that thou shalt see Jerusalem in prosperity all thy life long.

<sub>7</sub> Yea, that thou shalt see thy children's children<span> * </span>and peace upon Israel.

#### *Psalm 129. Saepe expugnaverunt*
MANY a time have they fought against me from my youth up<span> * </span>may Israel now say.

<sub>2</sub> Yea, many a time have they vexed me from my youth up<span> * </span>but they have not prevailed against me.

<sub>3</sub> The plowers plowed upon my back<span> * </span>and made long furrows.

<sub>4</sub> But the righteous Lord<span> * </span>hath hewn the snares of the ungodly in pieces.

<sub>5</sub> Let them be confounded and turned backward<span> * </span>as many as have evil will at Sion.

<sub>6</sub> Let them be even as the grass growing upon the house-tops<span> * </span>which withereth afore it be plucked up;

<sub>7</sub> Whereof the mower filleth not his hand<span> * </span>neither he that bindeth up the sheaves his bosom.

<sub>8</sub> So that they who go by say not so much as, The Lord prosper you<span> * </span>we wish you good luck in the Name of the Lord.

#### *Psalm 130. De profundis*
OUT of the deep have I called unto thee, O Lord<span> * </span>Lord, hear my voice.

<sub>2</sub> O let thine ears consider well<span> * </span>the voice of my complaint.

<sub>3</sub> If thou, Lord, wilt be extreme to mark what is done amiss<span> * </span>O Lord, who may abide it?

<sub>4</sub> For there is mercy with thee<span> * </span>therefore shalt thou be feared.

<sub>5</sub> I look for the Lord; my soul doth wait for him<span> * </span>in his word is my trust.

<sub>6</sub> My soul fleeth unto the Lord<span> * </span>before the morning watch, I say, before the morning watch.

<sub>7</sub> O Israel, trust in the Lord, for with the Lord there is mercy<span> * </span>and with him is plenteous redemption.

<sub>8</sub> And he shall redeem Israel<span> * </span>from all his sins.

#### *Psalm 131. Domine, non est*
LORD, I am not high-minded<span> * </span>I have no proud looks.

<sub>2</sub> I do not exercise myself in great matters<span> * </span>which are too high for me.

<sub>3</sub> But I refrain my soul, and keep it low, like as a child that is weaned from his mother<span> * </span>yea, my soul is even as a weaned child.

<sub>4</sub> O Israel, trust in the Lord<span> * </span>from this time forth for evermore.

