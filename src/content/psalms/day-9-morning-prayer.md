#### *Psalm 44. Deus, auribus*
WE HAVE heard with our ears, O God, our fathers have told us<span> * </span>what thou hast done in their time of old;

<sub>2</sub> How thou hast driven out the heathen with thy hand, and planted them in<span> * </span>how thou hast destroyed the nations and cast them out.

<sub>3</sub> For they gat not the land in possession through their own sword<span> * </span>neither was it their own arm that helped them;

<sub>4</sub> But thy right hand, and thine arm, and the light of thy countenance<span> * </span>because thou hadst a favour unto them.

<sub>5</sub> Thou art my King, O God<span> * </span>send help unto Jacob.

<sub>6</sub> Through thee will we overthrow our enemies<span> * </span>and in thy Name will we tread them under, that rise up against us.

<sub>7</sub> For I will not trust in my bow<span> * </span>it is not my sword that shall help me;

<sub>8</sub> But it is thou that savest us from our enemies<span> * </span>and puttest them to confusion that hate us.

<sub>9</sub> We make our boast of God all day long<span> * </span>and will praise thy Name for ever.

<sub>10</sub> But now thou art far off, and puttest us to confusion<span> * </span>and goest not forth with our armies.

<sub>11</sub> Thou makest us to turn our backs upon our enemies<span> * </span>so that they which hate us spoil our goods.

<sub>12</sub> Thou lettest us be eaten up like sheep<span> * </span>and hast scattered us among the heathen.

<sub>13</sub> Thou sellest thy people for nought<span> * </span>and takest no money for them.

<sub>14</sub> Thou makest us to be rebuked of our neighbours<span> * </span>to be laughed to scorn, and had in derision of them that are round about us.

<sub>15</sub> Thou makest us to be a by-word among the heathen<span> * </span>and that the people shake their heads at us.

<sub>16</sub> My confusion is daily before me<span> * </span>and the shame of my face hath covered me;

<sub>17</sub> For the voice of the slanderer and blasphemer<span> * </span>for the enemy and avenger.

<sub>18</sub> And though all this be come upon us, yet do we not forget thee<span> * </span>nor behave ourselves frowardly in thy covenant.

<sub>19</sub> Our heart is not turned back<span> * </span>neither our steps gone out of thy way;

<sub>20</sub> No, not when thou hast smitten us into the place of dragons<span> * </span>and covered us with the shadow of death.

<sub>21</sub> If we have forgotten the Name of our God, and holden up our hands to any strange god<span> * </span>shall not God search it out? for he knoweth the very secrets of the heart.

<sub>22</sub> For thy sake also are we killed all the day long<span> * </span>and are counted as sheep appointed to be slain.

<sub>23</sub> Up, Lord, why sleepest thou<span> * </span>awake, and be not absent from us for ever.

<sub>24</sub> Wherefore hidest thou thy face<span> * </span>and forgettest our misery and trouble?

<sub>25</sub> For our soul is brought low, even unto the dust<span> * </span>our belly cleaveth unto the ground.

<sub>26</sub> Arise, and help us<span> * </span>and deliver us for thy mercy's sake.

#### *Psalm 45. Eructavit cor meum*
MY HEART is inditing of a good matter<span> * </span>I speak of the things which I have made unto the King.

<sub>2</sub> My tongue is the pen<span> * </span>of a ready writer.

#### *3. Thou art fairer than the children of men: full of grace are thy lips, because God hath blessed thee for ever.*
<sub>4</sub> Gird thee with thy sword upon thy thigh, O thou most Mighty<span> * </span>according to thy worship and renown.

<sub>5</sub> Good luck have thou with thine honour<span> * </span>ride on, because of the word of truth , of meekness, and righteousness; and thy right hand shall teach thee terrible things.

<sub>6</sub> Thy arrows are very sharp, and the people shall be subdued unto thee<span> * </span>even in the midst among the King's enemies.

<sub>7</sub> Thy seat, O God, endureth for ever<span> * </span>the sceptre of thy kingdom is a right sceptre.

<sub>8</sub> Thou hast loved righteousness, and hated iniquity<span> * </span>wherefore God, even thy God, hath anointed thee with the oil of gladness above thy fellows.

<sub>9</sub> All thy garments smell of myrrh, aloes, and cassia<span> * </span>out of the ivory palaces, whereby they have made thee glad.

<sub>10</sub> Kings' daughters were among thy honourable women<span> * </span>upon thy right hand did stand the queen in a vesture of gold, wrought about with divers colours.

#### *11. Hearken, O daughter, and consider, incline thine ear ; forget also thine own people, and thy father's house.*
<sub>12</sub> So shall the King have pleasure in thy beauty<span> * </span>for he is thy Lord God, and worship thou him.

<sub>13</sub> And the daughter of Tyre shall be there with a gift<span> * </span>like as the rich also among the people shall make their supplication before thee.

<sub>14</sub> The King's daughter is all glorious within<span> * </span>her clothing is of wrought gold.

<sub>15</sub> She shall be brought unto the King in raiment of needle-work<span> * </span>the virgins that be her fellows shall bear her company, and shall be brought unto thee.

<sub>16</sub> With joy and gladness shall they be brought<span> * </span>and shall enter into the King's palace.

<sub>17</sub> Instead of thy fathers thou shalt have children<span> * </span>whom thou mayest make princes in all lands.

<sub>18</sub> I will remember thy Name from one generation to another<span> * </span>therefore shall the people give thanks unto thee, world without end.

#### *Psalm 46. Deus noster refugium*
GOD is our hope and strength<span> * </span>a very present help in trouble.

<sub>2</sub> Therefore will we not fear, though the earth be moved<span> * </span>and though the hills be carried into the midst of the sea;

<sub>3</sub> Though the waters thereof rage and swell<span> * </span>and though the mountains shake at the tempest of the same.

<sub>4</sub> The rivers of the flood thereof shall make glad the city of God<span> * </span>the holy place of the tabernacle of the most Highest.

<sub>5</sub> God is in the midst of her, therefore shall she not be removed<span> * </span>God shall help her, and that right early.

<sub>6</sub> The heathen make much ado, and the kingdoms are moved<span> * </span>but God hath shewed his voice, and the earth shall melt away.

<sub>7</sub> The Lord of hosts is with us<span> * </span>the God of Jacob is our refuge.

<sub>8</sub> O come hither, and behold the works of the Lord<span> * </span>what destruction he hath brought upon the earth.

<sub>9</sub> He maketh wars to cease in all the world<span> * </span>he breaketh the bow, and knappeth the spear in sunder, and burneth the chariots in the fire.

<sub>10</sub> Be still then, and know that I am God<span> * </span>I will be exalted among the heathen, and I will be exalted in the earth.

<sub>11</sub> The Lord of hosts is with us<span> * </span>the God of Jacob is our refuge.

