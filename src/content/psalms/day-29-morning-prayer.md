#### *Psalm 139. Domine, probasti*
OLORD, thou hast searched me out and known me<span> * </span>thou knowest my down-sitting and mine up-rising, thou understandest my thoughts long before.

<sub>2</sub> Thou art about my path, and about my bed<span> * </span>and spiest out all my ways.

<sub>3</sub> For lo, there is not a word in my tongue<span> * </span>but thou, O Lord, knowest it altogether.

<sub>4</sub> Thou hast fashioned me behind and before<span> * </span>and laid thine hand upon me.

<sub>5</sub> Such knowledge is too wonderful and excellent for me<span> * </span>I cannot attain unto it.

<sub>6</sub> Whither shall I go then from thy Spirit<span> * </span>or whither shall I go then from thy presence?

<sub>7</sub> If I climb up into heaven, thou art there<span> * </span>if I go down to hell, thou art there also.

<sub>8</sub> If I take the wings of the morning<span> * </span>and remain in the uttermost parts of the sea;

<sub>9</sub> Even there also shall thy hand lead me<span> * </span>and thy right hand shall hold me.

<sub>10</sub> If I say, Peradventure the darkness shall cover me<span> * </span>then shall my night be turned to day.

<sub>11</sub> Yea, the darkness is no darkness with thee, but the night is as clear as the day<span> * </span>the darkness and light to thee are both alike.

<sub>12</sub> For my reins are thine<span> * </span>thou hast covered me in my mother's womb.

<sub>13</sub> I will give thanks unto thee, for I am fearfully and wonderfully made<span> * </span>marvellous are thy works, and that my soul knoweth right well.

<sub>14</sub> My bones are not hid from thee<span> * </span>though I be made secretly, and fashioned beneath in the earth.

<sub>15</sub> Thine eyes did see my substance, yet being unperfect<span> * </span>and in thy book were all my members written;

<sub>16</sub> Which day by day were fashioned<span> * </span>when as yet there was none of them.

<sub>17</sub> How dear are thy counsels unto me, O God<span> * </span>O how great is the sum of them!

<sub>18</sub> If I tell them, they are more in number than the sand<span> * </span>when I wake up I am present with thee.

<sub>19</sub> Wilt thou not slay the wicked, O God<span> * </span>depart from me, ye blood-thirsty men.

<sub>20</sub> For they speak unrighteously against thee<span> * </span>and thine enemies take thy Name in vain.

<sub>21</sub> Do not I hate them, O Lord, that hate thee<span> * </span>and am not I grieved with those that rise up against thee?

<sub>22</sub> Yea, I hate them right sore<span> * </span>even as though they were mine enemies.

<sub>23</sub> Try me, O God, and seek the ground of my heart<span> * </span>prove me, and examine my thoughts.

<sub>24</sub> Look well if there be any way of wickedness in me<span> * </span>and lead me in the way everlasting.

#### *Psalm 140. Eripe me, Domine*
DELIVER me, O Lord, from the evil man<span> * </span>and preserve me from the wicked man.

<sub>2</sub> Who imagine mischief in their hearts<span> * </span>and stir up strife all the day long.

<sub>3</sub> They have sharpened their tongues like a serpent<span> * </span>adders' poison is under their lips.

<sub>4</sub> Keep me, O Lord, from the hands of the ungodly<span> * </span>preserve me from the wicked men, who are purposed to overthrow my goings.

<sub>5</sub> The proud have laid a snare for me, and spread a net abroad with cords<span> * </span>yea, and set traps in my way.

<sub>6</sub> I said unto the Lord, Thou art my God<span> * </span>hear the voice of my prayers, O Lord.

<sub>7</sub> O Lord God, thou strength of my health<span> * </span>thou hast covered my head in the day of the battle.

<sub>8</sub> Let not the ungodly have his desire, O Lord<span> * </span>let not his mischievous imagination prosper, lest they be too proud.

<sub>9</sub> Let the mischief of their own lips fall upon the head of them<span> * </span>that compass me about.

<sub>10</sub> Let hot burning coals fall upon them<span> * </span>let them be cast into the fire and into the pit, that they never rise up again.

<sub>11</sub> A man full of words shall not prosper upon the earth<span> * </span>evil shall hunt the wicked person to overthrow him.

<sub>12</sub> Sure I am that the Lord will avenge the poor<span> * </span>and maintain the cause of the helpless.

<sub>13</sub> The righteous also shall give thanks unto thy Name<span> * </span>and the just shall continue in thy sight.

#### *Psalm 141. Domine, clamavi*
LORD, I call upon thee, haste thee unto me<span> * </span>and consider my voice when I cry unto thee.

<sub>2</sub> Let my prayer be set forth in thy sight as the incense<span> * </span>and let the lifting up of my hands be an evening sacrifice.

<sub>3</sub> Set a watch, O Lord, before my mouth<span> * </span>and keep the door of my lips.

<sub>4</sub> O let not mine heart be inclined to any evil thing<span> * </span>let me not be occupied in ungodly works with the men that work wickedness, lest I eat of such things as please them.

<sub>5</sub> Let the righteous rather smite me friendly<span> * </span>and reprove me.

<sub>6</sub> But let not their precious balms break my head<span> * </span>yea, I will pray yet against their wickedness.

<sub>7</sub> Let their judges be overthrown in stony places<span> * </span>that they may hear my words, for they are sweet.

<sub>8</sub> Our bones lie scattered before the pit<span> * </span>like as when one breaketh and heweth wood upon the earth.

<sub>9</sub> But mine eyes look unto thee, O Lord God<span> * </span>in thee is my trust , O cast not out my soul.

<sub>10</sub> Keep me from the snare that they have laid for me<span> * </span>and from the traps of the wicked doers.

<sub>11</sub> Let the ungodly fall into their own nets together<span> * </span>and let me ever escape them.

