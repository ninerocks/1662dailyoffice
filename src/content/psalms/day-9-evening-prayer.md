#### *Psalm 47. Omnes gentes, plaudite*
OCLAP your hands together, all ye people<span> * </span>O sing unto God with the voice of melody.

<sub>2</sub> For the Lord is high, and to be feared<span> * </span>he is the great King upon all the earth.

<sub>3</sub> He shall subdue the people under us<span> * </span>and the nations under our feet.

<sub>4</sub> He shall choose out an heritage for us<span> * </span>even the worship of Jacob, whom he loved.

<sub>5</sub> God is gone up with a merry noise<span> * </span>and the Lord with the sound of the trump.

<sub>6</sub> O sing praises, sing praises unto our God<span> * </span>O sing praises, sing praises unto our King.

<sub>7</sub> For God is the King of all the earth<span> * </span>sing ye praises with understanding.

<sub>8</sub> God reigneth over the heathen<span> * </span>God sitteth upon his holy seat.

#### *9. The princes of the people are joined unto the people of the God of Abraham: for God, which is very high exalted, doth defend the earth, as it were with a shield.*
#### *Psalm 48. Magnus Dominus*
GREAT is the Lord, and highly to be praised<span> * </span>in the city of our God, even upon his holy hill.

<sub>2</sub> The hill of Sion is a fair place, and the joy of the whole earth<span> * </span>upon the north-side lieth the city of the great King; God is well known in her palaces as a sure refuge.

<sub>3</sub> For lo, the kings of the earth<span> * </span>are gathered, and gone by together.

<sub>4</sub> They marvelled to see such things<span> * </span>they were astonished, and suddenly cast down.

<sub>5</sub> Fear came there upon them, and sorrow<span> * </span>as upon a woman in her travail.

<sub>6</sub> Thou shalt break the ships of the sea<span> * </span>through the east-wind.

<sub>7</sub> Like as we have heard, so have we seen in the city of the Lord of hosts, in the city of our God<span> * </span>God upholdeth the same for ever.

<sub>8</sub> We wait for thy loving-kindness, O God<span> * </span>in the midst of thy temple.

<sub>9</sub> O God, according to thy Name, so is thy praise unto the world's end<span> * </span>thy right hand is full of righteousness.

<sub>10</sub> Let the mount Sion rejoice, and the daughters of Judah be glad<span> * </span>because of thy judgements.

<sub>11</sub> Walk about Sion, and go round about her<span> * </span>and tell the towers thereof.

<sub>12</sub> Mark well her bulwarks, set up her houses<span> * </span>that ye may tell them that come after.

<sub>13</sub> For this God is our God for ever and ever<span> * </span>he shall be our guide unto death.

#### *Psalm 49. Audite haec, omnes*
OHEAR ye this, all ye people<span> * </span>ponder it with your ears, all ye that dwell in the world;

<sub>2</sub> High and low, rich and poor<span> * </span>one with another.

<sub>3</sub> My mouth shall speak of wisdom<span> * </span>and my heart shall muse of understanding.

<sub>4</sub> I will incline mine ear to the parable<span> * </span>and shew my dark speech upon the harp.

<sub>5</sub> Wherefore should I fear in the days of wickedness<span> * </span>and when the wickedness of my heels compasseth me round about?

<sub>6</sub> There be some that put their trust in their goods<span> * </span>and boast themselves in the multitude of their riches.

<sub>7</sub> But no man may deliver his brother<span> * </span>nor make agreement unto God for him;

<sub>8</sub> For it cost more to redeem their souls<span> * </span>so that he must let that alone for ever;

<sub>9</sub> Yea, though he live long<span> * </span>and see not the grave.

<sub>10</sub> For he seeth that wise men also die, and perish together<span> * </span>as well as the ignorant and foolish, and leave their riches for other.

<sub>11</sub> And yet they think that their houses shall continue for ever<span> * </span>and that their dwelling-places shall endure from one generation to another; and call the lands after their own names.

<sub>12</sub> Nevertheless, man will not abide in honour<span> * </span>seeing he may be compared unto the beasts that perish; this is the way of them.

<sub>13</sub> This is their foolishness<span> * </span>and their posterity praise their saying.

<sub>14</sub> They lie in the hell like sheep, death gnaweth upon them, and the righteous shall have domination over them in the morning<span> * </span>their beauty shall consume in the sepulchre out of their dwelling.

<sub>15</sub> But God hath delivered my soul from the place of hell<span> * </span>for he shall receive me.

<sub>16</sub> Be not thou afraid, though one be made rich<span> * </span>or if the glory of his house be increased;

<sub>17</sub> For he shall carry nothing away with him when he dieth<span> * </span>neither shall his pomp follow him.

<sub>18</sub> For while he lived, he counted himself an happy man<span> * </span>and so long as thou doest well unto thyself, men will speak good of thee.

<sub>19</sub> He shall follow the generation of his fathers<span> * </span>and shall never see light.

<sub>20</sub> Man being in honour hath no understanding<span> * </span>but is compared unto the beasts that perish.

