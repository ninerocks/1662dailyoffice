#### *Psalm 142. Voce mea ad Dominum*
ICRIED unto the Lord with my voice<span> * </span>yea, even unto the Lord did I make my supplication.

<sub>2</sub> I poured out my complaints before him<span> * </span>and shewed him of my trouble.

<sub>3</sub> When my spirit was in heaviness thou knewest my path<span> * </span>in the way wherein I walked have they privily laid a snare for me.

<sub>4</sub> I looked also upon my right hand<span> * </span>and saw there was no man that would know me.

<sub>5</sub> I had no place to flee unto<span> * </span>and no man cared for my soul.

<sub>6</sub> I cried unto thee, O Lord, and said<span> * </span>Thou art my hope, and my portion in the land of the living.

<sub>7</sub> Consider my complaint<span> * </span>for I am brought very low.

<sub>8</sub> O deliver me from my persecutors<span> * </span>for they are too strong for me.

<sub>9</sub> Bring my soul out of prison, that I may give thanks unto thy Name<span> * </span>which thing if thou wilt grant me, then shall the righteous resort unto my company.

#### *Psalm 143. Domine, exaudi*
HEAR my prayer, O Lord, and consider my desire<span> * </span>hearken unto me for thy truth and righteousness' sake.

<sub>2</sub> And enter not into judgement with thy servant<span> * </span>for in thy sight shall no man living be justified.

<sub>3</sub> For the enemy hath persecuted my soul; he hath smitten my life down to the ground<span> * </span>he hath laid me in the darkness, as the men that have been long dead.

<sub>4</sub> Therefore is my spirit vexed within me<span> * </span>and my heart within me is desolate.

<sub>5</sub> Yet do I remember the time past; I muse upon all thy works<span> * </span>yea, I exercise myself in the works of thy hands.

<sub>6</sub> I stretch forth my hands unto thee<span> * </span>my soul gaspeth unto thee as a thirsty land.

<sub>7</sub> Hear me, O Lord, and that soon, for my spirit waxeth faint<span> * </span>hide not thy face from me, lest I be like unto them that go down into the pit.

<sub>8</sub> O let me hear thy loving-kindness betimes in the morning, for in thee is my trust<span> * </span>shew thou me the way that I should walk in, for I lift up my soul unto thee.

<sub>9</sub> Deliver me, O Lord, from mine enemies<span> * </span>for I flee unto thee to hide me.

<sub>10</sub> Teach me to do the thing that pleaseth thee, for thou art my God<span> * </span>let thy loving Spirit lead me forth into the land of righteousness.

<sub>11</sub> Quicken me, O Lord, for thy Name's sake<span> * </span>and for thy righteousness' sake bring my soul out of trouble.

<sub>12</sub> And of thy goodness slay mine enemies<span> * </span>and destroy all them that vex my soul ; for I am thy servant.

