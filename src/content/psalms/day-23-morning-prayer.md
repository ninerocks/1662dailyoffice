#### *Psalm 110. Dixit Dominus*
THE Lord said unto my Lord<span> * </span>Sit thou on my right hand, until I make thine enemies thy footstool.

<sub>2</sub> The Lord shall send the rod of thy power out of Sion<span> * </span>be thou ruler, even in the midst among thine enemies.

<sub>3</sub> In the day of thy power shall the people offer thee free-will offerings with an holy worship<span> * </span>the dew of thy birth is of the womb of the morning.

<sub>4</sub> The Lord sware, and will not repent<span> * </span>Thou art a priest for ever after the order of Melchisedech.

<sub>5</sub> The Lord upon thy right hand<span> * </span>shall wound even kings in the day of his wrath.

<sub>6</sub> He shall judge among the heathen; he shall fill the places with the dead bodies<span> * </span>and smite in sunder the heads over divers countries.

<sub>7</sub> He shall drink of the brook in the way<span> * </span>therefore shall he lift up his head.

#### *Psalm 111. Confitebor tibi*
IWILL give thanks unto the Lord with my whole heart<span> * </span>secretly among the faithful, and in the congregation.

<sub>2</sub> The works of the Lord are great<span> * </span>sought out of all them that have pleasure therein.

<sub>3</sub> His work is worthy to be praised and had in honour<span> * </span>and his righteousness endureth for ever.

<sub>4</sub> The merciful and gracious Lord hath so done his marvellous works<span> * </span>that they ought to be had in remembrance.

<sub>5</sub> He hath given meat unto them that fear him<span> * </span>he shall ever be mindful of his covenant.

<sub>6</sub> He hath shewed his people the power of his works<span> * </span>that he may give them the heritage of the heathen.

<sub>7</sub> The works of his hands are verity and judgement<span> * </span>all his commandments are true.

<sub>8</sub> They stand fast for ever and ever<span> * </span>and are done in truth and equity.

<sub>9</sub> He sent redemption unto his people<span> * </span>he hath commanded his covenant for ever; holy and reverend is his Name.

<sub>10</sub> The fear of the Lord is the beginning of wisdom<span> * </span>a good understanding have all they that do thereafter; the praise of it endureth for ever.

#### *Psalm 112. Beatus vir*
BLESSED is the man that feareth the Lord<span> * </span>he hath great delight in his commandments.

<sub>2</sub> His seed shall be mighty upon earth<span> * </span>the generation of the faithful shall be blessed.

<sub>3</sub> Riches and plenteousness shall be in his house<span> * </span>and his righteousness endureth for ever.

<sub>4</sub> Unto the godly there ariseth up light in the darkness<span> * </span>he is merciful, loving, and righteous.

<sub>5</sub> A good man is merciful, and lendeth<span> * </span>and will guide his words with discretion.

<sub>6</sub> For he shall never be moved<span> * </span>and the righteous shall be had in everlasting remembrance.

<sub>7</sub> He will not be afraid of any evil tidings<span> * </span>for his heart standeth fast, and believeth in the Lord.

<sub>8</sub> His heart is established, and will not shrink<span> * </span>until he see his desire upon his enemies.

<sub>9</sub> He hath dispersed abroad, and given to the poor<span> * </span>and his righteousness remaineth for ever; his horn shall be exalted with honour.

<sub>10</sub> The ungodly shall see it, and it shall grieve him<span> * </span>he shall gnash with his teeth, and consume away; the desire of the ungodly shall perish.

#### *Psalm 113. Laudate, pueri*
PRAISE the Lord, ye servants<span> * </span>O praise the Name of the Lord.

<sub>2</sub> Blessed be the Name of the Lord<span> * </span>from this time forth for evermore.

<sub>3</sub> The Lord's Name is praised<span> * </span>from the rising up of the sun unto the going down of the same.

<sub>4</sub> The Lord is high above all heathen<span> * </span>and his glory above the heavens.

<sub>5</sub> Who is like unto the Lord our God, that hath his dwelling so high<span> * </span>and yet humbleth himself to behold the things that are in heaven and earth?

<sub>6</sub> He taketh up the simple out of the dust<span> * </span>and lifteth the poor out of the mire;

<sub>7</sub> That he may set him with the princes<span> * </span>even with the princes of his people.

<sub>8</sub> He maketh the barren woman to keep house<span> * </span>and to be a joyful mother of children.

