#### *Clamavi in toto corde meo*
ICALL with my whole heart<span> * </span>hear me, O Lord, I will keep thy statutes.

<sub>146</sub> Yea, even unto thee do I call<span> * </span>help me, and I shall keep thy testimonies.

<sub>147</sub> Early in the morning do I cry unto thee<span> * </span>for in thy word is my trust.

<sub>148</sub> Mine eyes prevent the night-watches<span> * </span>that I might be occupied in thy words.

<sub>149</sub> Hear my voice, O Lord, according unto thy loving-kindness<span> * </span>quicken me, according as thou art wont.

<sub>150</sub> They draw nigh that of malice persecute me<span> * </span>and are far from thy law.

<sub>151</sub> Be thou nigh at hand, O Lord<span> * </span>for all thy commandments are true.

<sub>152</sub> As concerning thy testimonies, I have known long since<span> * </span>that thou hast grounded them for ever.

#### *Vide humilitatem*
OCONSIDER mine adversity, and deliver me<span> * </span>for I do not forget thy law.

<sub>154</sub> Avenge thou my cause, and deliver me<span> * </span>quicken me, according to thy word.

<sub>155</sub> Health is far from the ungodly<span> * </span>for they regard not thy statutes.

<sub>156</sub> Great is thy mercy, O Lord<span> * </span>quicken me, as thou art wont.

<sub>157</sub> Many there are that trouble me, and persecute me<span> * </span>yet do I not swerve from thy testimonies.

<sub>158</sub> It grieveth me when I see the transgressors<span> * </span>because they keep not thy law.

<sub>159</sub> Consider, O Lord, how I love thy commandments<span> * </span>O quicken me, according to thy loving-kindness.

<sub>160</sub> Thy word is true from everlasting<span> * </span>all the judgements of thy righteousness endure for evermore.

#### *Principes persecuti sunt*
PRINCES have persecuted me without a cause<span> * </span>but my heart standeth in awe of thy word.

<sub>162</sub> I am as glad of thy word<span> * </span>as one that findeth great spoils.

<sub>163</sub> As for lies, I hate and abhor them<span> * </span>but thy law do I love.

<sub>164</sub> Seven times a day do I praise thee<span> * </span>because of thy righteous judgements.

<sub>165</sub> Great is the peace that they have who love thy law<span> * </span>and they are not offended at it.

<sub>166</sub> Lord, I have looked for thy saving health<span> * </span>and done after thy commandments.

<sub>167</sub> My soul hath kept thy testimonies<span> * </span>and loved them exceedingly.

<sub>168</sub> I have kept thy commandments and testimonies<span> * </span>for all my ways are before thee.

#### *Appropinquet deprecatio*
LET my complaint come before thee, O Lord<span> * </span>give me understanding, according to thy word.

<sub>170</sub> Let my supplication come before thee<span> * </span>deliver me, according to thy word.

<sub>171</sub> My lips shall speak of thy praise<span> * </span>when thou hast taught me thy statutes.

<sub>172</sub> Yea, my tongue shall sing of thy word<span> * </span>for all thy commandments are righteous.

<sub>173</sub> Let thine hand help me<span> * </span>for I have chosen thy commandments.

<sub>174</sub> I have longed for thy saving health, O Lord<span> * </span>and in thy law is my delight.

<sub>175</sub> O let my soul live, and it shall praise thee<span> * </span>and thy judgements shall help me.

<sub>176</sub> I have gone astray like a sheep that is lost<span> * </span>O seek thy servant, for I do not forget thy commandments.

