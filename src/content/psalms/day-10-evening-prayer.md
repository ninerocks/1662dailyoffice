#### *Psalm 53. Dixit insipiens*
THE foolish body hath said in his heart<span> * </span>There is no God.

<sub>2</sub> Corrupt are they, and become abominable in their wickedness<span> * </span>there is none that doeth good.

<sub>3</sub> God looked down from heaven upon the children of men<span> * </span>to see if there were any that would understand, and seek after God.

<sub>4</sub> But they are all gone out of the way, they are altogether become abominable<span> * </span>there is also none that doeth good, no not one.

<sub>5</sub> Are not they without understanding, that work wickedness<span> * </span>eating up my people as if they would eat bread? they have not called upon God.

<sub>6</sub> They were afraid where no fear was<span> * </span>for God hath broken the bones of him that besieged thee; thou hast put them to confusion, because God hath despised them.

<sub>7</sub> O that the salvation were given unto Israel out of Sion<span> * </span>O that the Lord would deliver his people out of captivity!

<sub>8</sub> Then should Jacob rejoice<span> * </span>and Israel should be right glad.

#### *Psalm 54. Deus, in nomine*
SAVE me, O God, for thy Name's sake<span> * </span>and avenge me in thy strength.

<sub>2</sub> Hear my prayer, O God<span> * </span>and hearken unto the words of my mouth.

<sub>3</sub> For strangers are risen up against me<span> * </span>and tyrants, which have not God before their eyes, seek after my soul.

<sub>4</sub> Behold, God is my helper<span> * </span>the Lord is with them that uphold my soul.

<sub>5</sub> He shall reward evil unto mine enemies<span> * </span>destroy thou them in thy truth.

<sub>6</sub> An offering of a free heart will I give thee, and praise thy Name, O Lord<span> * </span>because it is so comfortable.

<sub>7</sub> For he hath delivered me out of all my trouble<span> * </span>and mine eye hath seen his desire upon mine enemies.

#### *Psalm 55. Exaudi, Deus*
HEAR my prayer, O God<span> * </span>and hide not thyself from my petition.

<sub>2</sub> Take heed unto me, and hear me<span> * </span>how I mourn in my prayer, and am vexed.

<sub>3</sub> The enemy crieth so, and the ungodly cometh on so fast<span> * </span>for they are minded to do me some mischief; so maliciously are they set against me.

<sub>4</sub> My heart is disquieted within me<span> * </span>and the fear of death is fallen upon me.

<sub>5</sub> Fearfulness and trembling are come upon me<span> * </span>and an horrible dread hath overwhelmed me.

<sub>6</sub> And I said, O that I had wings like a dove<span> * </span>for then would I flee away, and be at rest.

<sub>7</sub> Lo, then would I get me away far off<span> * </span>and remain in the wilderness.

<sub>8</sub> I would make haste to escape<span> * </span>because of the stormy wind and tempest.

<sub>9</sub> Destroy their tongues, O Lord, and divide them<span> * </span>for I have spied unrighteousness and strife in the city.

<sub>10</sub> Day and night they go about within the walls thereof<span> * </span>mischief also and sorrow are in the midst of it.

<sub>11</sub> Wickedness is therein<span> * </span>deceit and guile go not out of their streets.

<sub>12</sub> For it is not an open enemy, that hath done me this dishonour<span> * </span>for then I could have borne it.

<sub>13</sub> Neither was it mine adversary, that did magnify himself against me<span> * </span>for then peradventure I would have hid myself from him.

<sub>14</sub> But it was even thou, my companion<span> * </span>my guide, and mine own familiar friend.

<sub>15</sub> We took sweet counsel together<span> * </span>and walked in the house of God as friends.

<sub>16</sub> Let death come hastily upon them, and let them go down quick into hell<span> * </span>for wickedness is in their dwellings, and among them.

<sub>17</sub> As for me, I will call upon God<span> * </span>and the Lord shall save me.

<sub>18</sub> In the evening, and morning, and at noonday will I pray, and that instantly<span> * </span>and he shall hear my voice.

<sub>19</sub> It is he that hath delivered my soul in peace from the battle that was against me<span> * </span>for there were many with me.

<sub>20</sub> Yea, even God, that endureth for ever, shall hear me, and bring them down<span> * </span>for they will not turn, nor fear God.

<sub>21</sub> He laid his hands upon such as be at peace with him<span> * </span>and he brake his covenant.

<sub>22</sub> The words of his mouth were softer than butter, having war in his heart<span> * </span>his words were smoother than oil, and yet be they very swords.

<sub>23</sub> O cast thy burden upon the Lord, and he shall nourish thee<span> * </span>and shall not suffer the righteous to fall for ever.

<sub>24</sub> And as for them<span> * </span>thou, O God, shalt bring them into the pit of destruction.

<sub>25</sub> The blood-thirsty and deceitful men shall not live out half their days<span> * </span>nevertheless, my trust shall be in thee, O Lord.

