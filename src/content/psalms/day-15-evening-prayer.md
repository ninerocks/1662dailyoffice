#### *Psalm 78. Attendite, popule*
HEAR my law, O my people<span> * </span>incline your ears unto the words of my mouth.

<sub>2</sub> I will open my mouth in a parable<span> * </span>I will declare hard sentences of old;

<sub>3</sub> Which we have heard and known<span> * </span>and such as our fathers have told us;

<sub>4</sub> That we should not hide them from the children of the generations to come<span> * </span>but to shew the honour of the Lord, his mighty and wonderful works that he hath done.

<sub>5</sub> He made a covenant with Jacob, and gave Israel a law<span> * </span>which he commanded our forefathers to teach their children;

<sub>6</sub> That their posterity might know it<span> * </span>and the children which were yet unborn;

<sub>7</sub> To the intent that when they came up<span> * </span>they might shew their children the same;

<sub>8</sub> That they might put their trust in God<span> * </span>and not to forget the works of God, but to keep his commandments;

<sub>9</sub> And not to be as their forefathers, a faithless and stubborn generation<span> * </span>a generation that set not their heart aright, and whose spirit cleaveth not stedfastly unto God;

<sub>10</sub> Like as the children of Ephraim<span> * </span>who being harnessed, and carrying bows, turned themselves back in the day of battle.

<sub>11</sub> They kept not the covenant of God<span> * </span>and would not walk in his law;

<sub>12</sub> But forgat what he had done<span> * </span>and the wonderful works that he had shewed for them.

<sub>13</sub> Marvellous things did he in the sight of our forefathers, in the land of Egypt<span> * </span>even in the field of Zoan.

<sub>14</sub> He divided the sea, and let them go through<span> * </span>he made the waters to stand on an heap.

<sub>15</sub> In the day-time also he led them with a cloud<span> * </span>and all the night through with a light of fire.

<sub>16</sub> He clave the hard rocks in the wilderness<span> * </span>and gave them drink thereof, as it had been out of the great depth.

<sub>17</sub> He brought waters out of the stony rock<span> * </span>so that it gushed out like the rivers.

<sub>18</sub> Yet for all this they sinned more against him<span> * </span>and provoked the most Highest in the wilderness.

<sub>19</sub> They tempted God in their hearts<span> * </span>and required meat for their lust.

<sub>20</sub> They spake against God also, saying<span> * </span>Shall God prepare a table in the wilderness?

<sub>21</sub> He smote the stony rock indeed, that the waters gushed out, and the streams flowed withal<span> * </span>but can he give bread also, or provide flesh for his people?

<sub>22</sub> When the Lord heard this, he was wroth<span> * </span>so the fire was kindled in Jacob, and there came up heavy displeasure against Israel;

<sub>23</sub> Because they believed not in God<span> * </span>and put not their trust in his help.

<sub>24</sub> So he commanded the clouds above<span> * </span>and opened the doors of heaven.

<sub>25</sub> He rained down manna also upon them for to eat<span> * </span>and gave them food from heaven.

<sub>26</sub> So man did eat angels' food<span> * </span>for he sent them meat enough.

<sub>27</sub> He caused the east-wind to blow under heaven<span> * </span>and through his power he brought in the south-west-wind.

<sub>28</sub> He rained flesh upon them as thick as dust<span> * </span>and feathered fowls like as the sand of the sea.

<sub>29</sub> He let it fall among their tents<span> * </span>even round about their habitation.

<sub>30</sub> So they did eat and were well filled, for he gave them their own desire<span> * </span>they were not disappointed of their lust.

<sub>31</sub> But while the meat was yet in their mouths, the heavy wrath of God came upon them, and slew the wealthiest of them<span> * </span>yea, and smote down the chosen men that were in Israel.

<sub>32</sub> But for all this they sinned yet more<span> * </span>and believed not his wondrous works.

<sub>33</sub> Therefore their days did he consume in vanity<span> * </span>and their years in trouble.

<sub>34</sub> When he slew them, they sought him<span> * </span>and turned them early, and inquired after God.

<sub>35</sub> And they remembered that God was their strength<span> * </span>and that the high God was their redeemer.

<sub>36</sub> Nevertheless, they did but flatter him with their mouth<span> * </span>and dissembled with him in their tongue.

<sub>37</sub> For their heart was not whole with him<span> * </span>neither continued they stedfast in his covenant.

<sub>38</sub> But he was so merciful, that he forgave their misdeeds<span> * </span>and destroyed them not.

<sub>39</sub> Yea, many a time turned he his wrath away<span> * </span>and would not suffer his whole displeasure to arise.

<sub>40</sub> For he considered that they were but flesh<span> * </span>and that they were even a wind that passeth away, and cometh not again.

<sub>41</sub> Many a time did they provoke him in the wilderness<span> * </span>and grieved him in the desert.

<sub>42</sub> They turned back, and tempted God<span> * </span>and moved the Holy One in Israel.

<sub>43</sub> They thought not of his hand<span> * </span>and of the day when he delivered them from the hand of the enemy;

<sub>44</sub> How he had wrought his miracles in Egypt<span> * </span>and his wonders in the field of Zoan.

<sub>45</sub> He turned their waters into blood<span> * </span>so that they might not drink of the rivers.

<sub>46</sub> He sent lice among them, and devoured them up<span> * </span>and frogs to destroy them.

<sub>47</sub> He gave their fruit unto the caterpillar<span> * </span>and their labour unto the grasshopper.

<sub>48</sub> He destroyed their vines with hail-stones<span> * </span>and their mulberry-trees with the frost.

#### *49. He smote their cattle also with hail-stones: and their flocks with hot thunderbolts.*
<sub>50</sub> He cast upon them the furiousness of his wrath, anger, displeasure and trouble<span> * </span>and sent evil angels among them.

<sub>51</sub> He made a way to his indignation, and spared not their soul from death<span> * </span>but gave their life over to the pestilence;

<sub>52</sub> And smote all the first-born in Egypt<span> * </span>the most principal and mightiest in the dwellings of Ham.

<sub>53</sub> But as for his own people, he led them forth like sheep<span> * </span>and carried them in the wilderness like a flock.

<sub>54</sub> He brought them out safely, that they should not fear<span> * </span>and overwhelmed their enemies with the sea.

<sub>55</sub> And brought them within the borders of his sanctuary<span> * </span>even to his mountain which he purchased with his right hand.

<sub>56</sub> He cast out the heathen also before them<span> * </span>caused their land to be divided among them for an heritage, and made the tribes of Israel to dwell in their tents.

<sub>57</sub> So they tempted and displeased the most high God<span> * </span>and kept not his testimonies;

<sub>58</sub> But turned their backs, and fell away like their forefathers<span> * </span>starting aside like a broken bow.

<sub>59</sub> For they grieved him with their hill-altars<span> * </span>and provoked him to displeasure with their images.

<sub>60</sub> When God heard this, he was wroth<span> * </span>and took sore displeasure at Israel.

<sub>61</sub> So that he forsook the tabernacle in Silo<span> * </span>even the tent that he had pitched among men.

<sub>62</sub> He delivered their power into captivity<span> * </span>and their beauty into the enemy's hand.

<sub>63</sub> He gave his people over also unto the sword<span> * </span>and was wroth with his inheritance.

<sub>64</sub> The fire consumed their young men<span> * </span>and their maidens were not given to marriage.

<sub>65</sub> Their priests were slain with the sword<span> * </span>and there were no widows to make lamentation.

<sub>66</sub> So the Lord awaked as one out of sleep<span> * </span>and like a giant refreshed with wine.

<sub>67</sub> He smote his enemies in the hinder parts<span> * </span>and put them to a perpetual shame.

<sub>68</sub> He refused the tabernacle of Joseph<span> * </span>and chose not the tribe of Ephraim;

<sub>69</sub> But chose the tribe of Judah<span> * </span>even the hill of Sion which he loved.

<sub>70</sub> And there he built his temple on high<span> * </span>and laid the foundation of it like the ground which he hath made continually.

<sub>71</sub> He chose David also his servant<span> * </span>and took him away from the sheep-folds.

<sub>72</sub> As he was following the ewes great with young ones he took him<span> * </span>that he might feed Jacob his people, and Israel his inheritance.

<sub>73</sub> So he fed them with a faithful and true heart<span> * </span>and ruled them prudently with all his power.

