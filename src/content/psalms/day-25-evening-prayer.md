#### *Manus tuae fecerunt me*
#### *THY hands have made me and fashioned me: O give me understanding, that I may learn thy commandments.*
<sub>74</sub> They that fear thee will be glad when they see me<span> * </span>because I have put my trust in thy word.

<sub>75</sub> I know, O Lord, that thy judgements are right<span> * </span>and that thou of very faithfulness hast caused me to be troubled.

<sub>76</sub> O let thy merciful kindness be my comfort<span> * </span>according to thy word unto thy servant.

<sub>77</sub> O let thy loving mercies come unto me, that I may live<span> * </span>for thy law is my delight.

<sub>78</sub> Let the proud be confounded, for they go wickedly about to destroy me<span> * </span>but I will be occupied in thy commandments.

<sub>79</sub> Let such as fear thee, and have known thy testimonies<span> * </span>be turned unto me.

<sub>80</sub> O let my heart be sound in thy statutes<span> * </span>that I be not ashamed.

#### *Defecit anima mea*
MY SOUL hath longed for thy salvation<span> * </span>and I have a good hope because of thy word.

<sub>82</sub> Mine eyes long sore for thy word<span> * </span>saying, O when wilt thou comfort me?

<sub>83</sub> For I am become like a bottle in the smoke<span> * </span>yet do I not forget thy statutes.

<sub>84</sub> How many are the days of thy servant<span> * </span>when wilt thou be avenged of them that persecute me?

<sub>85</sub> The proud have digged pits for me<span> * </span>which are not after thy law.

<sub>86</sub> All thy commandments are true<span> * </span>they persecute me falsely; O be thou my help.

<sub>87</sub> They had almost made an end of me upon earth<span> * </span>but I forsook not thy commandments.

<sub>88</sub> O quicken me after thy loving-kindness<span> * </span>and so shall I keep the testimonies of thy mouth.

#### *In aeternum. Domine*
OLORD, thy word<span> * </span>endureth for ever in heaven.

<sub>90</sub> Thy truth also remaineth from one generation to another<span> * </span>thou hast laid the foundation of the earth, and it abideth.

<sub>91</sub> They continue this day according to thine ordinance<span> * </span>for all things serve thee.

<sub>92</sub> If my delight had not been in thy law<span> * </span>I should have perished in my trouble.

<sub>93</sub> I will never forget thy commandments<span> * </span>for with them thou hast quickened me.

<sub>94</sub> I am thine, O save me<span> * </span>for I have sought thy commandments.

<sub>95</sub> The ungodly laid wait for me to destroy me<span> * </span>but I will consider thy testimonies.

<sub>96</sub> I see that all things come to an end<span> * </span>but thy commandment is exceeding broad.

#### *Quomodo dilexi!*
LORD, what love have I unto thy law<span> * </span>all the day long is my study in it.

<sub>98</sub> Thou through thy commandments hast made me wiser than mine enemies<span> * </span>for they are ever with me.

<sub>99</sub> I have more understanding than my teachers<span> * </span>for thy testimonies are my study.

<sub>100</sub> I am wiser than the aged<span> * </span>because I keep thy commandments.

<sub>101</sub> I have refrained my feet from every evil way<span> * </span>that I may keep thy word.

<sub>102</sub> I have not shrunk from thy judgements<span> * </span>for thou teachest me.

<sub>103</sub> O how sweet are thy words unto my throat<span> * </span>yea, sweeter than honey unto my mouth.

<sub>104</sub> Through thy commandments I get understanding<span> * </span>therefore I hate all evil ways.

