#### *Psalm 41. Beatus qui intelligit*
BLESSED is he that considereth the poor and needy<span> * </span>the Lord shall deliver him in the time of trouble.

<sub>2</sub> The Lord preserve him, and keep him alive, that he may be blessed upon earth<span> * </span>and deliver not thou him into the will of his enemies.

<sub>3</sub> The Lord comfort him, when he lieth sick upon his bed<span> * </span>make thou all his bed in his sickness.

<sub>4</sub> I said, Lord, be merciful unto me<span> * </span>heal my soul, for I have sinned against thee.

<sub>5</sub> Mine enemies speak evil of me<span> * </span>When shall he die, and his name perish?

<sub>6</sub> And if he come to see me, he speaketh vanity<span> * </span>and his heart conceiveth falsehood within himself, and when he cometh forth he telleth it.

<sub>7</sub> All mine enemies whisper together against me<span> * </span>even against me do they imagine this evil.

<sub>8</sub> Let the sentence of guiltiness proceed against him<span> * </span>and now that he lieth, let him rise up no more.

<sub>9</sub> Yea, even mine own familiar friend, whom I trusted<span> * </span>who did also eat of my bread, hath laid great wait for me.

<sub>10</sub> But be thou merciful unto me, O Lord<span> * </span>raise thou me up again, and I shall reward them.

<sub>11</sub> By this I know thou favourest me<span> * </span>that mine enemy doth not triumph against me.

<sub>12</sub> And when I am in my health, thou upholdest me<span> * </span>and shalt set me before thy face for ever.

<sub>13</sub> Blessed be the Lord God of Israel<span> * </span>world without end. Amen.

#### *Psalm 42. Quemadmodum*
LIKE as the hart desireth the water-brooks<span> * </span>so longeth my soul after thee, O God.

<sub>2</sub> My soul is athirst for God, yea, even for the living God<span> * </span>when shall I come to appear before the presence of God?

<sub>3</sub> My tears have been my meat day and night<span> * </span>while they daily say unto me, Where is now thy God?

<sub>4</sub> Now when I think thereupon, I pour out my heart by myself<span> * </span>for I went with the multitude, and brought them forth into the house of God;

<sub>5</sub> In the voice of praise and thanksgiving<span> * </span>among such as keep holy-day.

<sub>6</sub> Why art thou so full of heaviness, O my soul<span> * </span>and why art thou so disquieted within me?

<sub>7</sub> Put thy trust in God<span> * </span>for I will yet give him thanks for the help of his countenance.

<sub>8</sub> My God, my soul is vexed within me<span> * </span>therefore will I remember thee concerning the land of Jordan, and the little hill of Hermon.

<sub>9</sub> One deep calleth another, because of the noise of the water-pipes<span> * </span>all thy waves and storms are gone over me.

<sub>10</sub> The Lord hath granted his loving-kindness in the day-time<span> * </span>and in the night-season did I sing of him, and made my prayer unto the God of my life.

<sub>11</sub> I will say unto the God of my strength, Why hast thou forgotten me<span> * </span>why go I thus heavily, while the enemy oppresseth me?

<sub>12</sub> My bones are smitten asunder as with a sword<span> * </span>while mine enemies that trouble me cast me in the teeth;

<sub>13</sub> Namely, while they say daily unto me<span> * </span>Where is now thy God?

<sub>14</sub> Why art thou so vexed, O my soul<span> * </span>and why art thou so disquieted within me?

<sub>15</sub> O put thy trust in God<span> * </span>for I will yet thank him, which is the help of my countenance, and my God.

#### *Psalm 43. Judica me, Deus*
GIVE sentence with me, O God, and defend my cause against the ungodly people<span> * </span>O deliver me from the deceitful and wicked man.

<sub>2</sub> For thou art the God of my strength, why hast thou put me from thee<span> * </span>and why go I so heavily, while the enemy oppresseth me?

<sub>3</sub> O send out thy light and thy truth, that they may lead me<span> * </span>and bring me unto thy holy hill, and to thy dwelling.

<sub>4</sub> And that I may go unto the altar of God, even unto the God of my joy and gladness<span> * </span>and upon the harp will I give thanks unto thee, O God, my God.

<sub>5</sub> Why art thou so heavy, O my soul<span> * </span>and why art thou so disquieted within me?

<sub>6</sub> O put thy trust in God<span> * </span>for I will yet give him thanks, which is the help of my countenance, and my God.

