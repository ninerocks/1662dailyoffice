#### *Psalm 69. Salvum me fac*
SAVE me, O God<span> * </span>for the waters are come in, even unto my soul.

<sub>2</sub> I stick fast in the deep mire, where no ground is<span> * </span>I am come into deep waters, so that the floods run over me.

<sub>3</sub> I am weary of crying; my throat is dry<span> * </span>my sight faileth me for waiting so long upon my God.

<sub>4</sub> They that hate me without a cause are more than the hairs of my head<span> * </span>they that are mine enemies, and would destroy me guiltless, are mighty.

<sub>5</sub> I paid them the things that I never took<span> * </span>God, thou knowest my simpleness, and my faults are not hid from thee.

<sub>6</sub> Let not them that trust in thee, O Lord God of hosts, be ashamed for my cause<span> * </span>let not those that seek thee be confounded through me, O Lord God of Israel.

<sub>7</sub> And why? for thy sake have I suffered reproof<span> * </span>shame hath covered my face.

<sub>8</sub> I am become a stranger unto my brethren<span> * </span>even an alien unto my mother's children.

<sub>9</sub> For the zeal of thine house hath even eaten me<span> * </span>and the rebukes of them that rebuked thee are fallen upon me.

<sub>10</sub> I wept, and chastened myself with fasting<span> * </span>and that was turned to my reproof.

<sub>11</sub> I put on sackcloth also<span> * </span>and they jested upon me.

<sub>12</sub> They that sit in the gate speak against me<span> * </span>and the drunkards make songs upon me.

<sub>13</sub> But, Lord, I make my prayer unto thee<span> * </span>in an acceptable time.

<sub>14</sub> Hear me, O God, in the multitude of thy mercy<span> * </span>even in the truth of thy salvation.

<sub>15</sub> Take me out of the mire, that I sink not<span> * </span>O let me be delivered from them that hate me, and out of the deep waters.

<sub>16</sub> Let not the water-flood drown me, neither let the deep swallow me up<span> * </span>and let not the pit shut her mouth upon me.

<sub>17</sub> Hear me, O Lord, for thy loving-kindness is comfortable<span> * </span>turn thee unto me according to the multitude of thy mercies.

<sub>18</sub> And hide not thy face from thy servant, for I am in trouble<span> * </span>O haste thee, and hear me.

<sub>19</sub> Draw nigh unto my soul, and save it<span> * </span>O deliver me, because of mine enemies.

<sub>20</sub> Thou hast known my reproof, my shame, and my dishonour<span> * </span>mine adversaries are all in thy sight.

<sub>21</sub> Thy rebuke hath broken my heart; I am full of heaviness<span> * </span>I looked for some to have pity on me, but there was no man, neither found I any to comfort me.

<sub>22</sub> They gave me gall to eat<span> * </span>and when I was thirsty they gave me vinegar to drink.

<sub>23</sub> Let their table be made a snare to take themselves withal<span> * </span>and let the things that should have been for their wealth be unto them an occasion of falling.

<sub>24</sub> Let their eyes be blinded, that they see not<span> * </span>and ever bow thou down their backs.

<sub>25</sub> Pour out thine indignation upon them<span> * </span>and let thy wrathful displeasure take hold of them.

<sub>26</sub> Let their habitation be void<span> * </span>and no man to dwell in their tents.

<sub>27</sub> For they persecute him whom thou hast smitten<span> * </span>and they talk how they may vex them whom thou hast wounded.

<sub>28</sub> Let them fall from one wickedness to another<span> * </span>and not come into thy righteousness.

<sub>29</sub> Let them be wiped out of the book of the living<span> * </span>and not be written among the righteous.

<sub>30</sub> As for me, when I am poor and in heaviness<span> * </span>thy help, O God, shall lift me up.

<sub>31</sub> I will praise the Name of God with a song<span> * </span>and magnify it with thanksgiving.

<sub>32</sub> This also shall please the Lord<span> * </span>better than a bullock that hath horns and hoofs.

<sub>33</sub> The humble shall consider this, and be glad<span> * </span>seek ye after God, and your soul shall live.

<sub>34</sub> For the Lord heareth the poor<span> * </span>and despiseth not his prisoners.

<sub>35</sub> Let heaven and earth praise him<span> * </span>the sea, and all that moveth therein.

<sub>36</sub> For God will save Sion, and build the cities of Judah<span> * </span>that men may dwell there, and have it in possession.

<sub>37</sub> The posterity also of his servants shall inherit it<span> * </span>and they that love his Name shall dwell therein.

#### *Psalm 70. Deus, in adjutorium*
HASTE thee, O God, to deliver me<span> * </span>make haste to help me, O Lord.

<sub>2</sub> Let them be ashamed and confounded that seek after my soul<span> * </span>let them be turned backward and put to confusion that wish me evil.

<sub>3</sub> Let them for their reward be soon brought to shame<span> * </span>that cry over me, There, there.

<sub>4</sub> But let all those that seek thee be joyful and glad in thee<span> * </span>and let all such as delight in thy salvation say alway, The Lord be praised.

<sub>5</sub> As for me, I am poor and in misery<span> * </span>haste thee unto me, O God.

<sub>6</sub> Thou art my helper and my redeemer<span> * </span>O Lord, make no long tarrying.

