#### *Psalm 107. Confitemini Domino*
OGIVE thanks unto the Lord, for he is gracious<span> * </span>and his mercy endureth for ever.

<sub>2</sub> Let them give thanks whom the Lord hath redeemed<span> * </span>and delivered from the hand of the enemy;

<sub>3</sub> And gathered them out of the lands, from the east and from the west<span> * </span>from the north and from the south.

<sub>4</sub> They went astray in the wilderness out of the way<span> * </span>and found no city to dwell in;

<sub>5</sub> Hungry and thirsty<span> * </span>their soul fainted in them.

<sub>6</sub> So they cried unto the Lord in their trouble<span> * </span>and he delivered them from their distress.

<sub>7</sub> He led them forth by the right way<span> * </span>that they might go to the city where they dwelt.

<sub>8</sub> O that men would therefore praise the Lord for his goodness<span> * </span>and declare the wonders that he doeth for the children of men!

<sub>9</sub> For he satisfieth the empty soul<span> * </span>and filleth the hungry soul with goodness.

<sub>10</sub> Such as sit in darkness, and in the shadow of death<span> * </span>being fast bound in misery and iron ;

<sub>11</sub> Because they rebelled against the words of the Lord<span> * </span>and lightly regarded the counsel of the most Highest;

<sub>12</sub> He also brought down their heart through heaviness<span> * </span>they fell down, and there was none to help them.

<sub>13</sub> So when they cried unto the Lord in their trouble<span> * </span>he delivered them out of their distress.

<sub>14</sub> For he brought them out of darkness, and out of the shadow of death<span> * </span>and brake their bonds in sunder.

<sub>15</sub> O that men would therefore praise the Lord for his goodness<span> * </span>and declare the wonders that he doeth for the children of men!

<sub>16</sub> For he hath broken the gates of brass<span> * </span>and smitten the bars of iron in sunder.

<sub>17</sub> Foolish men are plagued for their offence<span> * </span>and because of their wickedness.

<sub>18</sub> Their soul abhorred all manner of meat<span> * </span>and they were even hard at death's door.

<sub>19</sub> So when they cried unto the Lord in their trouble<span> * </span>he delivered them out of their distress.

<sub>20</sub> He sent his word, and healed them<span> * </span>and they were saved from their destruction.

<sub>21</sub> O that men would therefore praise the Lord for his goodness<span> * </span>and declare the wonders that he doeth for the children of men!

<sub>22</sub> That they would offer unto him the sacrifice of thanksgiving<span> * </span>and tell out his works with gladness!

<sub>23</sub> They that go down to the sea in ships<span> * </span>and occupy their business in great waters;

<sub>24</sub> These men see the works of the Lord<span> * </span>and his wonders in the deep.

<sub>25</sub> For at his word the stormy wind ariseth<span> * </span>which lifteth up the waves thereof.

<sub>26</sub> They are carried up to the heaven, and down again to the deep<span> * </span>their soul melteth away because of the trouble.

<sub>27</sub> They reel to and fro, and stagger like a drunken man<span> * </span>and are at their wits' end.

<sub>28</sub> So when they cry unto the Lord in their trouble<span> * </span>he delivereth them out of their distress.

<sub>29</sub> For he maketh the storm to cease<span> * </span>so that the waves thereof are still.

<sub>30</sub> Then are they glad, because they are at rest<span> * </span>and so he bringeth them unto the haven where they would be.

<sub>31</sub> O that men would therefore praise the Lord for his goodness<span> * </span>and declare the wonders that he doeth for the children of men!

<sub>32</sub> That they would exalt him also in the congregation of the people<span> * </span>and praise him in the seat of the elders!

<sub>33</sub> Who turneth the floods into a wilderness<span> * </span>and drieth up the water-springs.

<sub>34</sub> A fruitful land maketh he barren<span> * </span>for the wickedness of them that dwell therein.

<sub>35</sub> Again, he maketh the wilderness a standing water<span> * </span>and water-springs of a dry ground.

<sub>36</sub> And there he setteth the hungry<span> * </span>that they may build them a city to dwell in;

<sub>37</sub> That they may sow their land, and plant vineyards<span> * </span>to yield them fruits of increase.

<sub>38</sub> He blesseth them so that they multiply exceedingly<span> * </span>and suffereth not their cattle to decrease.

<sub>39</sub> And again, when they are minished and brought low<span> * </span>through oppression, through any plague or trouble;

<sub>40</sub> Though he suffer them to be evil intreated through tyrants<span> * </span>and let them wander out of the way in the wilderness;

<sub>41</sub> Yet helpeth he the poor out of misery<span> * </span>and maketh him households like a flock of sheep.

<sub>42</sub> The righteous will consider this, and rejoice<span> * </span>and the mouth of all wickedness shall be stopped.

<sub>43</sub> Whoso is wise will ponder these things<span> * </span>and they shall understand the loving-kindness of the Lord.

