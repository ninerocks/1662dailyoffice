#### *Psalm 37. Noli aemulari*
FRET not thyself because of the ungodly<span> * </span>neither be thou envious against the evil-doers.

<sub>2</sub> For they shall soon be cut down like the grass<span> * </span>and be withered even as the green herb.

<sub>3</sub> Put thou thy trust in the Lord, and be doing good<span> * </span>dwell in the land, and verily thou shalt be fed.

<sub>4</sub> Delight thou in the Lord<span> * </span>and he shall give thee thy heart's desire.

<sub>5</sub> Commit thy way unto the Lord, and put thy trust in him<span> * </span>and he shall bring it to pass.

<sub>6</sub> He shall make thy righteousness as clear as the light<span> * </span>and thy just dealing as the noon-day.

<sub>7</sub> Hold thee still in the Lord, and abide patiently upon him<span> * </span>but grieve not thyself at him whose way doth prosper, against the man that doeth after evil counsels.

<sub>8</sub> Leave off from wrath, and let go displeasure<span> * </span>fret not thyself, else shalt thou be moved to do evil.

<sub>9</sub> Wicked doers shall be rooted out<span> * </span>and they that patiently abide the Lord, those shall inherit the land.

<sub>10</sub> Yet a little while, and the ungodly shall be clean gone<span> * </span>thou shalt look after his place, and he shall be away.

<sub>11</sub> But the meek-spirited shall possess the earth<span> * </span>and shall be refreshed in the multitude of peace.

<sub>12</sub> The ungodly seeketh counsel against the just<span> * </span>and gnasheth upon him with his teeth.

<sub>13</sub> The Lord shall laugh him to scorn<span> * </span>for he hath seen that his day is coming.

<sub>14</sub> The ungodly have drawn out the sword, and have bent their bow<span> * </span>to cast down the poor and needy, and to slay such as are of a right conversation.

<sub>15</sub> Their sword shall go through their own heart<span> * </span>and their bow shall be broken.

<sub>16</sub> A small thing that the righteous hath<span> * </span>is better than great riches of the ungodly.

<sub>17</sub> For the arms of the ungodly shall be broken<span> * </span>and the Lord upholdeth the righteous.

<sub>18</sub> The Lord knoweth the days of the godly<span> * </span>and their inheritance shall endure for ever.

<sub>19</sub> They shall not be confounded in the perilous time<span> * </span>and in the days of dearth they shall have enough.

<sub>20</sub> As for the ungodly, they shall perish; and the enemies of the Lord shall consume as the fat of lambs<span> * </span>yea, even as the smoke shall they consume away.

<sub>21</sub> The ungodly borroweth, and payeth not again<span> * </span>but the righteous is merciful and liberal.

<sub>22</sub> Such as are blessed of God shall possess the land<span> * </span>and they that are cursed of him shall be rooted out.

<sub>23</sub> The Lord ordereth a good man's going<span> * </span>and maketh his way acceptable to himself.

<sub>24</sub> Though he fall, he shall not be cast away<span> * </span>for the Lord upholdeth him with his hand.

<sub>25</sub> I have been young, and now am old<span> * </span>and yet saw I never the righteous forsaken, nor his seed begging their bread.

<sub>26</sub> The righteous is ever merciful, and lendeth<span> * </span>and his seed is blessed.

<sub>27</sub> Flee from evil, and do the thing that is good<span> * </span>and dwell for evermore.

<sub>28</sub> For the Lord loveth the thing that is right<span> * </span>he forsaketh not his that be godly, but they are preserved for ever.

<sub>29</sub> The unrighteous shall be punished<span> * </span>as for the seed of the ungodly, it shall be rooted out.

<sub>30</sub> The righteous shall inherit the land<span> * </span>and dwell therein for ever.

<sub>31</sub> The mouth of the righteous is exercised in wisdom<span> * </span>and his tongue will be talking of judgement.

<sub>32</sub> The law of his God is in his heart<span> * </span>and his goings shall not slide.

<sub>33</sub> The ungodly seeth the righteous<span> * </span>and seeketh occasion to slay him.

<sub>34</sub> The Lord will not leave him in his hand<span> * </span>nor condemn him when he is judged.

<sub>35</sub> Hope thou in the Lord, and keep his way, and he shall promote thee, that thou shalt possess the land<span> * </span>when the ungodly shall perish, thou shalt see it.

<sub>36</sub> I myself have seen the ungodly in great power<span> * </span>and flourishing like a green bay-tree.

<sub>37</sub> I went by, and lo, he was gone<span> * </span>I sought him, but his place could no where be found.

<sub>38</sub> Keep innocency, and take heed unto the thing that is right<span> * </span>for that shall bring a man peace at the last.

<sub>39</sub> As for the transgressors, they shall perish together<span> * </span>and the end of the ungodly is, they shall be rooted out at the last.

<sub>40</sub> But the salvation of the righteous cometh of the Lord<span> * </span>who is also their strength in the time of trouble.

<sub>41</sub> And the Lord shall stand by them, and save them<span> * </span>he shall deliver them from the ungodly, and shall save them, because they put their trust in him.

