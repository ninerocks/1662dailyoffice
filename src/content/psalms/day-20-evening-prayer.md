#### *Psalm 104. Benedic, anima mea*
PRAISE the Lord, O my soul<span> * </span>O Lord my God, thou art become exceeding glorious; thou art clothed with majesty and honour.

<sub>2</sub> Thou deckest thyself with light as it were with a garment<span> * </span>and spreadest out the heavens like a curtain.

<sub>3</sub> Who layeth the beams of his chambers in the waters<span> * </span>and maketh the clouds his chariot, and walketh upon the wings of the wind.

<sub>4</sub> He maketh his angels spirits<span> * </span>and his ministers a flaming fire.

<sub>5</sub> He laid the foundations of the earth<span> * </span>that it never should move at any time.

<sub>6</sub> Thou coveredst it with the deep like as with a garment<span> * </span>the waters stand in the hills.

<sub>7</sub> At thy rebuke they flee<span> * </span>at the voice of thy thunder they are afraid.

<sub>8</sub> They go up as high as the hills, and down to the valleys beneath<span> * </span>even unto the place which thou hast appointed for them.

<sub>9</sub> Thou hast set them their bounds which they shall not pass<span> * </span>neither turn again to cover the earth.

<sub>10</sub> He sendeth the springs into the rivers<span> * </span>which run among the hills.

<sub>11</sub> All beasts of the field drink thereof<span> * </span>and the wild asses quench their thirst.

<sub>12</sub> Beside them shall the fowls of the air have their habitation<span> * </span>and sing among the branches.

<sub>13</sub> He watereth the hills from above<span> * </span>the earth is filled with the fruit of thy works.

<sub>14</sub> He bringeth forth grass for the cattle<span> * </span>and green herb for the service of men;

<sub>15</sub> That he may bring food out of the earth, and wine that maketh glad the heart of man<span> * </span>and oil to make him a cheerful countenance, and bread to strengthen man's heart.

<sub>16</sub> The trees of the Lord also are full of sap<span> * </span>even the cedars of Libanus which he hath planted;

<sub>17</sub> Wherein the birds make their nests<span> * </span>and the fir-trees are a dwelling for the stork.

<sub>18</sub> The high hills are a refuge for the wild goats<span> * </span>and so are the stony rocks for the conies.

<sub>19</sub> He appointed the moon for certain seasons<span> * </span>and the sun knoweth his going down.

<sub>20</sub> Thou makest darkness that it may be night<span> * </span>wherein all the beasts of the forest do move.

<sub>21</sub> The lions roaring after their prey<span> * </span>do seek their meat from God.

<sub>22</sub> The sun ariseth, and they get them away together<span> * </span>and lay them down in their dens.

<sub>23</sub> Man goeth forth to his work, and to his labour<span> * </span>until the evening.

<sub>24</sub> O Lord, how manifold are thy works<span> * </span>in wisdom hast thou made them all; the earth is full of thy riches.

<sub>25</sub> So is the great and wide sea also<span> * </span>wherein are things creeping innumerable, both small and great beasts.

<sub>26</sub> There go the ships, and there is that Leviathan<span> * </span>whom thou hast made to take his pastime therein.

<sub>27</sub> These wait all upon thee<span> * </span>that thou mayest give them meat in due season.

<sub>28</sub> When thou givest it them they gather it<span> * </span>and when thou openest thy hand they are filled with good.

<sub>29</sub> When thou hidest thy face they are troubled<span> * </span>when thou takest away their breath they die, and are turned again to their dust.

<sub>30</sub> When thou lettest thy breath go forth they shall be made<span> * </span>and thou shalt renew the face of the earth.

<sub>31</sub> The glorious majesty of the Lord shall endure for ever<span> * </span>the Lord shall rejoice in his works.

<sub>32</sub> The earth shall tremble at the look of him<span> * </span>if he do but touch the hills, they shall smoke.

<sub>33</sub> I will sing unto the Lord as long as I live<span> * </span>I will praise my God while I have my being.

<sub>34</sub> And so shall my words please him<span> * </span>my joy shall be in the Lord.

<sub>35</sub> As for sinners, they shall be consumed out of the earth, and the ungodly shall come to an end<span> * </span>praise thou the Lord, O my soul, praise the Lord.

