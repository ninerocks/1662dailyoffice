#### *Psalm 102. Domine, exaudi*
HEAR my prayer, O Lord<span> * </span>and let my crying come unto thee.

<sub>2</sub> Hide not thy face from me in the time of my trouble<span> * </span>incline thine ear unto me when I call; O hear me, and that right soon.

<sub>3</sub> For my days are consumed away like smoke<span> * </span>and my bones are burnt up as it were a firebrand.

<sub>4</sub> My heart is smitten down, and withered liked grass<span> * </span>so that I forget to eat my bread.

<sub>5</sub> For the voice of my groaning<span> * </span>my bones will scarce cleave to my flesh.

<sub>6</sub> I am become like a pelican in the wilderness<span> * </span>and like an owl that is in the desert.

<sub>7</sub> I have watched, and am even as it were a sparrow<span> * </span>that sitteth alone upon the house-top.

<sub>8</sub> Mine enemies revile me all the day long<span> * </span>and they that are mad upon me are sworn together against me.

<sub>9</sub> For I have eaten ashes as it were bread<span> * </span>and mingled my drink with weeping;

<sub>10</sub> And that because of thine indignation and wrath<span> * </span>for thou hast taken me up, and cast me down.

<sub>11</sub> My days are gone like a shadow<span> * </span>and I am withered like grass.

<sub>12</sub> But thou, O Lord, shalt endure for ever<span> * </span>and thy remembrance throughout all generations.

<sub>13</sub> Thou shalt arise, and have mercy upon Sion<span> * </span>for it is time that thou have mercy upon her, yea, the time is come.

<sub>14</sub> And why? thy servants think upon her stones<span> * </span>and it pitieth them to see her in the dust.

<sub>15</sub> The heathen shall fear thy Name, O Lord<span> * </span>and all the kings of the earth thy majesty;

<sub>16</sub> When the Lord shall build up Sion<span> * </span>and when his glory shall appear;

<sub>17</sub> When he turneth him unto the prayer of the poor destitute<span> * </span>and despiseth not their desire.

<sub>18</sub> This shall be written for those that come after<span> * </span>and the people which shall be born shall praise the Lord.

<sub>19</sub> For he hath looked down from his sanctuary<span> * </span>out of the heaven did the Lord behold the earth;

<sub>20</sub> That he might hear the mournings of such as are in captivity<span> * </span>and deliver the children appointed unto death;

<sub>21</sub> That they may declare the Name of the Lord in Sion<span> * </span>and his worship at Jerusalem;

<sub>22</sub> When the people are gathered together<span> * </span>and the kingdoms also, to serve the Lord.

<sub>23</sub> He brought down my strength in my journey<span> * </span>and shortened my days.

<sub>24</sub> But I said, O my God, take me not away in the midst of mine age<span> * </span>as for thy years, they endure throughout all generations.

<sub>25</sub> Thou, Lord, in the beginning hast laid the foundation of the earth<span> * </span>and the heavens are the work of thy hands.

<sub>26</sub> They shall perish, but thou shalt endure<span> * </span>they all shall wax old as doth a garment;

<sub>27</sub> And as a vesture shalt thou change them, and they shall be changed<span> * </span>but thou art the same, and thy years shall not fail.

<sub>28</sub> The children of thy servants shall continue<span> * </span>and their seed shall stand fast in thy sight.

#### *Psalm 103. Benedic, anima mea*
PRAISE the Lord, O my soul<span> * </span>and all that is within me praise his holy Name.

<sub>2</sub> Praise the Lord, O my soul<span> * </span>and forget not all his benefits;

<sub>3</sub> Who forgiveth all thy sin<span> * </span>and healeth all thine infirmities;

<sub>4</sub> Who saveth thy life from destruction<span> * </span>and crowneth thee with mercy and loving-kindness;

<sub>5</sub> Who satisfieth thy mouth with good things<span> * </span>making thee young and lusty as an eagle.

<sub>6</sub> The Lord executeth righteousness and judgement<span> * </span>for all them that are oppressed with wrong.

<sub>7</sub> He shewed his ways unto Moses<span> * </span>his works unto the children of Israel.

<sub>8</sub> The Lord is full of compassion and mercy<span> * </span>long-suffering, and of great goodness.

<sub>9</sub> He will not alway be chiding<span> * </span>neither keepeth he his anger for ever.

<sub>10</sub> He hath not dealt with us after our sins<span> * </span>nor rewarded us according to our wickednesses.

<sub>11</sub> For look how high the heaven is in comparison of the earth<span> * </span>so great is his mercy also toward them that fear him.

<sub>12</sub> Look how wide also the east is from the west<span> * </span>so far hath he set our sins from us.

<sub>13</sub> Yea, like as a father pitieth his own children<span> * </span>even so is the Lord merciful unto them that fear him.

<sub>14</sub> For he knoweth whereof we are made<span> * </span>he remembereth that we are but dust.

<sub>15</sub> The days of man are but as grass<span> * </span>for he flourisheth as a flower of the field.

<sub>16</sub> For as soon as the wind goeth over it, it is gone<span> * </span>and the place thereof shall know it no more.

<sub>17</sub> But the merciful goodness of the Lord endureth for ever and ever upon them that fear him<span> * </span>and his righteousness upon children's children;

<sub>18</sub> Even upon such as keep his covenant<span> * </span>and think upon his commandments to do them.

<sub>19</sub> The Lord hath prepared his seat in heaven<span> * </span>and his kingdom ruleth over all.

<sub>20</sub> O praise the Lord, ye angels of his, ye that excel in strength<span> * </span>ye that fulfil his commandment, and hearken unto the voice of his words.

<sub>21</sub> O praise the Lord, all ye his hosts<span> * </span>ye servants of his that do his pleasure.

<sub>22</sub> O speak good of the Lord, all ye works of his, in all places of his dominion<span> * </span>praise thou the Lord, O my soul.

