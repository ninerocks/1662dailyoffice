#### *Psalm 89. Misericordias Domini*
MY SONG shall be alway of the loving-kindness of the Lord<span> * </span>with my mouth will I ever be shewing thy truth from one generation to another.

<sub>2</sub> For I have said, Mercy shall be set up for ever<span> * </span>thy truth shalt thou stablish in the heavens.

<sub>3</sub> I have made a covenant with my chosen<span> * </span>I have sworn unto David my servant;

<sub>4</sub> Thy seed will I stablish for ever<span> * </span>and set up thy throne from one generation to another.

<sub>5</sub> O Lord, the very heavens shall praise thy wondrous works<span> * </span>and thy truth in the congregation of the saints.

<sub>6</sub> For who is he among the clouds<span> * </span>that shall be compared unto the Lord?

<sub>7</sub> And what is he among the gods<span> * </span>that shall be like unto the Lord?

<sub>8</sub> God is very greatly to be feared in the council of the saints<span> * </span>and to be had in reverence of all them that are round about him.

<sub>9</sub> O Lord God of hosts, who is like unto thee<span> * </span>thy truth, most mighty Lord, is on every side.

<sub>10</sub> Thou rulest the raging of the sea<span> * </span>thou stillest the waves thereof when they arise.

<sub>11</sub> Thou hast subdued Egypt, and destroyed it<span> * </span>thou hast scattered thine enemies abroad with thy mighty arm.

<sub>12</sub> The heavens are thine, the earth also is thine<span> * </span>thou hast laid the foundation of the round world, and all that therein is.

<sub>13</sub> Thou hast made the north and the south<span> * </span>Tabor and Hermon shall rejoice in thy Name.

<sub>14</sub> Thou hast a mighty arm<span> * </span>strong is thy hand, and high is thy right hand.

<sub>15</sub> Righteousness and equity are the habitation of thy seat<span> * </span>mercy and truth shall go before thy face.

<sub>16</sub> Blessed is the people, O Lord, that can rejoice in thee<span> * </span>they shall walk in the light of thy countenance.

<sub>17</sub> Their delight shall be daily in thy Name<span> * </span>and in thy righteousness shall they make their boast.

<sub>18</sub> For thou art the glory of their strength<span> * </span>and in thy loving-kindness thou shalt lift up our horns.

<sub>19</sub> For the Lord is our defence<span> * </span>the Holy One of Israel is our King.

<sub>20</sub> Thou spakest sometime in visions unto thy saints, and saidst<span> * </span>I have laid help upon one that is mighty; I have exalted one chosen out of the people.

<sub>21</sub> I have found David my servant<span> * </span>with my holy oil have I anointed him.

<sub>22</sub> My hand shall hold him fast<span> * </span>and my arm shall strengthen him.

<sub>23</sub> The enemy shall not be able to do him violence<span> * </span>the son of wickedness shall not hurt him.

<sub>24</sub> I will smite down his foes before his face<span> * </span>and plague them that hate him.

<sub>25</sub> My truth also and my mercy shall be with him<span> * </span>and in my Name shall his horn be exalted.

<sub>26</sub> I will set his dominion also in the sea<span> * </span>and his right hand in the floods.

<sub>27</sub> He shall call me, Thou art my Father<span> * </span>my God, and my strong salvation.

<sub>28</sub> And I will make him my first-born<span> * </span>higher than the kings of the earth.

<sub>29</sub> My mercy will I keep for him for evermore<span> * </span>and my covenant shall stand fast with him.

<sub>30</sub> His seed also will I make to endure for ever<span> * </span>and his throne as the days of heaven.

<sub>31</sub> But if his children forsake my law<span> * </span>and walk not in my judgements;

<sub>32</sub> If they break my statutes, and keep not my commandments<span> * </span>I will visit their offences with the rod, and their sin with scourges.

<sub>33</sub> Nevertheless, my loving-kindness will I not utterly take from him<span> * </span>nor suffer my truth to fail.

<sub>34</sub> My covenant I will not break, nor alter the thing that is gone out of my lips<span> * </span>I have sworn once by my holiness, that I will not fail David.

<sub>35</sub> His seed shall endure for ever<span> * </span>and his seat is like as the sun before me.

<sub>36</sub> He shall stand fast for evermore as the moon<span> * </span>and as the faithful witness in heaven.

<sub>37</sub> But thou hast abhorred and forsaken thine Anointed<span> * </span>and art displeased at him.

<sub>38</sub> Thou hast broken the covenant of thy servant<span> * </span>and cast his crown to the ground.

<sub>39</sub> Thou hast overthrown all his hedges<span> * </span>and broken down his strong holds.

<sub>40</sub> All they that go by spoil him<span> * </span>and he is become a reproach to his neighbours.

<sub>41</sub> Thou hast set up the right hand of his enemies<span> * </span>and made all his adversaries to rejoice.

<sub>42</sub> Thou hast taken away the edge of his sword<span> * </span>and givest him not victory in the battle.

<sub>43</sub> Thou hast put out his glory<span> * </span>and cast his throne down to the ground.

<sub>44</sub> The days of his youth hast thou shortened<span> * </span>and covered him with dishonour.

<sub>45</sub> Lord, how long wilt thou hide thyself, for ever<span> * </span>and shall thy wrath burn like fire?

<sub>46</sub> O remember how short my time is<span> * </span>wherefore hast thou made all men for nought?

<sub>47</sub> What man is he that liveth, and shall not see death<span> * </span>and shall he deliver his soul from the hand of hell?

<sub>48</sub> Lord, where are thy old loving-kindnesses<span> * </span>which thou swarest unto David in thy truth?

<sub>49</sub> Remember, Lord, the rebuke that thy servants have<span> * </span>and how I do bear in my bosom the rebukes of many people.

<sub>50</sub> Wherewith thine enemies have blasphemed thee, and slandered the footsteps of thine Anointed<span> * </span>Praised be the Lord for evermore. Amen, and Amen.

