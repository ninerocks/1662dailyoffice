#### *Psalm 59. Eripe me de inimicis*
DELIVER me from mine enemies, O God<span> * </span>defend me from them that rise up against me.

<sub>2</sub> O deliver me from the wicked doers<span> * </span>and save me from the blood-thirsty men.

<sub>3</sub> For lo, they lie waiting for my soul<span> * </span>the mighty men are gathered against me, without any offence or fault of me, O Lord.

<sub>4</sub> They run and prepare themselves without my fault<span> * </span>arise thou therefore to help me, and behold.

<sub>5</sub> Stand up, O Lord God of hosts, thou God of Israel, to visit all the heathen<span> * </span>and be not merciful unto them that offend of malicious wickedness.

<sub>6</sub> They go to and fro in the evening<span> * </span>they grin like a dog, and run about through the city.

<sub>7</sub> Behold, they speak with their mouth, and swords are in their lips<span> * </span>for who doth hear?

<sub>8</sub> But thou. O Lord, shalt have them in derision<span> * </span>and thou shalt laugh all the heathen to scorn.

<sub>9</sub> My strength will I ascribe unto thee<span> * </span>for thou art the God of my refuge.

<sub>10</sub> God sheweth me his goodness plenteously<span> * </span>and God shall let me see my desire upon mine enemies.

<sub>11</sub>Slay them not, lest my people forget it<span> * </span>but scatter them abroad among the people, and put them down, O Lord, our defence.

<sub>12</sub> For the sin of their mouth, and for the words of their lips, they shall be taken in their pride<span> * </span>and why? their preaching is of cursing and lies.

<sub>13</sub> Consume them in thy wrath, consume them, that they may perish<span> * </span>and know that it is God that ruleth in Jacob, and unto the ends of the world.

<sub>14</sub> And in the evening they will return<span> * </span>grin like a dog, and will go about the city.

<sub>15</sub> They will run here and there for meat<span> * </span>and grudge if they be not satisfied.

<sub>16</sub> As for me, I will sing of thy power, and will praise thy mercy betimes in the morning<span> * </span>for thou hast been my defence and refuge in the day of my trouble.

<sub>17</sub> Unto thee, O my strength, will I sing<span> * </span>for thou, O God, art my refuge, and my merciful God.

#### *Psalm 60. Deus, repulisti nos*
OGOD, thou hast cast us out, and scattered us abroad<span> * </span>thou hast also been displeased; O turn thee unto us again.

<sub>2</sub> Thou hast moved the land, and divided it<span> * </span>heal the sores thereof, for it shaketh.

<sub>3</sub> Thou hast shewed thy people heavy things<span> * </span>thou hast given us a drink of deadly wine.

<sub>4</sub> Thou hast given a token for such as fear thee<span> * </span>that they may triumph because of the truth.

<sub>5</sub> Therefore were thy beloved delivered<span> * </span>help me with thy right hand, and hear me.

<sub>6</sub> God hath spoken in his holiness, I will rejoice, and divide Sichem<span> * </span>and mete out the valley of Succoth.

<sub>7</sub> Gilead is mine, and Manasses is mine<span> * </span>Ephraim also is the strength of my head; Judah is my law-giver;

<sub>8</sub> Moab is my wash-pot; over Edom will I cast out my shoe<span> * </span>Philistia, be thou glad of me.

<sub>9</sub> Who will lead me into the strong city<span> * </span>who will bring me into Edom?

<sub>10</sub> Hast not thou cast us out, O God<span> * </span>wilt not thou, O God, go out with our hosts?

<sub>11</sub> O be thou our help in trouble<span> * </span>for vain is the help of man.

<sub>12</sub> Through God will we do great acts<span> * </span>for it is he that shall tread down our enemies.

#### *Psalm 61. Exaudi, Deus*
HEAR my crying, O God<span> * </span>give ear unto my prayer.

<sub>2</sub> From the ends of the earth will I call upon thee<span> * </span>when my heart is in heaviness.

<sub>3</sub> O set me up upon the rock that is higher than I<span> * </span>for thou hast been my hope, and a strong tower for me against the enemy.

<sub>4</sub> I will dwell in thy tabernacle for ever<span> * </span>and my trust shall be under the covering of thy wings.

<sub>5</sub> For thou, O Lord, hast heard my desires<span> * </span>and hast given an heritage unto those that fear thy Name.

<sub>6</sub> Thou shalt grant the King a long life<span> * </span>that his years may endure throughout all generations.

<sub>7</sub> He shall dwell before God for ever<span> * </span>O prepare thy loving mercy and faithfulness, that they may preserve him.

<sub>8</sub> So will I always sing praise unto thy Name<span> * </span>that I may daily perform my vows.

