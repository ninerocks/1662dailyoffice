#### *Psalm 106. Confitemini Domino*
OGIVE thanks unto the Lord, for he is gracious<span> * </span>and his mercy endureth for ever.

<sub>2</sub> Who can express the noble acts of the Lord<span> * </span>or shew forth all his praise?

<sub>3</sub> Blessed are they that alway keep judgement<span> * </span>and do righteousness.

<sub>4</sub> Remember me, O Lord, according to the favour that thou bearest unto thy people<span> * </span>O visit me with thy salvation;

<sub>5</sub> That I may see the felicity of thy chosen<span> * </span>and rejoice in the gladness of thy people, and give thanks with thine inheritance.

<sub>6</sub> We have sinned with our fathers<span> * </span>we have done amiss, and dealt wickedly.

<sub>7</sub> Our fathers regarded not thy wonders in Egypt, neither kept they thy great goodness in remembrance<span> * </span>but were disobedient at the sea, even at the Red sea.

<sub>8</sub> Nevertheless, he helped them for his Name's sake<span> * </span>that he might make his power to be known.

<sub>9</sub> He rebuked the Red sea also, and it was dried up<span> * </span>so he led them through the deep, as through a wilderness.

<sub>10</sub> And he saved them from the adversaries' hand<span> * </span>and delivered them from the hand of the enemy.

<sub>11</sub> As for those that troubled them, the waters overwhelmed them<span> * </span>there was not one of them left.

<sub>12</sub> Then believed they his words<span> * </span>and sang praise unto him.

<sub>13</sub> But within a while they forgat his works<span> * </span>and would not abide his counsel.

<sub>14</sub> But lust came upon them in the wilderness<span> * </span>and they tempted God in the desert.

<sub>15</sub> And he gave them their desire<span> * </span>and sent leanness withal into their soul.

<sub>16</sub> They angered Moses also in the tents<span> * </span>and Aaron the saint of the Lord.

<sub>17</sub> So the earth opened, and swallowed up Dathan<span> * </span>and covered the congregation of Abiram.

<sub>18</sub> And the fire was kindled in their company<span> * </span>the flame burnt up the ungodly.

<sub>19</sub> They made a calf in Horeb<span> * </span>and worshipped the molten image.

<sub>20</sub> Thus they turned their glory<span> * </span>into the similitude of a calf that eateth hay.

<sub>21</sub> And they forgat God their Saviour<span> * </span>who had done so great things in Egypt;

<sub>22</sub> Wondrous works in the land of Ham<span> * </span>and fearful things by the Red sea.

<sub>23</sub> So he said, he would have destroyed them, had not Moses his chosen stood before him in the gap<span> * </span>to turn away his wrathful indignation, lest he should destroy them.

<sub>24</sub> Yea, they thought scorn of that pleasant land<span> * </span>and gave no credence unto his word;

<sub>25</sub> But murmured in their tents<span> * </span>and hearkened not unto the voice of the lord.

<sub>26</sub> Then lift he up his hand against them<span> * </span>to overthrow them in the wilderness;

<sub>27</sub> To cast out their seed among the nations<span> * </span>and to scatter them in the lands.

<sub>28</sub> They joined themselves unto Baal-peor<span> * </span>and ate the offerings of the dead.

<sub>29</sub> Thus they provoked him to anger with their own inventions<span> * </span>and the plague was great among them.

<sub>30</sub> Then stood up Phinees and prayed<span> * </span>and so the plague ceased.

<sub>31</sub> And that was counted unto him for righteousness<span> * </span>among all posterities for evermore.

<sub>32</sub> They angered him also at the waters of strife<span> * </span>so that he punished Moses for their sakes;

<sub>33</sub> Because they provoked his spirit<span> * </span>so that he spake unadvisedly with his lips.

<sub>34</sub> Neither destroyed they the heathen<span> * </span>as the Lord commanded them;

<sub>35</sub> But were mingled among the heathen<span> * </span>and learned their works.

<sub>36</sub> Insomuch that they worshipped their idols, which turned to their own decay<span> * </span>yea, they offered their sons and their daughters unto devils;

<sub>37</sub> And shed innocent blood, even the blood of their sons and of their daughters<span> * </span>whom they had offered unto the idols of Canaan; and the land was defiled with blood.

<sub>38</sub> Thus were they stained with their own works<span> * </span>and went a whoring with their own inventions.

<sub>39</sub> Therefore was the wrath of the Lord kindled against his people<span> * </span>insomuch that he abhorred his own inheritance.

<sub>40</sub> And he gave them over into the hands of the heathen<span> * </span>and they that hated them were lords over them.

<sub>41</sub> Their enemies oppressed them<span> * </span>and had them in subjection.

<sub>42</sub> Many a time did he deliver them<span> * </span>but they rebelled against him with their own inventions, and were brought down in their wickedness.

<sub>43</sub> Nevertheless, when he saw their adversity<span> * </span>he heard their complaint.

<sub>44</sub> He thought upon his covenant, and pitied them according unto the multitude of his mercies<span> * </span>yea, he made all those that led them away captive to pity them.

<sub>45</sub> Deliver us, O Lord our God, and gather us from among the heathen<span> * </span>that we may give thanks unto thy holy Name, and make our boast of thy praise.

<sub>46</sub> Blessed be the Lord God of Israel from everlasting and world without end<span> * </span>and let all the people say, Amen.

