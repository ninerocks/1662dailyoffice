#### *Psalm 50. Deus deorum*
THE Lord, even the most mighty God, hath spoken<span> * </span>and called the world, from the rising up of the sun unto the going down thereof.

<sub>2</sub> Out of Sion hath God appeared<span> * </span>in perfect beauty.

<sub>3</sub> Our God shall come, and shall not keep silence<span> * </span>there shall go before him a consuming fire, and a mighty tempest shall be stirred up round about him.

<sub>4</sub> He shall call the heaven from above<span> * </span>and the earth, that he may judge his people.

<sub>5</sub> Gather my saints together unto me<span> * </span>those that have made a covenant with me with sacrifice.

<sub>6</sub> And the heavens shall declare his righteousness<span> * </span>for God is Judge himself.

<sub>7</sub> Hear, O my people, and I will speak<span> * </span>I myself will testify against thee, O Israel; for I am God, even thy God.

<sub>8</sub> I will not reprove thee because of thy sacrifices, or for thy burnt-offerings<span> * </span>because they were not alway before me.

<sub>9</sub> I will take no bullock out of thine house<span> * </span>nor he-goat out of thy folds.

<sub>10</sub> For all the beasts of the forest are mine<span> * </span>and so are the cattle upon a thousand hills.

<sub>11</sub> I know all the fowls upon the mountains<span> * </span>and the wild beasts of the field are in my sight.

<sub>12</sub> If I be hungry, I will not tell thee<span> * </span>for the whole world is mine, and all that is therein.

<sub>13</sub> Thinkest thou that I will eat bulls' flesh<span> * </span>and drink the blood of goats?

<sub>14</sub> Offer unto God thanksgiving<span> * </span>and pay thy vows unto the most Highest.

<sub>15</sub> And call upon me in the time of trouble<span> * </span>so will I hear thee, and thou shalt praise me.

<sub>16</sub> But unto the ungodly said God<span> * </span>Why dost thou preach my laws, and takest my covenant in thy mouth;

<sub>17</sub> Whereas thou hatest to be reformed<span> * </span>and has cast my words behind thee?

<sub>18</sub> When thou sawest a thief, thou consentedst unto him<span> * </span>and hast been partaker with the adulterers.

<sub>19</sub> Thou hast let thy mouth speak wickedness<span> * </span>and with thy tongue thou hast set forth deceit.

<sub>20</sub> Thou satest, and spakest against thy brother<span> * </span>yea, and hast slandered thine own mother's son.

<sub>21</sub> These things hast thou done, and I held my tongue, and thou thoughtest wickedly, that I am even such a one as thyself<span> * </span>but I will reprove thee, and set before thee the things that thou hast done.

<sub>22</sub> O consider this, ye that forget God<span> * </span>lest I pluck you away, and there be none to deliver you.

<sub>23</sub> Whoso offereth me thanks and praise, he honoureth me<span> * </span>and to him that ordereth his conversation right will I shew the salvation of God.

#### *Psalm 51. Miserere mei, Deus*
HAVE mercy upon me, O God, after thy great goodness<span> * </span>according to the multitude of thy mercies do away mine offences.

<sub>2</sub> Wash me throughly from my wickedness<span> * </span>and cleanse me from my sin.

<sub>3</sub> For I acknowledge my faults<span> * </span>and my sin is ever before me.

<sub>4</sub> Against thee only have I sinned, and done this evil in thy sight<span> * </span>that thou mightest be justified in thy saying, and clear when thou art judged.

<sub>5</sub> Behold, I was shapen in wickedness<span> * </span>and in sin hath my mother conceived me.

#### *6. But lo, thou requirest truth in the inward parts: and shalt make me to understand wisdom secretly.*
<sub>7</sub> Thou shalt purge me with hyssop, and I shall be clean<span> * </span>thou shalt wash me, and I shall be whiter than snow.

<sub>8</sub> Thou shalt make me hear of joy and gladness<span> * </span>that the bones which thou hast broken may rejoice.

<sub>9</sub> Turn thy face from my sins<span> * </span>and put out all my misdeeds.

<sub>10</sub> Make me a clean heart, O God<span> * </span>and renew a right spirit within me.

<sub>11</sub> Cast me not away from thy presence<span> * </span>and take not thy holy Spirit from me.

<sub>12</sub> O give me the comfort of thy help again<span> * </span>and stablish me with thy free Spirit.

<sub>13</sub> Then shall I teach thy ways unto the wicked<span> * </span>and sinners shall be converted unto thee.

<sub>14</sub> Deliver me from blood-guiltiness, O God, thou that art the God of my health<span> * </span>and my tongue shall sing of thy righteousness.

<sub>15</sub> Thou shalt open my lips, O Lord<span> * </span>and my mouth shall shew thy praise.

<sub>16</sub> For thou desirest no sacrifice, else would I give it thee<span> * </span>but thou delightest not in burnt-offerings.

<sub>17</sub> The sacrifice of God is a troubled spirit<span> * </span>a broken and contrite heart, O God, shalt thou not despise.

<sub>18</sub> O be favourable and gracious unto Sion<span> * </span>build thou the walls of Jerusalem.

<sub>19</sub> Then shalt thou be pleased with the sacrifice of righteousness, with the burnt-offerings and oblations<span> * </span>then shall they offer young bullocks upon thine altar.

#### *Psalm 52. Quid gloriaris?*
WHY boastest thou thyself, thou tyrant<span> * </span>that thou canst do mischief;

<sub>2</sub> Whereas the goodness of God<span> * </span>endureth yet daily?

<sub>3</sub> Thy tongue imagineth wickedness<span> * </span>and with lies thou cuttest like a sharp rasor.

<sub>4</sub> Thou hast loved unrighteousness more then goodness<span> * </span>and to talk of lies more than righteousness.

<sub>5</sub> Thou hast loved to speak all words that may do hurt<span> * </span>O thou false tongue.

<sub>6</sub> Therefore shall God destroy thee for ever<span> * </span>he shall take thee, and pluck thee out of thy dwelling, and root thee out of the land of the living.

<sub>7</sub> The righteous also shall see this, and fear<span> * </span>and shall laugh him to scorn;

<sub>8</sub> Lo, this is the man that took not God for his strength<span> * </span>but trusted unto the multitude of his riches, and strengthened himself in his wickedness.

<sub>9</sub> As for me, I am like a green olive-tree in the house of God<span> * </span>my trust is in the tender mercy of God for ever and ever.

<sub>10</sub> I will always give thanks unto thee for that thou hast done<span> * </span>and I will hope in thy Name, for thy saints like it well.

