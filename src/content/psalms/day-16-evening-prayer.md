#### *Psalm 82. Deus stetit*
GOD standeth in the congregation of princes<span> * </span>he is a Judge among gods.

<sub>2</sub> How long will ye give wrong judgement<span> * </span>and accept the persons of the ungodly?

<sub>3</sub> Defend the poor and fatherless<span> * </span>see that such as are in need and necessity have right.

<sub>4</sub> Deliver the outcast and poor<span> * </span>save them from the hand of the ungodly.

<sub>5</sub> They will not be learned nor understand, but walk on still in darkness<span> * </span>all the foundations of the earth are out of course.

<sub>6</sub> I have said, Ye are gods<span> * </span>and ye are all the children of the most Highest.

<sub>7</sub> But ye shall die like men<span> * </span>and fall like one of the princes.

<sub>8</sub> Arise, O God, and judge thou the earth<span> * </span>for thou shalt take all heathen to thine inheritance.

#### *Psalm 83. Deus, quis similis?*
HOLD not thy tongue, O God, keep not still silence<span> * </span>refrain not thyself, O God.

<sub>2</sub> For lo, thine enemies make a murmuring<span> * </span>and they that hate thee have lift up their head.

<sub>3</sub> They have imagined craftily against thy people<span> * </span>and taken counsel against thy secret ones.

<sub>4</sub> They have said, Come, and let us root them out, that they be no more a people<span> * </span>and that the name of Israel may be no more in remembrance.

<sub>5</sub> For they have cast their heads together with one consent<span> * </span>and are confederate against thee;

<sub>6</sub> The tabernacles of the Edomites, and the Ismaelites<span> * </span>the Moabites and Hagarenes;

<sub>7</sub> Gebal, and Ammon, and Amalek<span> * </span>the Philistines, with them that dwell at Tyre.

<sub>8</sub> Assur also is joined with them<span> * </span>and have holpen the children of Lot.

<sub>9</sub> But do thou to them as unto the Madianites<span> * </span>unto Sisera, and unto Jabin at the brook of Kison;

<sub>10</sub> Who perished at Endor<span> * </span>and became as the dung of the earth.

<sub>11</sub> Make them and their princes like Oreb and Zeb<span> * </span>yea, make all their princes like as Zeba and Salmana;

<sub>12</sub> Who say, Let us take to ourselves<span> * </span>the houses of God in possession.

<sub>13</sub> O my God, make them like unto a wheel<span> * </span>and as the stubble before the wind;

<sub>14</sub> Like as the fire that burneth up the wood<span> * </span>and as the flame that consumeth the mountains.

<sub>15</sub> Persecute them even so with thy tempest<span> * </span>and make them afraid with thy storm.

<sub>16</sub> Make their faces ashamed, O Lord<span> * </span>that they may seek thy Name.

<sub>17</sub> Let them be confounded and vexed ever more and more<span> * </span>let them be put to shame, and perish.

<sub>18</sub> And they shall know that thou, whose Name is Jehovah<span> * </span>art only the most Highest over all the earth.

#### *Psalm 84. Quam dilecta!*
OHOW amiable are thy dwellings<span> * </span>thou Lord of hosts!

<sub>2</sub> My soul hath a desire and longing to enter into the courts of the Lord<span> * </span>my heart and my flesh rejoice in the living God.

<sub>3</sub> Yea, the sparrow hath found her an house, and the swallow a nest where she may lay her young<span> * </span>even thy altars, O Lord of hosts, my King and my God.

<sub>4</sub> Blessed are they that dwell in thy house<span> * </span>they will be alway praising thee.

<sub>5</sub> Blessed is the man whose strength is in thee<span> * </span>in whose heart are thy ways.

<sub>6</sub> Who going through the vale of misery use it for a well<span> * </span>and the pools are filled with water.

<sub>7</sub> They will go from strength to strength<span> * </span>and unto the God of gods appeareth every one of them in Sion.

<sub>8</sub> O Lord God of hosts, hear my prayer<span> * </span>hearken, O God of Jacob.

<sub>9</sub> Behold, O God our defender<span> * </span>and look upon the face of thine Anointed.

<sub>10</sub> For one day in thy courts<span> * </span>is better than a thousand.

<sub>11</sub> I had rather be a door-keeper in the house of my God<span> * </span>than to dwell in the tents of ungodliness.

<sub>12</sub> For the Lord God is a light and defence<span> * </span>the Lord will give grace and worship, and no good thing shall he withhold from them that live a godly life.

<sub>13</sub> O Lord God of hosts<span> * </span>blessed is the man that putteth his trust in thee.

#### *Psalm 85. Benedixisti, Domine*
LORD, thou art become gracious unto thy land<span> * </span>thou hast turned away the captivity of Jacob.

<sub>2</sub> Thou hast forgiven the offence of thy people<span> * </span>and covered all their sins.

<sub>3</sub> Thou hast taken away all thy displeasure<span> * </span>and turned thyself from thy wrathful indignation.

<sub>4</sub> Turn us then, O God our Saviour<span> * </span>and let thine anger cease from us.

<sub>5</sub> Wilt thou be displeased at us for ever<span> * </span>and wilt thou stretch out thy wrath from one generation to another?

<sub>6</sub> Wilt thou not turn again, and quicken us<span> * </span>that thy people may rejoice in thee?

<sub>7</sub> Shew us thy mercy, O Lord<span> * </span>and grant us thy salvation.

<sub>8</sub> I will hearken what the Lord God will say concerning me<span> * </span>for he shall speak peace unto his people, and to his saints, that they turn not again.

<sub>9</sub> For his salvation is nigh them that fear him<span> * </span>that glory may dwell in our land.

<sub>10</sub> Mercy and truth are met together<span> * </span>righteousness and peace have kissed each other.

<sub>11</sub> Truth shall flourish out of the earth<span> * </span>and righteousness hath looked down from heaven.

<sub>12</sub> Yea, the Lord shall shew loving-kindness<span> * </span>and our land shall give her increase.

<sub>13</sub> Righteousness shall go before him<span> * </span>and he shall direct his going in the way.

