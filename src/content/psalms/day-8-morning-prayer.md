#### *Psalm 38. Domine, ne in furore*
PUT me not to rebuke, O Lord, in thine anger<span> * </span>neither chasten me in thy heavy displeasure.

<sub>2</sub> For thine arrows stick fast in me<span> * </span>and thy hand presseth me sore.

<sub>3</sub> There is no health in my flesh, because of thy displeasure<span> * </span>neither is there any rest in my bones, by reason of my sin.

<sub>4</sub> For my wickednesses are gone over my head<span> * </span>and are like a sore burden, too heavy for me to bear.

<sub>5</sub> My wounds stink, and are corrupt<span> * </span>through my foolishness.

<sub>6</sub> I am brought into so great trouble and misery<span> * </span>that I go mourning all the day long.

<sub>7</sub> For my loins are filled with a sore disease<span> * </span>and there is no whole part in my body.

<sub>8</sub> I am feeble, and sore smitten<span> * </span>I have roared for the very disquietness of my heart.

<sub>9</sub> Lord, thou knowest all my desire<span> * </span>and my groaning is not hid from thee.

<sub>10</sub> My heart panteth, my strength hath failed me<span> * </span>and the sight of mine eyes is gone from me.

<sub>11</sub> My lovers and my neighbours did stand looking upon my trouble<span> * </span>and my kinsmen stood afar off.

<sub>12</sub> They also that sought after my life laid snares for me<span> * </span>and they that went about to do me evil talked of wickedness, and imagined deceit all the day long.

<sub>13</sub> As for me, I was like a deaf man, and heard not<span> * </span>and as one that is dumb, who doth not open his mouth.

<sub>14</sub> I became even as a man that heareth not<span> * </span>and in whose mouth are no reproofs.

<sub>15</sub> For in thee, O Lord, have I put my trust<span> * </span>thou shalt answer for me, O Lord my God.

<sub>16</sub> I have required that they, even mine enemies, should not triumph over me<span> * </span>for when my foot slipped, they rejoiced greatly against me.

<sub>17</sub> And I, truly, am set in the plague<span> * </span>and my heaviness is ever in my sight.

<sub>18</sub> For I will confess my wickedness<span> * </span>and be sorry for my sin.

<sub>19</sub> But mine enemies live, and are mighty<span> * </span>and they that hate me wrongfully are many in number.

<sub>20</sub> They also that reward evil for good are against me<span> * </span>because I follow the thing that good is.

<sub>21</sub> Forsake me not, O Lord my God<span> * </span>be not thou far from me.

<sub>22</sub> Haste thee to help me<span> * </span>O Lord God of my salvation.

#### *Psalm 39. Dixi, Custodiam*
ISAID, I will take heed to my ways<span> * </span>that I offend not in my tongue.

<sub>2</sub> I will keep my mouth as it were with a bridle<span> * </span>while the ungodly is in my sight.

<sub>3</sub> I held my tongue, and spake nothing<span> * </span>I kept silence, yea, even from good words; but it was pain and grief to me.

<sub>4</sub> My heart was hot within me, and while I was thus musing the fire kindled<span> * </span>and at the last I spake with my tongue;

<sub>5</sub> Lord, let me know mine end, and the number of my days<span> * </span>that I may be certified how long I have to live.

<sub>6</sub> Behold, thou hast made my days as it were a span long<span> * </span>and mine age is even as nothing in respect of thee; and verily every man living is altogether vanity.

<sub>7</sub> For man walketh in a vain shadow, and disquieteth himself in vain<span> * </span>he heapeth up riches, and cannot tell who shall gather them.

<sub>8</sub> And now, Lord, what is my hope<span> * </span>truly my hope is even in thee.

<sub>9</sub> Deliver me from all mine offences<span> * </span>and make me not a rebuke unto the foolish.

<sub>10</sub> I became dumb, and opened not my mouth<span> * </span>for it was thy doing.

<sub>11</sub> Take thy plague away from me<span> * </span>I am even consumed by the means of thy heavy hand.

<sub>12</sub> When thou with rebukes dost chasten man for sin, thou makest his beauty to consume away, like as it were a moth fretting a garment<span> * </span>every man therefore is but vanity.

<sub>13</sub> Hear my prayer, O Lord, and with thine ears consider my calling<span> * </span>hold not thy peace at my tears.

<sub>14</sub> For I am a stranger with thee<span> * </span>and a sojourner, as all my fathers were.

<sub>15</sub> O spare me a little, that I may recover my strength<span> * </span>before I go hence, and be no more seen.

#### *Psalm 40. Expectans expectavi*
IWAITED patiently for the Lord<span> * </span>and he inclined unto me, and heard my calling.

<sub>2</sub> He brought me also out of the horrible pit, out of the mire and clay<span> * </span>and set my feet upon the rock, and ordered my goings.

<sub>3</sub> And he hath put a new song in my mouth<span> * </span>even a thanksgiving unto our God.

<sub>4</sub> Many shall see it, and fear<span> * </span>and shall put their trust in the Lord.

<sub>5</sub> Blessed is the man that hath set his hope in the Lord<span> * </span>and turned not unto the proud, and to such as go about with lies.

<sub>6</sub> O Lord my God, great are the wondrous works which thou hast done, like as be also thy thoughts which are to us-ward<span> * </span>and yet there is no man that ordereth them unto thee:

<sub>7</sub> If I should declare them, and speak of them<span> * </span>they should be more than I am able to express.

<sub>8</sub> Sacrifice and meat-offering thou wouldest not<span> * </span>but mine ears hast thou opened.

<sub>9</sub> Burnt-offerings, and sacrifice for sin, hast thou not required<span> * </span>then said I, Lo, I come,

<sub>10</sub> In the volume of the book it is written of me, that I should fulfil thy will, O my God<span> * </span>I am content to do it; yea, thy law is within my heart.

<sub>11</sub> I have declared thy righteousness in the great congregation<span> * </span>lo, I will not refrain my lips, O Lord, and that thou knowest.

<sub>12</sub> I have not hid thy righteousness within my heart<span> * </span>my talk hath been of thy truth and of thy salvation.

<sub>13</sub> I have not kept back thy loving mercy and truth<span> * </span>from the great congregation.

<sub>14</sub> Withdraw not thou thy mercy from me, O Lord<span> * </span>let thy loving-kindness and thy truth alway preserve me.

<sub>15</sub> For innumerable troubles are come about me; my sins have taken such hold upon me that I am not able to look up<span> * </span>yea, they are more in number than the hairs of my head, and my heart hath failed me.

<sub>16</sub> O Lord, let it be thy pleasure to deliver me<span> * </span>make haste, O Lord, to help me.

<sub>17</sub> Let them be ashamed and confounded together, that seek after my soul to destroy it<span> * </span>let them be driven backward and put to rebuke, that wish me evil.

<sub>18</sub> Let them be desolate, and rewarded with shame<span> * </span>that say unto me, Fie upon thee, fie upon thee.

<sub>19</sub> Let all those that seek thee be joyful and glad in thee<span> * </span>and let such as love thy salvation say alway, The Lord be praised.

<sub>20</sub> As for me, I am poor and needy<span> * </span>but the Lord careth for me.

<sub>21</sub> Thou art my helper and redeemer<span> * </span>make no long tarrying, O my God.

