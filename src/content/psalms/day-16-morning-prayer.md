#### *Psalm 79. Deus, venerunt*
OGOD, the heathen are come into thine inheritance<span> * </span>thy holy temple have they defiled, and made Jerusalem an heap of stones.

<sub>2</sub> The dead bodies of thy servants have they given to be meat unto the fowls of the air<span> * </span>and the flesh of thy saints unto the beasts of the land.

<sub>3</sub> Their blood have they shed like water on every side of Jerusalem<span> * </span>and there was no man to bury them.

<sub>4</sub> We are become an open shame to our enemies<span> * </span>a very scorn and derision unto them that are round about us.

<sub>5</sub> Lord, how long wilt thou be angry<span> * </span>shall thy jealousy burn like fire for ever?

<sub>6</sub> Pour out thine indignation upon the heathen that have not known thee<span> * </span>and upon the kingdoms that have not called upon thy Name.

<sub>7</sub> For they have devoured Jacob<span> * </span>and laid waste his dwelling-place.

<sub>8</sub> O remember not our old sins, but have mercy upon us, and that soon<span> * </span>for we are come to great misery.

<sub>9</sub> Help us, O God of our salvation, for the glory of thy Name<span> * </span>O deliver us, and be merciful unto our sins, for thy Name's sake.

<sub>10</sub> Wherefore do the heathen say<span> * </span>Where is now their God?

<sub>11</sub> O let the vengeance of thy servants' blood that is shed<span> * </span>be openly shewed upon the heathen in our sight.

<sub>12</sub> O let the sorrowful sighing of the prisoners come before thee<span> * </span>according to the greatness of thy power, preserve thou those that are appointed to die.

<sub>13</sub> And for the blasphemy wherewith our neighbours have blasphemed thee<span> * </span>reward thou them , O Lord, seven-fold into their bosom.

<sub>14</sub> So we, that are thy people, and sheep of thy pasture, shall give thee thanks for ever<span> * </span>and will alway be shewing forth thy praise from generation to generation.

#### *Psalm 80. Qui regis Israel*
HEAR, O thou Shepherd of Israel, thou that leadest Joseph like a sheep<span> * </span>shew thyself also, thou that sittest upon the cherubims.

<sub>2</sub> Before Ephraim, Benjamin, and Manasses<span> * </span>stir up thy strength, and come, and help us.

<sub>3</sub> Turn us again, O God<span> * </span>shew the light of thy countenance, and we shall be whole.

<sub>4</sub> O Lord God of hosts<span> * </span>how long wilt thou be angry with thy people that prayeth?

<sub>5</sub> Thou feedest them with the bread of tears<span> * </span>and givest them plenteousness of tears to drink.

<sub>6</sub> Thou hast made us a very strife unto our neighbours<span> * </span>and our enemies laugh us to scorn.

<sub>7</sub> Turn us again, thou God of hosts<span> * </span>shew the light of thy countenance, and we shall be whole.

<sub>8</sub> Thou hast brought a vine out of Egypt<span> * </span>thou hast cast out the heathen, and planted it.

<sub>9</sub> Thou madest room for it<span> * </span>and when it had taken root it filled the land.

<sub>10</sub> The hills were covered with the shadow of it<span> * </span>and the boughs thereof were like the goodly cedar-trees.

<sub>11</sub> She stretched out her branches unto the sea<span> * </span>and her boughs unto the river.

<sub>12</sub> Why hast thou then broken down her hedge<span> * </span>that all they that go by pluck off her grapes?

<sub>13</sub> The wild boar out of the wood doth root it up<span> * </span>and the wild beasts of the field devour it.

<sub>14</sub> Turn thee again, thou God of hosts, look down from heaven<span> * </span>behold, and visit this vine;

<sub>15</sub> And the place of the vineyard that thy right hand hath planted<span> * </span>and the branch that thou madest so strong for thyself.

<sub>16</sub> It is burnt with fire, and cut down<span> * </span>and they shall perish at the rebuke of thy countenance.

<sub>17</sub> Let thy hand be upon the man of thy right hand<span> * </span>and upon the son of man, whom thou madest so strong for thine own self.

<sub>18</sub> And so will not we go back from thee<span> * </span>O let us live, and we shall call upon thy Name.

<sub>19</sub> Turn us again, O Lord God of hosts<span> * </span>shew the light of thy countenance, and we shall be whole.

#### *Psalm 81. Exultate Deo*
SING we merrily unto God our strength<span> * </span>make a cheerful noise unto the God of Jacob.

<sub>2</sub> Take the psalm, bring hither the tabret<span> * </span>the merry harp with the lute.

<sub>3</sub> Blow up the trumpet in the new-moon<span> * </span>even in the time appointed, and upon our solemn feast-day.

<sub>4</sub> For this was made a statute for Israel<span> * </span>and a law of the God of Jacob.

<sub>5</sub> This he ordained in Joseph for a testimony<span> * </span>when he came out of the land of Egypt, and had heard a strange language.

<sub>6</sub> I eased his shoulder from the burden<span> * </span>and his hands were delivered from making the pots.

<sub>7</sub> Thou calledst upon me in troubles, and I delivered thee<span> * </span>and heard thee what time as the storm fell upon thee.

<sub>8</sub> I proved thee also<span> * </span>at the waters of strife.

<sub>9</sub> Hear, O my people, and I will assure thee, O Israel<span> * </span>if thou wilt hearken unto me,

<sub>10</sub> There shall no strange god be in thee<span> * </span>neither shalt thou worship any other god.

<sub>11</sub> I am the Lord thy God, who brought thee out of the land of Egypt<span> * </span>open thy mouth wide, and I shall fill it.

<sub>12</sub> But my people would not hear my voice<span> * </span>and Israel would not obey me.

<sub>13</sub> So I gave them up unto their own hearts' lusts<span> * </span>and let them follow their own imaginations.

<sub>14</sub> O that my people would have hearkened unto me<span> * </span>for if Israel had walked in my ways,

<sub>15</sub> I should soon have put down their enemies<span> * </span>and turned my hand against their adversaries.

<sub>16</sub> The haters of the Lord should have been found liars<span> * </span>but their time should have endured for ever.

<sub>17</sub> He should have fed them also with the finest wheat-flour<span> * </span>and with honey out of the stony rock should I have satisfied thee.

