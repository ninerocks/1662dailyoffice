#### *Psalm 15. Domine, quis habitabit?*
LORD, who shall dwell in thy tabernacle<span> * </span>or who shall rest upon thy holy hill?

<sub>2</sub> Even he that leadeth an uncorrupt life<span> * </span>and doeth the thing which is right, and speaketh the truth from his heart.

<sub>3</sub> He that hath used no deceit in his tongue, nor done evil to his neighbour<span> * </span>and hath not slandered his neighbour.

<sub>4</sub> He that setteth not by himself, but is lowly in his own eyes<span> * </span>and maketh much of them that fear the Lord.

<sub>5</sub> He that sweareth unto his neighbour, and disappointeth him not<span> * </span>though it were to his own hindrance.

<sub>6</sub> He that hath not given his money upon usury<span> * </span>nor taken reward against the innocent.

<sub>7</sub> Whoso doeth these things<span> * </span>shall never fall.

#### *Psalm 16. Conserva me, Domine*
PRESERVE me, O God<span> * </span>for in thee have I put my trust.

<sub>2</sub> O my soul, thou hast said unto the Lord<span> * </span>Thou art my God, my goods are nothing unto thee.

<sub>3</sub> All my delight is upon the saints, that are in the earth<span> * </span>and upon such as excel in virtue.

<sub>4</sub> But they that run after another god<span> * </span>shall have great trouble.

<sub>5</sub> Their drink-offerings of blood will I not offer<span> * </span>neither make mention of their names within my lips.

<sub>6</sub> The Lord himself is the portion of mine inheritance, and of my cup<span> * </span>thou shalt maintain my lot.

<sub>7</sub> The lot is fallen unto me in a fair ground<span> * </span>yea, I have a goodly heritage.

<sub>8</sub> I will thank the Lord for giving me warning<span> * </span>my reins also chasten me in the night-season.

<sub>9</sub> I have set God always before me<span> * </span>for he is on my right hand, therefore I shall not fall.

<sub>10</sub> Wherefore my heart was glad, and my glory rejoiced<span> * </span>my flesh also shall rest in hope.

<sub>11</sub> For why? thou shalt not leave my soul in hell<span> * </span>neither shalt thou suffer thy Holy One to see corruption.

<sub>12</sub> Thou shalt shew me the path of life; in thy presence is the fulness of joy<span> * </span>and at thy right hand there is pleasure for evermore.

#### *Psalm 17. Exaudi, Domine*
HEAR the right, O Lord, consider my complaint<span> * </span>and hearken unto my prayer, that goeth not out of feigned lips.

<sub>2</sub> Let my sentence come forth from thy presence<span> * </span>and let thine eyes look upon the thing that is equal.

#### *3. Thou hast proved and visited mine heart in the night-season; thou hast tried me, and shalt find no wickedness in me; for I am utterly purposed that my mouth shall not offend.*
<sub>4</sub> Because of men's works, that are done against the words of thy lips<span> * </span>I have kept me from the ways of the destroyer.

<sub>5</sub> O hold thou up my goings in thy paths<span> * </span>that my footsteps slip not.

<sub>6</sub> I have called upon thee, O God, for thou shalt hear me<span> * </span>incline thine ear to me, and hearken unto my words.

<sub>7</sub> Shew thy marvellous loving-kindness, thou that art the Saviour of them which put their trust in thee<span> * </span>from such as resist thy right hand.

<sub>8</sub> Keep me as the apple of an eye<span> * </span>hide me under the shadow of thy wings.

<sub>9</sub> From the ungodly that trouble me<span> * </span>mine enemies compass me round about to take away my soul.

<sub>10</sub> They are inclosed in their own fat<span> * </span>and their mouth speaketh proud things.

<sub>11</sub> They lie waiting in our way on every side<span> * </span>turning their eyes down to the ground.

<sub>12</sub> Like as a lion that is greedy of his prey<span> * </span>and as it were a lion's whelp, lurking in secret places.

<sub>13</sub> Up, Lord, disappoint him, and cast him down<span> * </span>deliver my soul from the ungodly, which is a sword of thine;

<sub>14</sub> From the men of thy hand, O Lord, from the men, I say, and from the evil world<span> * </span>which have their portion in this life, whose bellies thou fillest with thy hid treasure.

<sub>15</sub> They have children at their desire<span> * </span>and leave the rest of their substance for their babes.

<sub>16</sub> But as for me, I will behold thy presence in righteousness<span> * </span>and when I awake up after thy likeness, I shall be satisfied with it.

