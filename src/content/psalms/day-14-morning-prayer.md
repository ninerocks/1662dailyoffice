#### *Psalm 71. In te, Domine, speravi*
IN THEE, O Lord, have I put my trust, let me never be put to confusion<span> * </span>but rid me and deliver me in thy righteousness, incline thine ear unto me, and save me.

<sub>2</sub> Be thou my strong hold, whereunto I may alway resort<span> * </span>thou hast promised to help me, for thou art my house of defence and my castle.

<sub>3</sub> Deliver me, O my God, out of the hand of the ungodly<span> * </span>out of the hand of the unrighteous and cruel man.

<sub>4</sub> For thou, O Lord God, art the thing that I long for<span> * </span>thou art my hope, even from my youth.

<sub>5</sub> Through thee have I been holden up ever since I was born<span> * </span>thou art he that took me out of my mother's womb; my praise shall be always of thee.

<sub>6</sub> I am become as it were a monster unto many<span> * </span>but my sure trust is in thee.

<sub>7</sub> O let my mouth be filled with thy praise<span> * </span>that I may sing of thy glory and honour all the day long.

<sub>8</sub> Cast me not away in the time of age<span> * </span>forsake me not when my strength faileth me.

<sub>9</sub> For mine enemies speak against me, and they that lay wait for my soul take their counsel together, saying<span> * </span>God hath forsaken him; persecute him, and take him, for there is none to deliver him.

<sub>10</sub> Go not far from me, O God<span> * </span>my God, haste thee to help me.

<sub>11</sub> Let them be confounded and perish that are against my soul<span> * </span>let them be covered with shame and dishonour that seek to do me evil.

<sub>12</sub> As for me, I will patiently abide alway<span> * </span>and will praise thee more and more.

<sub>13</sub> My mouth shall daily speak of thy righteousness and salvation<span> * </span>for I know no end thereof.

<sub>14</sub> I will go forth in the strength of the Lord God<span> * </span>and will make mention of thy righteousness only.

<sub>15</sub> Thou, O God, hast taught me from my youth up until now<span> * </span>therefore will I tell of thy wondrous works.

<sub>16</sub> Forsake me not, O God, in mine old age, when I am gray-headed<span> * </span>until I have shewed thy strength unto this generation, and thy power to all them that are yet for to come.

<sub>17</sub> Thy righteousness, O God, is very high<span> * </span>and great things are they that thou hast done; O God, who is like unto thee?

<sub>18</sub> O what great troubles and adversities hast thou shewed me, and yet didst thou turn and refresh me<span> * </span>yea, and broughtest me from the deep of the earth again.

<sub>19</sub> Thou hast brought me to great honour<span> * </span>and comforted me on every side.

<sub>20</sub> Therefore will I praise thee and thy faithfulness, O God, playing upon an instrument of musick<span> * </span>unto thee will I sing upon the harp, O thou Holy One of Israel.

<sub>21</sub> My lips will be fain when I sing unto thee<span> * </span>and so will my soul whom thou hast delivered.

<sub>22</sub> My tongue also shall talk of thy righteousness all the day long<span> * </span>for they are confounded and brought unto shame that seek to do me evil.

#### *Psalm 72. Deus, judicium*
GIVE the King thy judgements, O God<span> * </span>and thy righteousness unto the King's son.

<sub>2</sub> Then shall he judge thy people according unto right<span> * </span>and defend the poor.

<sub>3</sub> The mountains also shall bring peace<span> * </span>and the little hills righteousness unto the people.

<sub>4</sub> He shall keep the simple folk by their right<span> * </span>defend the children of the poor, and punish the wrong-doer.

<sub>5</sub> They shall fear thee, as long as the sun and moon endureth<span> * </span>from one generation to another.

<sub>6</sub> He shall come down like the rain into a fleece of wool<span> * </span>even as the drops that water the earth.

<sub>7</sub> In his time shall the righteous flourish<span> * </span>yea, and abundance of peace, so long as the moon endureth.

<sub>8</sub> His dominion shall be also from the one sea to the other<span> * </span>and from the flood unto the world's end.

<sub>9</sub> They that dwell in the wilderness shall kneel before him<span> * </span>his enemies shall lick the dust.

<sub>10</sub> The kings of Tharsis and of the isles shall give presents<span> * </span>the kings of Arabia and Saba shall bring gifts.

<sub>11</sub> All kings shall fall down before him<span> * </span>all nations shall do him service.

<sub>12</sub> For he shall deliver the poor when he crieth<span> * </span>the needy also, and him that hath no helper.

<sub>13</sub> He shall be favourable to the simple and needy<span> * </span>and shall preserve the souls of the poor.

<sub>14</sub> He shall deliver their souls from falsehood and wrong<span> * </span>and dear shall their blood be in his sight.

<sub>15</sub> He shall live, and unto him shall be given of the gold of Arabia<span> * </span>prayer shall be made ever unto him, and daily shall he be praised.

<sub>16</sub> There shall be an heap of corn in the earth, high upon the hills<span> * </span>his fruit shall shake like Libanus, and shall be green in the city like grass upon the earth.

<sub>17</sub> His Name shall endure for ever; his Name shall remain under the sun among the posterities<span> * </span>which shall be blessed through him; and all the heathen shall praise him.

<sub>18</sub> Blessed be the Lord God, even the God of Israel<span> * </span>which only doeth wondrous things;

<sub>19</sub> And blessed be the Name of his majesty for ever<span> * </span>and all the earth shall be filled with his majesty. Amen, Amen.

