#### *Lucerna pedibus meis*
THY word is a lantern unto my feet<span> * </span>and a light unto my paths.

<sub>106</sub> I have sworn, and am stedfastly purposed<span> * </span>to keep thy righteous judgements.

<sub>107</sub> I am troubled above measure<span> * </span>quicken me, O Lord, according to thy word.

<sub>108</sub> Let the free-will offerings of my mouth please thee, O Lord<span> * </span>and teach me thy judgements.

<sub>109</sub> My soul is alway in my hand<span> * </span>yet do I not forget thy law.

<sub>110</sub> The ungodly have laid a snare for me<span> * </span>but yet I swerved not from thy commandments.

<sub>111</sub> Thy testimonies have I claimed as mine heritage for ever<span> * </span>and why? they are the very joy of my heart.

<sub>112</sub> I have applied my heart to fulfil thy statutes alway<span> * </span>even unto the end.

#### *Iniquos odio habui*
IHATE them that imagine evil things<span> * </span>but thy law do I love.

<sub>114</sub> Thou art my defence and shield<span> * </span>and my trust is in thy word.

<sub>115</sub> Away from me, ye wicked<span> * </span>I will keep the commandments of my God.

<sub>116</sub> O stablish me according to thy word, that I may live<span> * </span>and let me not be disappointed of my hope.

<sub>117</sub> Hold thou me up, and I shall be safe<span> * </span>yea, my delight shall be ever in thy statutes.

<sub>118</sub> Thou hast trodden down all them that depart from thy statutes<span> * </span>for they imagine but deceit.

<sub>119</sub> Thou puttest away all the ungodly of the earth like dross<span> * </span>therefore I love thy testimonies.

<sub>120</sub> My flesh trembleth for fear of thee<span> * </span>and I am afraid of thy judgements.

#### *Feci judicium*
IDEAL with the thing that is lawful and right<span> * </span>O give me not over unto mine oppressors.

<sub>122</sub> Make thou thy servant to delight in that which is good<span> * </span>that the proud do me no wrong.

<sub>123</sub> Mine eyes are wasted away with looking for thy health<span> * </span>and for the word of thy righteousness.

<sub>124</sub> O deal with thy servant according unto thy loving mercy<span> * </span>and teach me thy statutes.

<sub>125</sub> I am thy servant, O grant me understanding<span> * </span>that I may know thy testimonies.

<sub>126</sub> It is time for thee, Lord, to lay to thine hand<span> * </span>for they have destroyed thy law.

<sub>127</sub> For I love thy commandments<span> * </span>above gold and precious stone.

<sub>128</sub> Therefore hold I straight all thy commandments<span> * </span>and all false ways I utterly abhor.

#### *Mirabilia*
THY testimonies are wonderful<span> * </span>therefore doth my soul keep them.

<sub>130</sub> When thy word goeth forth<span> * </span>it giveth light and understanding unto the simple.

<sub>131</sub> I opened my mouth, and drew in my breath<span> * </span>for my delight was in thy commandments.

<sub>132</sub> O look thou upon me, and be merciful unto me<span> * </span>as thou usest to do unto those that love thy Name.

<sub>133</sub> Order my steps in thy word<span> * </span>and so shall no wickedness have dominion over me.

<sub>134</sub> O deliver me from the wrongful dealings of men<span> * </span>and so shall I keep thy commandments.

<sub>135</sub> Shew the light of thy countenance upon thy servant<span> * </span>and teach me thy statutes.

<sub>136</sub> Mine eyes gush out with water<span> * </span>because men keep not thy law.

#### *Justus es, Domine*
RIGHTEOUS art thou, O Lord<span> * </span>and true is thy judgement.

<sub>138</sub> The testimonies that thou hast commanded<span> * </span>are exceeding righteous and true.

<sub>139</sub> My zeal hath even consumed me<span> * </span>because mine enemies have forgotten thy words.

<sub>140</sub> Thy word is tried to the uttermost<span> * </span>and thy servant loveth it.

<sub>141</sub> I am small, and of no reputation<span> * </span>yet do I not forget thy commandments.

<sub>142</sub> Thy righteousness is an everlasting righteousness<span> * </span>and thy law is the truth.

<sub>143</sub> Trouble and heaviness have taken hold upon me<span> * </span>yet is my delight in thy commandments.

<sub>144</sub> The righteousness of thy testimonies is everlasting<span> * </span>O grant me understanding, and I shall live.

