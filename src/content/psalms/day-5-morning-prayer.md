#### *Psalm 24. Domini est terra*
THE earth is the Lord's, and all that therein is<span> * </span>the compass of the world, and they that dwell therein.

<sub>2</sub> For he hath founded it upon the seas<span> * </span>and prepared it upon the floods.

<sub>3</sub> Who shall ascend into the hill of the Lord<span> * </span>or who shall rise up in his holy place?

<sub>4</sub> Even he that hath clean hands, and a pure heart<span> * </span>and that hath not lift up his mind unto vanity, nor sworn to deceive his neighbour.

<sub>5</sub> He shall receive the blessing from the Lord<span> * </span>and righteousness from the God of his salvation.

<sub>6</sub> This is the generation of them that seek him<span> * </span>even of them that seek thy face, O Jacob.

<sub>7</sub> Lift up your heads, O ye gates, and be ye lift up, ye everlasting doors<span> * </span>and the King of glory shall come in.

<sub>8</sub> Who is the King of glory<span> * </span>it is the Lord strong and mighty, even the Lord mighty in battle.

<sub>9</sub> Lift up your heads, O ye gates, and be ye lift up, ye everlasting doors<span> * </span>and the King of glory shall come in.

<sub>10</sub> Who is the King of glory<span> * </span>even the Lord of hosts, he is the King of glory.

#### *Psalm 25. Ad te, Domine, levavi*
UNTO thee, O Lord, will I lift up my soul; my God, I have put my trust in thee<span> * </span>O let me not be confounded, neither let mine enemies triumph over me.

<sub>2</sub> For all they that hope in thee shall not be ashamed<span> * </span>but such as transgress without a cause shall be put to confusion.

<sub>3</sub> Shew me thy ways, O Lord<span> * </span>and teach me thy paths.

<sub>4</sub> Lead me forth in thy truth, and learn me<span> * </span>for thou art the God of my salvation; in thee hath been my hope all the day long.

<sub>5</sub> Call to remembrance, O Lord, thy tender mercies<span> * </span>and thy loving-kindnesses, which have been ever of old.

<sub>6</sub> O remember not the sins and offences of my youth<span> * </span>but according to thy mercy think thou upon me, O Lord, for thy goodness.

<sub>7</sub> Gracious and righteous is the Lord<span> * </span>therefore will he teach sinners in the way.

<sub>8</sub> Them that are meek shall he guide in judgement<span> * </span>and such as are gentle, them shall he learn his way.

<sub>9</sub> All the paths of the Lord are mercy and truth<span> * </span>unto such as keep his covenant and his testimonies.

<sub>10</sub> For thy Name's sake, O Lord<span> * </span>be merciful unto my sin, for it is great.

<sub>11</sub> What man is he that feareth the Lord<span> * </span>him shall he teach in the way that he shall choose.

<sub>12</sub> His soul shall dwell at ease<span> * </span>and his seed shall inherit the land.

<sub>13</sub> The secret of the Lord is among them that fear him<span> * </span>and he will shew them his covenant.

<sub>14</sub> Mine eyes are ever looking unto the Lord<span> * </span>for he shall pluck my feet out of the net.

<sub>15</sub> Turn thee unto me, and have mercy upon me<span> * </span>for I am desolate and in misery.

<sub>16</sub> The sorrows of my heart are enlarged<span> * </span>O bring thou me out of my troubles.

<sub>17</sub> Look upon my adversity and misery<span> * </span>and forgive me all my sin.

<sub>18</sub> Consider mine enemies, how many they are<span> * </span>and they bear a tyrannous hate against me.

<sub>19</sub> O keep my soul, and deliver me<span> * </span>let me not be confounded, for I have put my trust in thee.

<sub>20</sub> Let perfectness and righteous dealing wait upon me<span> * </span>for my hope hath been in thee.

<sub>21</sub> Deliver Israel, O God<span> * </span>out of all his troubles.

#### *Psalm 26. Judica me, Domine*
BE THOU my judge, O Lord, for I have walked innocently<span> * </span>my trust hath been also in the Lord, therefore shall I not fall.

<sub>2</sub> Examine me, O Lord, and prove me<span> * </span>try out my reins and my heart.

<sub>3</sub> For thy loving-kindness is ever before mine eyes<span> * </span>and I will walk in thy truth.

<sub>4</sub> I have not dwelt with vain persons<span> * </span>neither will I have fellowship with the deceitful.

<sub>5</sub> I have hated the congregation of the wicked<span> * </span>and will not sit among the ungodly.

<sub>6</sub> I will wash my hands in innocency, O Lord<span> * </span>and so will I go to thine altar.

<sub>7</sub> That I may shew the voice of thanksgiving<span> * </span>and tell of all thy wondrous works.

<sub>8</sub> Lord, I have loved the habitation of thy house<span> * </span>and the place where thine honour dwelleth.

<sub>9</sub> O shut not up my soul with the sinners<span> * </span>nor my life with the blood-thirsty.

<sub>10</sub> In whose hands is wickedness<span> * </span>and their right hand is full of gifts.

<sub>11</sub> But as for me, I will walk innocently<span> * </span>O deliver me, and be merciful unto me.

<sub>12</sub> My foot standeth right<span> * </span>I will praise the Lord in the congregations.

