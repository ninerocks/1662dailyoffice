#### *Psalm 18. Diligam te, Domine*
IWILL love thee, O Lord, my strength; the Lord is my stony rock, and my defence<span> * </span>my saviour, my God, and my might, in whom I will trust, my buckler, the horn also of my salvation, and my refuge.

<sub>2</sub> I will call upon the Lord, which is worthy to be praised<span> * </span>so shall I be safe from mine enemies.

<sub>3</sub> The sorrows of death compassed me<span> * </span>and the overflowings of ungodliness made me afraid.

<sub>4</sub> The pains of hell came about me<span> * </span>the snares of death overtook me.

<sub>5</sub> In my trouble I will call upon the Lord<span> * </span>and complain unto my God.

<sub>6</sub> So shall he hear my voice out of his holy temple<span> * </span>and my complaint shall come before him, it shall enter even into his ears.

<sub>7</sub> The earth trembled and quaked<span> * </span>the very foundations also of the hills shook, and were removed, because he was wroth.

<sub>8</sub> There went a smoke out in his presence<span> * </span>and a consuming fire out of his mouth, so that coals were kindled at it.

<sub>9</sub> He bowed the heavens also, and came down<span> * </span>and it was dark under his feet.

<sub>10</sub> He rode upon the cherubins, and did fly<span> * </span>he came flying upon the wings of the wind.

<sub>11</sub> He made darkness his secret place<span> * </span>his pavilion round about him, with dark water and thick clouds to cover him.

<sub>12</sub> At the brightness of his presence his clouds removed<span> * </span>hail-stones, and coals of fire.

<sub>13</sub> The Lord also thundered out of heaven, and the Highest gave his thunder<span> * </span>hail-stones, and coals of fire.

<sub>14</sub> He sent out his arrows, and scattered them<span> * </span>he cast forth lightnings, and destroyed them.

<sub>15</sub> The springs of water were seen, and the foundations of the round world were discovered, at thy chiding, O Lord<span> * </span>at the blasting of the breath of thy displeasure.

<sub>16</sub> He shall send down from on high to fetch me<span> * </span>and shall take me out of many waters.

<sub>17</sub> He shall deliver me from my strongest enemy, and from them which hate me<span> * </span>for they are too mighty for me.

<sub>18</sub> They prevented me in the day of my trouble<span> * </span>but the Lord was my upholder.

<sub>19</sub> He brought me forth also into a place of liberty<span> * </span>he brought me forth, even because he had a favour unto me.

<sub>20</sub> The Lord shall reward me after my righteous dealing<span> * </span>according to the cleanness of my hands shall he recompense me.

<sub>21</sub> Because I have kept the ways of the Lord<span> * </span>and have not forsaken my God, as the wicked doth.

<sub>22</sub> For I have an eye unto all his laws<span> * </span>and will not cast out his commandments from me.

<sub>23</sub> I was also uncorrupt before him<span> * </span>and eschewed mine own wickedness.

<sub>24</sub> Therefore shall the Lord reward me after my righteous dealing<span> * </span>and according unto the cleanness of my hands in his eye-sight.

<sub>25</sub> With the holy thou shalt be holy<span> * </span>and with a perfect man thou shalt be perfect.

<sub>26</sub> With the clean thou shalt be clean<span> * </span>and with the froward thou shalt learn frowardness.

<sub>27</sub> For thou shalt save the people that are in adversity<span> * </span>and shalt bring down the high looks of the proud.

<sub>28</sub> Thou also shalt light my candle<span> * </span>the Lord my God shall make my darkness to be light.

<sub>29</sub> For in thee I shall discomfit an host of men<span> * </span>and with the help of my God I shall leap over the wall.

<sub>30</sub> The way of God is an undefiled way<span> * </span>the word of the Lord also is tried in the fire; he is the defender of all them that put their trust in him.

<sub>31</sub> For who is God, but the Lord<span> * </span>or who hath any strength, except our God?

<sub>32</sub> It is God, that girdeth me with strength of war<span> * </span>and maketh my way perfect.

<sub>33</sub> He maketh my feet like harts' feet<span> * </span>and setteth me up on high.

<sub>34</sub> He teacheth mine hands to fight<span> * </span>and mine arms shall break even a bow of steel.

<sub>35</sub> Thou hast given me the defence of thy salvation<span> * </span>thy right hand also shall hold me up, and thy loving correction shall make me great.

<sub>36</sub> Thou shalt make room enough under me for to go<span> * </span>that my footsteps shall not slide.

<sub>37</sub> I will follow upon mine enemies, and overtake them<span> * </span>neither will I turn again till I have destroyed them.

<sub>38</sub> I will smite them, that they shall not be able to stand<span> * </span>but fall under my feet.

<sub>39</sub> Thou hast girded me with strength unto the battle<span> * </span>thou shalt throw down mine enemies under me.

<sub>40</sub> Thou hast made mine enemies also to turn their backs upon me<span> * </span>and I shall destroy them that hate me.

<sub>41</sub> They shall cry, but there shall be none to help them<span> * </span>yea, even unto the Lord shall they cry, but he shall not hear them.

<sub>42</sub> I will beat them as small as the dust before the wind<span> * </span>I will cast them out as the clay in the streets.

<sub>43</sub> Thou shalt deliver me from the strivings of the people<span> * </span>and thou shalt make me the head of the heathen.

<sub>44</sub> A people whom I have not known<span> * </span>shall serve me.

<sub>45</sub> As soon as they hear of me, they shall obey me<span> * </span>but the strange children shall dissemble with me.

<sub>46</sub> The strange children shall fail<span> * </span>and be afraid out of their prisons.

<sub>47</sub> The Lord liveth, and blessed be my strong helper<span> * </span>and praised be the Lord of my salvation;

<sub>48</sub> Even the God that seeth that I be avenged<span> * </span>and subdueth the people unto me.

<sub>49</sub> It is he that delivereth me from my cruel enemies, and setteth me up above mine adversaries<span> * </span>thou shalt rid me from the wicked man.

<sub>50</sub> For this cause will I give thanks unto thee, O Lord, among the Gentiles<span> * </span>and sing praises unto thy Name.

<sub>51</sub> Great prosperity giveth he unto his King<span> * </span>and sheweth loving-kindness unto David his Anointed, and unto his seed for evermore.

