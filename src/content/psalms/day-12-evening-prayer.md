#### *Psalm 65. Te decet hymnus*
THOU, O God, art praised in Sion<span> * </span>and unto thee shall the vow be performed in Jerusalem.

<sub>2</sub> Thou that hearest the prayer<span> * </span>unto thee shall all flesh come.

<sub>3</sub> My misdeeds prevail against me<span> * </span>O be thou merciful unto our sins.

<sub>4</sub> Blessed is the man whom thou choosest, and receivest unto thee<span> * </span>he shall dwell in thy court, and shall be satisfied with the pleasures of thy house, even of thy holy temple.

<sub>5</sub> Thou shalt shew us wonderful things in thy righteousness, O God of our salvation<span> * </span>thou that art the hope of all the ends of the earth, and of them that remain in the broad sea.

<sub>6</sub> Who in his strength setteth fast the mountains<span> * </span>and is girded about with power.

<sub>7</sub> Who stilleth the raging of the sea<span> * </span>and the noise of his waves, and the madness of the people.

<sub>8</sub> They also that dwell in the uttermost parts of the earth shall be afraid at thy tokens<span> * </span>thou that makest the outgoings of the morning and evening to praise thee.

<sub>9</sub> Thou visitest the earth, and blessest it<span> * </span>thou makest it very plenteous.

<sub>10</sub> The river of God is full of water<span> * </span>thou preparest their corn, for so thou providest for the earth.

<sub>11</sub> Thou waterest her furrows, thou sendest rain into the little valleys thereof<span> * </span>thou makest it soft with the drops of rain, and blessest the increase of it.

<sub>12</sub> Thou crownest the year with thy goodness<span> * </span>and thy clouds drop fatness.

<sub>13</sub> They shall drop upon the dwellings of the wilderness<span> * </span>and the little hills shall rejoice on every side.

<sub>14</sub> The folds shall be full of sheep<span> * </span>the valleys also shall stand so thick with corn, that they shall laugh and sing.

#### *Psalm 66. Jubilate Deo*
OBE joyful in God, all ye lands<span> * </span>sing praises unto the honour of his Name, make his praise to be glorious.

<sub>2</sub> Say unto God, O how wonderful art thou in thy works<span> * </span>through the greatness of thy power shall thine enemies be found liars unto thee.

<sub>3</sub> For all the world shall worship thee<span> * </span>sing of thee, and praise thy Name.

<sub>4</sub> O come hither, and behold the works of God<span> * </span>how wonderful he is in his doing toward the children of men.

<sub>5</sub> He turned the sea into dry land<span> * </span>so that they went through the water on foot; there did we rejoice thereof.

<sub>6</sub> He ruleth with his power for ever; his eyes behold the people<span> * </span>and such as will not believe shall not be able to exalt themselves.

<sub>7</sub> O praise our God, ye people<span> * </span>and make the voice of his praise to be heard;

<sub>8</sub> Who holdeth our soul in life<span> * </span>and suffereth not our feet to slip.

<sub>9</sub> For thou, O God, hast proved us<span> * </span>thou also hast tried us, like as silver is tried.

<sub>10</sub> Thou broughtest us into the snare<span> * </span>and laidest trouble upon our loins.

<sub>11</sub> Thou sufferedst men to ride over our heads<span> * </span>we went through fire and water, and thou broughtest us out into a wealthy place.

<sub>12</sub> I will go into thine house with burnt-offerings<span> * </span>and will pay thee my vows, which I promised with my lips, and spake with my mouth, when I was in trouble.

<sub>13</sub> I will offer unto thee fat burnt-sacrifices, with the incense of rams<span> * </span>I will offer bullocks and goats.

<sub>14</sub> O come hither, and hearken, all ye that fear God<span> * </span>and I will tell you what he hath done for my soul.

<sub>15</sub> I called unto him with my mouth<span> * </span>and gave him praises with my tongue.

<sub>16</sub> If I incline unto wickedness with mine heart<span> * </span>the Lord will not hear me.

<sub>17</sub> But God hath heard me<span> * </span>and considered the voice of my prayer.

<sub>18</sub> Praised be God, who hath not cast out my prayer<span> * </span>nor turned his mercy from me.

#### *Psalm 67. Deus misereatur*
GOD be merciful unto us, and bless us<span> * </span>and shew us the light of his countenance, and be merciful unto us:

<sub>2</sub> That thy way may be known upon earth<span> * </span>thy saving health among all nations.

<sub>3</sub> Let the people praise thee, O God<span> * </span>yea, let all the people praise thee.

<sub>4</sub> O let the nations rejoice and be glad<span> * </span>for thou shalt judge the folk righteously, and govern the nations upon earth.

<sub>5</sub> Let the people praise thee, O God<span> * </span>let all the people praise thee.

<sub>6</sub> Then shall the earth bring forth her increase<span> * </span>and God, even our own God, shall give us his blessing.

<sub>7</sub> God shall bless us<span> * </span>and all the ends of the world shall fear him.

