#### *Psalm 75. Confitebimur tibi*
UNTO thee, O God, do we give thanks<span> * </span>yea, unto thee do we give thanks.

<sub>2</sub> Thy Name also is so nigh<span> * </span>and that do thy wondrous works declare.

<sub>3</sub> When I receive the congregation<span> * </span>I shall judge according unto right.

<sub>4</sub> The earth is weak, and all the inhabiters thereof<span> * </span>I bear up the pillars of it.

#### *5. I said unto the fools, Deal not so madly: and to the ungodly, Set not up your horn.*
<sub>6</sub> Set not up your horn on high<span> * </span>and speak not with a stiff neck.

<sub>7</sub> For promotion cometh neither from the east, nor from the west<span> * </span>nor yet from the south.

<sub>8</sub> And why? God is the Judge<span> * </span>he putteth down one, and setteth up another.

<sub>9</sub> For in the hand of the Lord there is a cup, and the wine is red<span> * </span>it is full mixed, and he poureth out of the same.

<sub>10</sub> As for the dregs thereof<span> * </span>all the ungodly of the earth shall drink them, and suck them out.

<sub>11</sub> But I will talk of the God of Jacob<span> * </span>and praise him for ever.

<sub>12</sub> All the horns of the ungodly also will I break<span> * </span>and the horns of the righteous shall be exalted.

#### *Psalm 76. Notus in Judaea*
IN JEWRY is God known<span> * </span>his Name is great in Israel.

<sub>2</sub> At Salem is his tabernacle<span> * </span>and his dwelling in Sion.

<sub>3</sub> There brake he the arrows of the bow<span> * </span>the shield, the sword, and the battle.

<sub>4</sub> Thou art of more honour and might<span> * </span>than the hills of the robbers.

<sub>5</sub> The proud are robbed, they have slept their sleep<span> * </span>and all the men whose hands were mighty have found nothing.

<sub>6</sub> At thy rebuke, O God of Jacob<span> * </span>both the chariot and horse are fallen.

<sub>7</sub> Thou, even thou art to be feared<span> * </span>and who may stand in thy sight when thou art angry?

<sub>8</sub> Thou didst cause thy judgement to be heard from heaven<span> * </span>the earth trembled, and was still;

<sub>9</sub> When God arose to judgement<span> * </span>and to help all the meek upon earth.

<sub>10</sub> The fierceness of man shall turn to thy praise<span> * </span>and the fierceness of them shalt thou refrain.

<sub>11</sub> Promise unto the Lord your God, and keep it, all ye that are round about him<span> * </span>bring presents unto him that ought to be feared.

<sub>12</sub> He shall refrain the spirit of princes<span> * </span>and is wonderful among the kings of the earth.

#### *Psalm 77. Voce mea ad Dominum*
IWILL cry unto God with my voice<span> * </span>even unto God will I cry with my voice, and he shall hearken unto me.

<sub>2</sub> In the time of my trouble I sought the Lord<span> * </span>my sore ran and ceased not in the night-season ; my soul refused comfort.

<sub>3</sub> When I am in heaviness, I will think upon God<span> * </span>when my heart is vexed, I will complain.

<sub>4</sub> Thou holdest mine eyes waking<span> * </span>I am so feeble, that I cannot speak.

<sub>5</sub> I have considered the days of old<span> * </span>and the years that are past.

<sub>6</sub> I call to remembrance my song<span> * </span>and in the night I commune with mine own heart, and search out my spirits.

<sub>7</sub> Will the Lord absent himself for ever<span> * </span>and will he be no more intreated?

<sub>8</sub> Is his mercy clean gone for ever<span> * </span>and is his promise come utterly to an end for evermore?

<sub>9</sub> Hath God forgotten to be gracious<span> * </span>and will he shut up his loving-kindness in displeasure?

<sub>10</sub> And I said, It is mine own infirmity<span> * </span>but I will remember the years of the right hand of the most Highest.

<sub>11</sub> I will remember the works of the Lord<span> * </span>and call to mind thy wonders of old time.

<sub>12</sub> I will think also of all thy works<span> * </span>and my talking shall be of thy doings.

<sub>13</sub> Thy way, O God, is holy<span> * </span>who is so great a God as our God?

<sub>14</sub> Thou art the God that doeth wonders<span> * </span>and hast declared thy power among the people.

<sub>15</sub> Thou hast mightily delivered thy people<span> * </span>even the sons of Jacob and Joseph.

<sub>16</sub> The waters saw thee, O God, the waters saw thee, and were afraid<span> * </span>the depths also were troubled.

<sub>17</sub> The clouds poured out water, the air thundered<span> * </span>and thine arrows went abroad.

<sub>18</sub> The voice of thy thunder was heard round about<span> * </span>the lightnings shone upon the ground : the earth was moved, and shook withal.

<sub>19</sub> Thy way is in the sea, and thy paths in the great waters<span> * </span>and thy footsteps are not known.

<sub>20</sub> Thou leddest thy people like sheep<span> * </span>by the hand of Moses and Aaron.

