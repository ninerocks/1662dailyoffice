#### *Psalm 9. Confitebor tibi*
IWILL give thanks unto thee, O Lord, with my whole heart<span> * </span>I will speak of all thy marvellous works.

<sub>2</sub> I will be glad and rejoice in thee<span> * </span>yea, my songs will I make of thy Name, O thou most Highest.

<sub>3</sub> While mine enemies are driven back<span> * </span>they shall fall and perish at thy presence.

<sub>4</sub> For thou hast maintained my right and my cause<span> * </span>thou art set in the throne that judgest right.

<sub>5</sub> Thou hast rebuked the heathen, and destroyed the ungodly<span> * </span>thou hast put out their name for ever and ever.

<sub>6</sub> O thou enemy, destructions are come to a perpetual end<span> * </span>even as the cities which thou hast destroyed, their memorial is perished with them.

<sub>7</sub> But the Lord shall endure for ever<span> * </span>he hath also prepared his seat for judgement.

<sub>8</sub> For he shall judge the world in righteousness<span> * </span>and minister true judgement unto the people.

<sub>9</sub> The Lord also will be a defence for the oppressed<span> * </span>even a refuge in due time of trouble.

<sub>10</sub> And they that know thy Name will put their trust in thee<span> * </span>for thou, Lord, hast never failed them that seek thee.

<sub>11</sub> O praise the Lord which dwelleth in Sion<span> * </span>shew the people of his doings.

<sub>12</sub> For when he maketh inquisition for blood, he remembereth them<span> * </span>and forgetteth not the complaint of the poor.

<sub>13</sub> Have mercy upon me, O Lord; consider the trouble which I suffer of them that hate me<span> * </span>thou that liftest me up from the gates of death.

<sub>14</sub> That I may shew all thy praises within the ports of the daughter of Sion<span> * </span>I will rejoice in thy salvation.

<sub>15</sub> The heathen are sunk down in the pit that they made<span> * </span>in the same net which they hid privily, is their foot taken.

<sub>16</sub> The Lord is known to execute judgement<span> * </span>the ungodly is trapped in the work of his own hands.

<sub>17</sub> The wicked shall be turned into hell<span> * </span>and all the people that forget God.

<sub>18</sub> For the poor shall not alway be forgotten<span> * </span>the patient abiding of the meek shall not perish for ever.

<sub>19</sub> Up, Lord, and let not man have the upper hand<span> * </span>let the heathen be judged in thy sight.

<sub>20</sub> Put them in fear, O Lord<span> * </span>that the heathen may know themselves to be but men.

#### *Psalm 10. Ut quid, Domine?*
WHY standest thou so far off, O Lord<span> * </span>and hidest thy face in the needful time of trouble?

<sub>2</sub> The ungodly for his own lust doth persecute the poor<span> * </span>let them be taken in the crafty wiliness that they have imagined.

<sub>3</sub> For the ungodly hath made boast of his own heart's desire<span> * </span>and speaketh good of the covetous, whom God abhorreth.

#### *4. The ungodly is so proud, that he careth not for God ; neither is God in all his thoughts.*
<sub>5</sub> His ways are alway grievous<span> * </span>thy judgements are far above out of his sight, and therefore defieth he all his enemies.

<sub>6</sub> For he hath said in his heart, Tush, I shall never be cast down<span> * </span>there shall no harm happen unto me.

<sub>7</sub> His mouth is full of cursing, deceit, and fraud<span> * </span>under his tongue is ungodliness and vanity.

<sub>8</sub> He sitteth lurking in the thievish corners of the streets<span> * </span>and privily in his lurking dens doth he murder the innocent; his eyes are set against the poor.

<sub>9</sub> For he lieth waiting secretly, even as a lion lurketh he in his den<span> * </span>that he may ravish the poor.

<sub>10</sub> He doth ravish the poor<span> * </span>when he getteth him into his net.

<sub>11</sub> He falleth down, and humbleth himself<span> * </span>that the congregation of the poor may fall into the hands of his captains.

<sub>12</sub> He hath said in his heart, Tush, God hath forgotten<span> * </span>he hideth away his face, and he will never see it.

<sub>13</sub> Arise, O Lord God, and lift up thine hand<span> * </span>forget not the poor.

<sub>14</sub> Wherefore should the wicked blaspheme God<span> * </span>while he doth say in his heart, Tush, thou God carest not for it.

<sub>15</sub> Surely thou hast seen it<span> * </span>for thou beholdest ungodliness and wrong.

<sub>16</sub> That thou mayest take the matter into thy hand<span> * </span>the poor committeth himself unto thee; for thou art the helper of the friendless.

<sub>17</sub> Break thou the power of the ungodly and malicious<span> * </span>take away his ungodliness, and thou shalt find none.

<sub>18</sub> The Lord is King for ever and ever<span> * </span>and the heathen are perished out of the land.

<sub>19</sub> Lord, thou hast heard the desire of the poor<span> * </span>thou preparest their heart, and thine ear hearkeneth thereto;

<sub>20</sub> To help the fatherless and poor unto their right<span> * </span>that the man of the earth be no more exalted against them.

#### *Psalm 11. In Domino confido*
IN THE Lord put I my trust<span> * </span>how say ye then to my soul, that she should flee as a bird unto the hill?

<sub>2</sub> For lo, the ungodly bend their bow, and make ready their arrows within the quiver<span> * </span>that they may privily shoot at them which are true of heart.

<sub>3</sub> For the foundations will be cast down<span> * </span>and what hath the righteous done?

<sub>4</sub> The Lord is in his holy temple<span> * </span>the Lord's seat is in heaven.

<sub>5</sub> His eyes consider the poor<span> * </span>and his eye-lids try the children of men.

<sub>6</sub> The Lord alloweth the righteous<span> * </span>but the ungodly, and him that delighteth in wickedness, doth his soul abhor.

<sub>7</sub> Upon the ungodly he shall rain snares, fire and brimstone, storm and tempest<span> * </span>this shall be their portion to drink.

<sub>8</sub> For the righteous Lord loveth righteousness<span> * </span>his countenance will behold the thing that is just.

