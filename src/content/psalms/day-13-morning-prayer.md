#### *Psalm 68. Exurgat Deus*
LET God arise, and let his enemies be scattered<span> * </span>let them also that hate him flee before him.

<sub>2</sub> Like as the smoke vanisheth, so shalt thou drive them away<span> * </span>and like as wax melteth at the fire, so let the ungodly perish at the presence of God.

<sub>3</sub> But let the righteous be glad and rejoice before God<span> * </span>let them also be merry and joyful.

<sub>4</sub> O sing unto God, and sing praises unto his Name<span> * </span>magnify him that rideth upon the heavens, as it were upon an horse; praise him in his Name JAH, and rejoice before him.

<sub>5</sub> He is a father of the fatherless, and defendeth the cause of the widows<span> * </span>even God in his holy habitation.

<sub>6</sub> He is the God that maketh men to be of one mind in an house, and bringeth the prisoners out of captivity<span> * </span>but letteth the runagates continue in scarceness.

<sub>7</sub> O God, when thou wentest forth before the people<span> * </span>when thou wentest through the wilderness;

<sub>8</sub> The earth shook, and the heavens dropped at the presence of God<span> * </span>even as Sinai also was moved at the presence of God, who is the God of Israel.

<sub>9</sub> Thou, O God, sentest a gracious rain upon thine inheritance<span> * </span>and refreshedst it when it was weary.

<sub>10</sub> Thy congregation shall dwell therein<span> * </span>for thou, O God, hast of thy goodness prepared for the poor.

<sub>11</sub> The Lord gave the word<span> * </span>great was the company of the preachers.

<sub>12</sub> Kings with their armies did flee, and were discomfited<span> * </span>and they of the household divided the spoil.

<sub>13</sub> Though ye have lien among the pots, yet shall ye be as the wings of a dove<span> * </span>that is covered with silver wings, and her feathers like gold.

<sub>14</sub> When the Almighty scattered kings for their sake<span> * </span>then were they as white as snow in Salmon.

<sub>15</sub> As the hill of Basan, so is God's hill<span> * </span>even an high hill, as the hill of Basan.

<sub>16</sub> Why hop ye so, ye high hills? this is God's hill, in the which it pleaseth him to dwell<span> * </span>yea, the Lord will abide in it for ever.

<sub>17</sub> The chariots of God are twenty thousand, even thousands of angels<span> * </span>and the Lord is among them, as in the holy place of Sinai.

<sub>18</sub> Thou art gone up on high, thou hast led captivity captive, and received gifts for men<span> * </span>yea, even for thine enemies, that the Lord God might dwell among them.

<sub>19</sub> Praised be the Lord daily<span> * </span>even the God who helpeth us, and poureth his benefits upon us.

<sub>20</sub> He is our God, even the God of whom cometh salvation<span> * </span>God is the Lord, by whom we escape death.

<sub>21</sub> God shall wound the head of his enemies<span> * </span>and the hairy scalp of such a one as goeth on still in his wickedness.

<sub>22</sub> The Lord hath said, I will bring my people again, as I did from Basan<span> * </span>mine own will I bring again, as I did sometime from the deep of the sea.

<sub>23</sub> That thy foot may be dipped in the blood of thine enemies<span> * </span>and that the tongue of thy dogs may be red through the same.

<sub>24</sub> It is well seen, O God, how thou goest<span> * </span>how thou, my God and King, goest in the sanctuary.

<sub>25</sub> The singers go before, the minstrels follow after<span> * </span>in the midst are the damsels playing with the timbrels.

<sub>26</sub> Give thanks, O Israel, unto God the Lord in the congregations<span> * </span>from the ground of the heart.

<sub>27</sub> There is little Benjamin their ruler, and the princes of Judah their counsel<span> * </span>the princes of Zabulon, and the princes of Nephthali.

<sub>28</sub> Thy God hath sent forth strength for thee<span> * </span>stablish the thing, O God, that thou hast wrought in us,

<sub>29</sub> For thy temple's sake at Jerusalem<span> * </span>so shall kings bring presents unto thee.

<sub>30</sub> When the company of the spear-men, and multitude of the mighty are scattered abroad among the beasts of the people, so that they humbly bring pieces of silver<span> * </span>and when he hath scattered the people that delight in war;

<sub>31</sub> Then shall the princes come out of Eqypt<span> * </span>the Morians' land shall soon stretch our her hands unto God.

<sub>32</sub> Sing unto God, O ye kingdoms of the earth<span> * </span>O sing praises unto the Lord;

<sub>33</sub> Who sitteth in the heavens over all from the beginning<span> * </span>lo, he doth send out his voice, yea, and that a mighty voice.

<sub>34</sub> Ascribe ye the power to God over Israel<span> * </span>his worship and strength is in the clouds.

<sub>35</sub> O God, wonderful art thou in thy holy places<span> * </span>even the God of Israel, he will give strength and power unto his people; blessed be God.

