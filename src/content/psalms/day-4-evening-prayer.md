#### *Psalm 22. Deus, Deus meus*
MY GOD, my God, look upon me; why hast thou forsaken me<span> * </span>and art so far from my health, and from the words of my complaint?

<sub>2</sub> O my God, I cry in the day-time, but thou hearest not<span> * </span>and in the night-season also I take no rest.

<sub>3</sub> And thou continuest holy<span> * </span>O thou worship of Israel.

<sub>4</sub> Our fathers hoped in thee<span> * </span>they trusted in thee, and thou didst deliver them.

<sub>5</sub> They called upon thee, and were holpen<span> * </span>they put their trust in thee, and were not confounded.

<sub>6</sub> But as for me, I am a worm, and no man<span> * </span>a very scorn of men, and the outcast of the people.

<sub>7</sub> All they that see me laugh me to scorn<span> * </span>they shoot our their lips, and shake their heads, saying,

<sub>8</sub> He trusted in God, that he would deliver him<span> * </span>let him deliver him, if he will have him.

<sub>9</sub> But thou art he that took me out of my mother's womb<span> * </span>thou wast my hope, when I hanged yet upon my mother's breasts.

<sub>10</sub> I have been left unto thee ever since I was born<span> * </span>thou art my God, even from my mother's womb.

<sub>11</sub> O go not from me, for trouble is hard at hand<span> * </span>and there is none to help me.

<sub>12</sub> Many oxen are come about me<span> * </span>fat bulls of Basan close me in on every side.

<sub>13</sub> They gape upon me with their mouths<span> * </span>as it were a ramping and a roaring lion.

<sub>14</sub> I am poured out like water, and all my bones are out of joint<span> * </span>my heart also in the midst of my body is even like melting wax.

<sub>15</sub> My strength is dried up like a potsherd, and my tongue cleaveth to my gums<span> * </span>and thou shalt bring me into the dust of death.

<sub>16</sub> For many dogs are come about me<span> * </span>and the council of the wicked layeth siege against me.

<sub>17</sub> They pierced my hands and my feet; I may tell all my bones<span> * </span>they stand staring and looking upon me.

<sub>18</sub> They part my garments among them<span> * </span>and casts lots upon my vesture.

<sub>19</sub> But be not thou far from me, O Lord<span> * </span>thou art my succour, haste thee to help me.

<sub>20</sub> Deliver my soul from the sword<span> * </span>my darling from the power of the dog.

<sub>21</sub> Save me from the lion's mouth<span> * </span>thou hast heard me also from among the horns of the unicorns.

<sub>22</sub> I will declare thy Name unto my brethren<span> * </span>in the midst of the congregation will I praise thee.

<sub>23</sub> O praise the Lord, ye that fear him<span> * </span>magnify him, all ye of the seed of Jacob, and fear him, all ye seed of Israel.

<sub>24</sub> For he hath not despised, nor abhorred, the low estate of the poor<span> * </span>he hath not hid his face from him, but when he called unto him he heard him.

<sub>25</sub> My praise is of thee in the great congregation<span> * </span>my vows will I perform in the sight of them that fear him.

<sub>26</sub> The poor shall eat and be satisfied<span> * </span>they that seek after the Lord shall praise him; your heart shall live for ever.

<sub>27</sub> All the ends of the world shall remember themselves, and be turned unto the Lord<span> * </span>and all the kindreds of the nations shall worship before him.

<sub>28</sub> For the kingdom is the Lord's<span> * </span>and he is the Governor among the people.

<sub>29</sub> All such as be fat upon earth<span> * </span>have eaten and worshipped.

<sub>30</sub> All they that go down into the dust shall kneel before him<span> * </span>and no man hath quickened his own soul.

<sub>31</sub> My seed shall serve him<span> * </span>they shall be counted unto the Lord for a generation.

<sub>32</sub> They shall come, and the heavens shall declare his righteousness<span> * </span>unto a people that shall be born, whom the Lord hath made.

#### *Psalm 23. Dominus regit me.*
THE Lord is my shepherd<span> * </span>therefore can I lack nothing.

<sub>2</sub> He shall feed me in a green pasture<span> * </span>and lead me forth beside the waters of comfort.

<sub>3</sub> He shall convert my soul<span> * </span>and bring me forth in the paths of righteousness, for his Name's sake.

<sub>4</sub> Yea, though I walk through the valley of the shadow of death, I will fear no evil<span> * </span>for thou art with me; thy rod and thy staff comfort me.

<sub>5</sub> Thou shalt prepare a table before me against them that trouble me<span> * </span>thou hast anointed my head with oil, and my cup shall be full.

<sub>6</sub> But thy loving-kindness and mercy shall follow me all the days of my life<span> * </span>and I will dwell in the house of the Lord for ever.

