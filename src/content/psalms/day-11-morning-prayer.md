#### *Psalm 56. Miserere mei, Deus*
BE MERCIFUL unto me, O God, for man goeth about to devour me<span> * </span>he is daily fighting, and troubling me.

<sub>2</sub> Mine enemies are daily in hand to swallow me up<span> * </span>for they be many that fight against me, O thou most Highest.

<sub>3</sub> Nevertheless, though I am sometime afraid<span> * </span>yet put I my trust in thee.

<sub>4</sub> I will praise God, because of his word<span> * </span>I have put my trust in God, and will not fear what flesh can do unto me.

<sub>5</sub> They daily mistake my words<span> * </span>all that they imagine is to do me evil.

<sub>6</sub> They hold all together, and keep themselves close<span> * </span>and mark my steps, when they lay wait for my soul.

<sub>7</sub> Shall they escape for their wickedness<span> * </span>thou, O God, in thy displeasure shalt cast them down.

<sub>8</sub> Thou tellest my flittings; put my tears into thy bottle<span> * </span>are not these things noted in thy book?

<sub>9</sub> Whensoever I call upon thee, then shall mine enemies be put to flight<span> * </span>this I know; for God is on my side.

<sub>10</sub> In God's word I will rejoice<span> * </span>in the Lord's word will I comfort me.

<sub>11</sub> Yea, in God have I put my trust<span> * </span>I will not be afraid what man can do unto me.

<sub>12</sub> Unto thee, O God, will I pay my vows<span> * </span>unto thee will I give thanks.

<sub>13</sub> For thou hast delivered my soul from death, and my feet from falling<span> * </span>that I may walk before God in the light of the living.

#### *Psalm 57 . Miserere mei, Deus*
BE MERCIFUL unto me, O God, be merciful unto me, for my soul trusteth in thee<span> * </span>and under the shadow of thy wings shall be my refuge, until this tyranny be over-past.

<sub>2</sub> I will call unto the most high God<span> * </span>even unto the God that shall perform the cause which I have in hand.

<sub>3</sub> He shall send from heaven<span> * </span>and save me from the reproof of him that would eat me up.

<sub>4</sub> God shall send forth his mercy and truth<span> * </span>my soul is among lions.

<sub>5</sub> And I lie even among the children of men, that are set on fire<span> * </span>whose teeth are spears and arrows, and their tongue a sharp sword

<sub>6</sub> Set up thyself, O God, above the heavens<span> * </span>and thy glory above all the earth.

<sub>7</sub> They have laid a net for my feet, and pressed down my soul<span> * </span>they have digged a pit before me, and are fallen into the midst of it themselves.

<sub>8</sub> My heart is fixed, O God, my heart is fixed<span> * </span>I will sing, and give praise.

<sub>9</sub> Awake up, my glory; awake, lute and harp<span> * </span>I myself will awake right early.

<sub>10</sub> I will give thanks unto thee, O Lord, among the people<span> * </span>and I will sing unto thee among the nations.

<sub>11</sub> For the greatness of thy mercy reacheth unto the heavens<span> * </span>and thy truth unto the clouds.

<sub>12</sub> Set up thyself, O God, above the heavens<span> * </span>and thy glory above all the earth.

#### *Psalm 58. Si vere utique*
ARE your minds set upon righteousness, O ye congregation<span> * </span>and do ye judge the thing that is right, O ye sons of men?

<sub>2</sub> Yea, ye imagine mischief in your heart upon the earth<span> * </span>and your hands deal with wickedness.

<sub>3</sub> The ungodly are froward, even from their mother's womb<span> * </span>as soon as they are born, they go astray, and speak lies.

<sub>4</sub> They are as venomous as the poison of a serpent<span> * </span>even like the deaf adder that stoppeth her ears;

<sub>5</sub> Which refuseth to hear the voice of the charmer<span> * </span>charm he never so wisely.

<sub>6</sub> Break their teeth, O God, in their mouths; smite the jaw-bones of the lions, O Lord<span> * </span>let them fall away like water that runneth apace; and when they shoot their arrows let them be rooted out.

<sub>7</sub> Let them consume away like a snail, and be like the untimely fruit of a woman<span> * </span>and let them not see the sun.

<sub>8</sub> Or ever your pots be made hot with thorns<span> * </span>so let indignation vex him, even as a thing that is raw.

<sub>9</sub> The righteous shall rejoice when he seeth the vengeance<span> * </span>he shall wash his footsteps in the blood of the ungodly.

<sub>10</sub> So that a man shall say, Verily there is a reward for the righteous<span> * </span>doubtless there is a God that judgeth the earth.

