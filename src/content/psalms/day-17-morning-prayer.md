#### *Psalm 86. Inclina, Domine*
BOW down thine ear, O Lord, and hear me<span> * </span>for I am poor, and in misery.

<sub>2</sub> Preserve thou my soul, for I am holy<span> * </span>my God, save thy servant that putteth his trust in thee.

<sub>3</sub> Be merciful unto me, O Lord<span> * </span>for I will call daily upon thee.

<sub>4</sub> Comfort the soul of thy servant<span> * </span>for unto thee, O Lord, do I lift up my soul.

<sub>5</sub> For thou, Lord, art good and gracious<span> * </span>and of great mercy unto all them that call upon thee.

<sub>6</sub> Give ear, Lord, unto my prayer<span> * </span>and ponder the voice of my humble desires.

<sub>7</sub> In the time of my trouble I will call upon thee<span> * </span>for thou hearest me.

<sub>8</sub> Among the gods there is none like unto thee, O Lord<span> * </span>there is not one that can do as thou doest.

<sub>9</sub> All nations whom thou hadst made shall come and worship thee, O Lord<span> * </span>and shall glorify thy Name.

<sub>10</sub> For thou art great, and doest wondrous things<span> * </span>thou art God alone.

<sub>11</sub> Teach me thy way, O Lord, and I will walk in thy truth<span> * </span>O knit my heart unto thee, that I may fear thy Name.

<sub>12</sub> I will thank thee, O Lord my God, with all my heart<span> * </span>and will praise thy Name for evermore.

<sub>13</sub> For great is thy mercy toward me<span> * </span>and thou hast delivered my soul from the nethermost hell.

<sub>14</sub> O God, the proud are risen against me<span> * </span>and the congregations of naughty men have sought after my soul, and have not set thee before their eyes.

<sub>15</sub> But thou, O Lord God, art full of compassion and mercy<span> * </span>long-suffering, plenteous in goodness and truth.

<sub>16</sub> O turn thee then unto me, and have mercy upon me<span> * </span>give thy strength unto thy servant, and help the son of thine handmaid.

<sub>17</sub> Shew some token upon me for good, that they who hate me may see it and be ashamed<span> * </span>because thou, Lord, hast holpen me and comforted me.

#### *Psalm 87. Fundamenta ejus*
HER foundations are upon the holy hills<span> * </span>the Lord loveth the gates of Sion more than all the dwellings of Jacob.

<sub>2</sub> Very excellent things are spoken of thee<span> * </span>thou city of God.

<sub>3</sub> I will think upon Rahab and Babylon<span> * </span>with them that know me.

<sub>4</sub> Behold ye the Philistines also<span> * </span>and they of Tyre, with the Morians; lo, there was he born.

<sub>5</sub> And of Sion it shall be reported that he was born in her<span> * </span>and the most High shall stablish her.

<sub>6</sub> The Lord shall rehearse it when he writeth up the people<span> * </span>that he was born there.

<sub>7</sub> The singers also and trumpeters shall he rehearse<span> * </span>All my fresh springs shall be in thee.

#### *Psalm 88. Domine Deus*
OLORD God of my salvation, I have cried day and night before thee<span> * </span>O let my prayer enter into thy presence, incline thine ear unto my calling.

<sub>2</sub> For my soul is full of trouble<span> * </span>and my life draweth nigh unto hell.

<sub>3</sub> I am counted as one of them that go down into the pit<span> * </span>and I have been even as a man that hath no strength.

<sub>4</sub> Free among the dead, like unto them that are wounded, and lie in the grave<span> * </span>who are out of remembrance, and are cut away from thy hand.

<sub>5</sub> Thou hast laid me in the lowest pit<span> * </span>in a place of darkness, and in the deep.

<sub>6</sub> Thine indignation lieth hard upon me<span> * </span>and thou hast vexed me with all thy storms.

<sub>7</sub> Thou hast put away mine acquaintance far from me<span> * </span>and made me to be abhorred of them.

<sub>8</sub> I am so fast in prison<span> * </span>that I cannot get forth.

<sub>9</sub> My sight faileth for very trouble<span> * </span>Lord, I have called daily upon thee, I have stretched forth my hands unto thee.

<sub>10</sub> Dost thou shew wonders among the dead<span> * </span>or shall the dead rise up again, and praise thee?

<sub>11</sub> Shall thy loving-kindness be shewed in the grave<span> * </span>or thy faithfulness in destruction?

<sub>12</sub> Shall thy wondrous works be known in the dark<span> * </span>and thy righteousness in the land where all things are forgotten?

<sub>13</sub> Unto thee have I cried, O Lord<span> * </span>and early shall my prayer come before thee.

<sub>14</sub> Lord, why abhorrest thou my soul<span> * </span>and hidest thou thy face from me?

<sub>15</sub> I am in misery, and like unto him that is at the point to die<span> * </span>even from my youth up thy terrors have I suffered with a troubled mind.

<sub>16</sub> Thy wrathful displeasure goeth over me<span> * </span>and the fear of thee hath undone me.

<sub>17</sub> They came round about me daily like water<span> * </span>and compassed me together on every side.

#### *18. My lovers and friends hast thou put away from me: and hid mine acquaintance out of my sight.*
