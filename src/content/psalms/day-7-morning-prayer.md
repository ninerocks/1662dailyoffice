#### *Psalm 35. Judica, Domine*
PLEAD thou my cause, O Lord, with them that strive with me<span> * </span>and fight thou against them that fight against me.

<sub>2</sub> Lay hand upon the shield and buckler<span> * </span>and stand up to help me.

<sub>3</sub> Bring forth the spear, and stop the way against them that persecute me<span> * </span>say unto my soul, I am thy salvation.

<sub>4</sub> Let them be confounded and put to shame, that seek after my soul<span> * </span>let them be turned back and brought to confusion, that imagine mischief for me.

<sub>5</sub> Let them be as the dust before the wind<span> * </span>and the angel of the Lord scattering them.

<sub>6</sub> Let their way be dark and slippery<span> * </span>and let the angel of the Lord persecute them.

<sub>7</sub> For they have privily laid their net to destroy me without a cause<span> * </span>yea, even without a cause have they made a pit for my soul.

<sub>8</sub> Let a sudden destruction come upon him unawares, and his net, that he hath laid privily, catch himself<span> * </span>that he may fall into his own mischief.

<sub>9</sub> And, my soul, be joyful in the Lord<span> * </span>it shall rejoice in his salvation.

<sub>10</sub> All my bones shall say, Lord, who is like unto thee, who deliverest the poor from him that is too strong for him<span> * </span>yea, the poor, and him that is in misery, from him that spoileth him?

<sub>11</sub> False witnesses did rise up<span> * </span>they laid to my charge things that I knew not.

<sub>12</sub> They rewarded me evil for good<span> * </span>to the great discomfort of my soul.

<sub>13</sub> Nevertheless, when they were sick, I put on sackcloth, and humbled my soul with fasting<span> * </span>and my prayer shall turn into mine own bosom.

<sub>14</sub> I behaved myself as though it had been my friend or my brother<span> * </span>I went heavily, as one that mourneth for his mother.

<sub>15</sub> But in mine adversity they rejoiced, and gathered themselves together<span> * </span>yea, the very abjects came together against me unawares, making mouths at me, and ceased not.

<sub>16</sub> With the flatterers were busy mockers<span> * </span>who gnashed upon me with their teeth.

<sub>17</sub> Lord, how long wilt thou look upon this<span> * </span>O deliver my soul from the calamities which they bring on me, and my darling from the lions.

<sub>18</sub> So will I give thee thanks in the great congregation<span> * </span>I will praise thee among much people.

<sub>19</sub> O let not them that are mine enemies triumph over me ungodly<span> * </span>neither let them wink with their eyes that hate me without a cause.

#### *20. And why? their communing is not for peace; but they imagine deceitful words against them that are quiet in the land.*
<sub>21</sub> They gaped upon me with their mouths, and said<span> * </span>Fie on thee, fie on thee, we saw it with our eyes.

<sub>22</sub> This thou hast seen, O Lord<span> * </span>hold not thy tongue then, go not far from me, O Lord.

<sub>23</sub> Awake, and stand up to judge my quarrel<span> * </span>avenge thou my cause, my God, and my Lord.

<sub>24</sub> Judge me, O Lord my God, according to thy righteousness<span> * </span>and let them not triumph over me.

<sub>25</sub> Let them not say in their hearts, There, there, so would we have it<span> * </span>neither let them say, We have devoured him.

<sub>26</sub> Let them be put to confusion and shame together, that rejoice at my trouble<span> * </span>let them be clothed with rebuke and dishonour, that boast themselves against me.

<sub>27</sub> Let them be glad and rejoice, that favour my righteous dealing<span> * </span>yea, let them say alway, Blessed be the Lord, who hath pleasure in the prosperity of his servant.

<sub>28</sub> And as for my tongue, it shall be talking of thy righteousness<span> * </span>and of thy praise all the day long.

#### *Psalm 36. Dixit injustus*
MY HEART sheweth me the wickedness of the ungodly<span> * </span>that there is no fear of God before his eyes.

<sub>2</sub> For he flattereth himself in his own sight<span> * </span>until his abominable sin be found out.

<sub>3</sub> The words of his mouth are unrighteous, and full of deceit<span> * </span>he hath left off to behave himself wisely, and to do good.

<sub>4</sub> He imagineth mischief upon his bed, and hath set himself in no good way<span> * </span>neither doth he abhor any thing that is evil.

<sub>5</sub> Thy mercy, O Lord, reacheth unto the heavens<span> * </span>and thy faithfulness unto the clouds.

<sub>6</sub> Thy righteousness standeth like the strong mountains<span> * </span>thy judgements are like the great deep.

<sub>7</sub> Thou, Lord, shalt save both man and beast; How excellent is thy mercy, O God<span> * </span>and the children of men shall put their trust under the shadow of thy wings.

<sub>8</sub> They shall be satisfied with the plenteousness of thy house<span> * </span>and thou shalt give them drink of thy pleasures, as out of the river.

<sub>9</sub> For with thee is the well of life<span> * </span>and in thy light shall we see light.

<sub>10</sub> O continue forth thy loving-kindness unto them that know thee<span> * </span>and thy righteousness unto them that are true of heart.

<sub>11</sub> O let not the foot of pride come against me<span> * </span>and let not the hand of the ungodly cast me down.

<sub>12</sub> There are they fallen, all that work wickedness<span> * </span>they are cast down, and shall not be able to stand.

