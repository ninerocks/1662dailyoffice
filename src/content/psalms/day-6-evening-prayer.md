#### *Psalm 32. Beati, quorum*
BLESSED is he whose unrighteousness is forgiven<span> * </span>and whose sin is covered.

<sub>2</sub> Blessed is the man unto whom the Lord imputeth no sin<span> * </span>and in whose spirit there is no guile.

<sub>3</sub> For while I held my tongue<span> * </span>my bones consumed away through my daily complaining.

<sub>4</sub> For thy hand is heavy upon me day and night<span> * </span>and my moisture is like the drought in summer.

<sub>5</sub> I will acknowledge my sin unto thee<span> * </span>and mine unrighteousness have I not hid.

<sub>6</sub> I said, I will confess my sins unto the Lord<span> * </span>and so thou forgavest the wickedness of my sin.

<sub>7</sub> For this shall every one that is godly make his prayer unto thee, in a time when thou mayest be found<span> * </span>but in the great water-floods they shall not come nigh him.

<sub>8</sub> Thou art a place to hide me in, thou shalt preserve me from trouble<span> * </span>thou shalt compass me about with songs of deliverance.

<sub>9</sub> I will inform thee, and teach thee in the way wherein thou shalt go<span> * </span>and I will guide thee with mine eye.

<sub>10</sub> Be ye not like to horse and mule, which have no understanding<span> * </span>whose mouths must be held with bit and bridle, lest they fall upon thee.

<sub>11</sub> Great plagues remain for the ungodly<span> * </span>but whoso putteth his trust in the Lord, mercy embraceth him on every side.

<sub>12</sub> Be glad, O ye righteous, and rejoice in the Lord<span> * </span>and be joyful, all ye that are true of heart.

#### *Psalm 33. Exultate, justi*
REJOICE in the Lord, O ye righteous<span> * </span>for it becometh well the just to be thankful.

<sub>2</sub> Praise the Lord with harp<span> * </span>sing praises unto him with the lute, and instrument of ten strings.

<sub>3</sub> Sing unto the Lord a new song<span> * </span>sing praises lustily unto him with a good courage.

<sub>4</sub> For the word of the Lord is true<span> * </span>and all his works are faithful.

<sub>5</sub> He loveth righteousness and judgement<span> * </span>the earth is full of the goodness of the Lord.

<sub>6</sub> By the word of the Lord were the heavens made<span> * </span>and all the hosts of them by the breath of his mouth.

<sub>7</sub> He gathereth the waters of the sea together, as it were upon an heap<span> * </span>and layeth up the deep, as in a treasure-house.

<sub>8</sub> Let all the earth fear the Lord<span> * </span>stand in awe of him, all ye that dwell in the world.

<sub>9</sub> For he spake, and it was done<span> * </span>he commanded, and it stood fast.

<sub>10</sub> The Lord bringeth the counsel of the heathen to nought<span> * </span>and maketh the devices of the people to be of none effect, and casteth out the counsels of princes.

<sub>11</sub> The counsel of the Lord shall endure for ever<span> * </span>and the thoughts of his heart from generation to generation.

<sub>12</sub> Blessed are the people, whose God is the Lord Jehovah<span> * </span>and blessed are the folk, that he hath chosen to him to be his inheritance.

<sub>13</sub> The Lord looked down from heaven, and beheld all the children of men<span> * </span>from the habitation of his dwelling he considereth all them that dwell on the earth.

<sub>14</sub> He fashioneth all the hearts of them<span> * </span>and understandeth all their works.

<sub>15</sub> There is no king that can be saved by the multitude of an host<span> * </span>neither is any mighty man delivered by much strength.

<sub>16</sub> A horse is counted but a vain thing to save a man<span> * </span>neither shall he deliver any man by his great strength.

<sub>17</sub> Behold, the eye of the Lord is upon them that fear him<span> * </span>and upon them that put their trust in his mercy;

<sub>18</sub> To deliver their soul from death<span> * </span>and to feed them in the time of dearth.

<sub>19</sub> Our soul hath patiently tarried for the Lord<span> * </span>for he is our help and our shield.

<sub>20</sub> For our heart shall rejoice in him<span> * </span>because we have hoped in his holy Name.

<sub>21</sub> Let thy merciful kindness, O Lord, be upon us<span> * </span>like as we do put our trust in thee.

#### *Psalm 34. Benedicam Domino*
IWILL alway give thanks unto the Lord<span> * </span>his praise shall ever be in my mouth.

<sub>2</sub> My soul shall make her boast in the Lord<span> * </span>the humble shall hear thereof, and be glad.

<sub>3</sub> O praise the Lord with me<span> * </span>and let us magnify his Name together.

<sub>4</sub> I sought the Lord, and he heard me<span> * </span>yea, he delivered me out of all my fear.

<sub>5</sub> They had an eye unto him, and were lightened<span> * </span>and their faces were not ashamed.

<sub>6</sub> Lo, the poor crieth, and the Lord heareth him<span> * </span>yea, and saveth him out of all his troubles.

<sub>7</sub> The angel of the Lord tarrieth round about them that fear him<span> * </span>and delivereth them.

<sub>8</sub> O taste, and see, how gracious the Lord is<span> * </span>blessed is the man that trusteth in him.

<sub>9</sub> O fear the Lord, ye that are his saints<span> * </span>for they that fear him lack nothing.

<sub>10</sub> The lions do lack, and suffer hunger<span> * </span>but they who seek the Lord shall want no manner of thing that is good.

<sub>11</sub> Come, ye children, and hearken unto me<span> * </span>I will teach you the fear of the Lord.

<sub>12</sub> What man is he that lusteth to live<span> * </span>and would fain see good days?

<sub>13</sub> Keep thy tongue from evil<span> * </span>and thy lips, that they speak no guile.

<sub>14</sub> Eschew evil, and do good<span> * </span>seek peace, and ensue it.

<sub>15</sub> The eyes of the Lord are over the righteous<span> * </span>and his ears are open unto their prayers.

<sub>16</sub> The countenance of the Lord is against them that do evil<span> * </span>to root out the remembrance of them from the earth.

<sub>17</sub> The righteous cry, and the Lord heareth them<span> * </span>and delivereth them out of all their troubles.

<sub>18</sub> The Lord is nigh unto them that are of a contrite heart<span> * </span>and will save such as be of an humble spirit.

<sub>19</sub> Great are the troubles of the righteous<span> * </span>but the Lord delivereth him out of all.

<sub>20</sub> He keepeth all his bones<span> * </span>so that not one of them is broken.

<sub>21</sub> But misfortune shall slay the ungodly<span> * </span>and they that hate the righteous shall be desolate.

<sub>22</sub> The Lord delivereth the souls of his servants<span> * </span>and all they that put their trust in him shall not be destitute.

