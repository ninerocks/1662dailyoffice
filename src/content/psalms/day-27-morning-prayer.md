#### *Psalm 120. Ad Dominum*
WHEN I was in trouble I called upon the Lord<span> * </span>and he heard me.

<sub>2</sub> Deliver my soul, O Lord, from lying lips<span> * </span>and from a deceitful tongue.

<sub>3</sub> What reward shall be given or done unto thee, thou false tongue<span> * </span>even mighty and sharp arrows, with hot burning coals.

<sub>4</sub> Woe is me, that I am constrained to dwell with Mesech<span> * </span>and to have my habitation among the tents of Kedar.

<sub>5</sub> My soul hath long dwelt among them<span> * </span>that are enemies unto peace.

<sub>6</sub> I labour for peace, but when I speak unto them thereof<span> * </span>they make them ready to battle.

#### *Psalm 121. Levavi oculus*
IWILL lift up mine eyes unto the hills<span> * </span>from whence cometh my help.

<sub>2</sub> My help cometh even from the Lord<span> * </span>who hath made heaven and earth.

<sub>3</sub> He will not suffer thy foot to be moved<span> * </span>and he that keepeth thee will not sleep.

<sub>4</sub> Behold, he that keepeth Israel<span> * </span>shall neither slumber nor sleep.

<sub>5</sub> The Lord himself is thy keeper<span> * </span>the Lord is thy defence upon thy right hand;

<sub>6</sub> So that the sun shall not burn thee by day<span> * </span>neither the moon by night.

<sub>7</sub> The Lord shall preserve thee from all evil<span> * </span>yea, it is even he that shall keep thy soul.

<sub>8</sub> The Lord shall preserve thy going out, and thy coming in<span> * </span>from this time forth for evermore.

#### *Psalm 122. Laetatus sum*
IWAS glad when they said unto me<span> * </span>We will go into the house of the Lord.

<sub>2</sub> Our feet shall stand in thy gates<span> * </span>O Jerusalem.

<sub>3</sub> Jerusalem is built as a city<span> * </span>that is at unity in itself.

<sub>4</sub> For thither the tribes go up, even the tribes of the Lord<span> * </span>to testify unto Israel, to give thanks unto the Name of the Lord.

<sub>5</sub> For there is the seat of judgement<span> * </span>even the seat of the house of David.

<sub>6</sub> O pray for the peace of Jerusalem<span> * </span>they shall prosper that love thee.

<sub>7</sub> Peace be within thy walls<span> * </span>and plenteousness within thy palaces.

<sub>8</sub> For my brethren and companions' sakes<span> * </span>I will wish thee prosperity.

<sub>9</sub> Yea, because of the house of the Lord our God<span> * </span>I will seek to do thee good.

#### *Psalm 123. Ad te levavi oculos meos*
UNTO thee lift I up mine eyes<span> * </span>O thou that dwellest in the heavens.

<sub>2</sub> Behold, even as the eyes of servants look unto the hand of their masters, and as the eyes of a maiden unto the hand of her mistress<span> * </span>even so our eyes wait upon the Lord our God, until he have mercy upon us.

<sub>3</sub> Have mercy upon us, O Lord, have mercy upon us<span> * </span>for we are utterly despised.

<sub>4</sub> Our soul is filled with the scornful reproof of the wealthy<span> * </span>and with the despitefulness of the proud.

#### *Psalm 124. Nisi quia Dominus*
IF THE Lord himself had not been on our side, now may Israel say<span> * </span>if the Lord himself had not been on our side, when men rose up against us;

<sub>2</sub> They had swallowed us up quick<span> * </span>when thy were so wrathfully displeased at us.

<sub>3</sub> Yea, the waters had drowned us<span> * </span>and the stream had gone over our soul.

<sub>4</sub> The deep waters of the proud<span> * </span>had gone even over our soul.

<sub>5</sub> But praised be the Lord<span> * </span>who hath not given us over for a prey unto their teeth.

<sub>6</sub> Our soul is escaped even as a bird out of the snare of the fowler<span> * </span>the snare is broken, and we are delivered .

<sub>7</sub> Our help standeth in the Name of the Lord<span> * </span>who hath made heaven and earth.

#### *Psalm 125. Qui confidunt*
THEY that put their trust in the Lord shall be even as the mount Sion<span> * </span>which may not be removed, but standeth fast for ever.

<sub>2</sub> The hills stand about Jerusalem<span> * </span>even so standeth the Lord round about his people, from this time forth for evermore.

<sub>3</sub> For the rod of the ungodly cometh not into the lot of the righteous<span> * </span>lest the righteous put their hand unto wickedness.

<sub>4</sub> Do well, O Lord<span> * </span>unto those that are good and true of heart.

<sub>5</sub> As for such as turn back unto their own wickedness<span> * </span>the Lord shall lead them forth with the evil-doers; but peace shall be upon Israel.

