#### *Psalm 108. Paratum cor meum*
OGOD, my heart is ready, my heart is ready<span> * </span>I will sing and give praise with the best member that I have.

<sub>2</sub> Awake, thou lute, and harp<span> * </span>I myself will awake right early.

<sub>3</sub> I will give thanks unto thee, O Lord, among the people<span> * </span>I will sing praises unto thee among the nations.

<sub>4</sub> For thy mercy is greater than the heavens<span> * </span>and thy truth reacheth unto the clouds.

<sub>5</sub> Set up thyself, O God, above the heavens<span> * </span>and thy glory above all the earth.

<sub>6</sub> That thy beloved may be delivered<span> * </span>let thy right hand save them, and hear thou me.

<sub>7</sub> God hath spoken in his holiness<span> * </span>I will rejoice therefore, and divide Sichem, and mete out the valley of Succoth.

<sub>8</sub> Gilead is mine, and Manasses is mine<span> * </span>Ephraim also is the strength of my head.

<sub>9</sub> Judah is my law-giver, Moab is my wash-pot<span> * </span>over Edom will I cast out my shoe, upon Philistia will I triumph.

<sub>10</sub> Who will lead me into the strong city<span> * </span>and who will bring me into Edom?

<sub>11</sub> Hast not thou forsaken us, O God<span> * </span>and wilt not thou, O God, go forth with our hosts?

<sub>12</sub> O help us against the enemy<span> * </span>for vain is the help of man.

<sub>13</sub> Through God we shall do great acts<span> * </span>and it is he that shall tread down our enemies.

#### *Psalm 109. Deus, laudem*
HOLD not thy tongue, O God of my praise<span> * </span>for the mouth of the ungodly, yea, the mouth of the deceitful is opened upon me.

<sub>2</sub> And they have spoken against me with false tongues<span> * </span>they compassed me about also with words of hatred, and fought against me without a cause.

<sub>3</sub> For the love that I had unto them, lo, they take now my contrary part<span> * </span>but I give myself unto prayer.

<sub>4</sub> Thus have they rewarded me evil for good<span> * </span>and hatred for my good will.

<sub>5</sub> Set thou an ungodly man to be ruler over him<span> * </span>and let Satan stand at his right hand.

<sub>6</sub> When sentence is given upon him, let him be condemned<span> * </span>and let his prayer be turned into sin.

<sub>7</sub> Let his days be few<span> * </span>and let another take his office.

<sub>8</sub> Let his children be fatherless<span> * </span>and his wife a widow.

<sub>9</sub> Let his children be vagabonds, and beg their bread<span> * </span>let them seek it also out of desolate places.

<sub>10</sub> Let the extortioner consume all that he hath<span> * </span>and let the stranger spoil his labour.

<sub>11</sub> Let there be no man to pity him<span> * </span>nor to have compassion upon his fatherless children.

<sub>12</sub> Let his posterity be destroyed<span> * </span>and in the next generation let his name be clean put out.

<sub>13</sub> Let the wickedness of his fathers be had in remembrance in the sight of the Lord<span> * </span>and let not the sin of his mother be done away.

<sub>14</sub> Let them alway be before the Lord<span> * </span>that he may root out the memorial of them from off the earth.

<sub>15</sub> And that, because his mind was not to do good<span> * </span>but persecuted the poor helpless man, that he might slay him that was vexed at the heart.

<sub>16</sub> His delight was in cursing, and it shall happen unto him<span> * </span>he loved not blessing, therefore shall it be far from him.

<sub>17</sub> He clothed himself with cursing, like as with a raiment<span> * </span>and it shall come into his bowels like water, and like oil into his bones.

<sub>18</sub> Let it be unto him as the cloke that he hath upon him<span> * </span>and as the girdle that he is alway girded withal.

<sub>19</sub> Let it thus happen from the Lord unto mine enemies<span> * </span>and to those that speak evil against my soul.

<sub>20</sub> But deal thou with me, O Lord God, according unto thy Name<span> * </span>for sweet is thy mercy.

<sub>21</sub> O deliver me, for I am helpless and poor<span> * </span>and my heart is wounded within me.

<sub>22</sub> I go hence like the shadow that departeth<span> * </span>and am driven away as the grasshopper.

<sub>23</sub> My knees are weak through fasting<span> * </span>my flesh is dried up for want of fatness.

<sub>24</sub> I became also a reproach unto them<span> * </span>they that looked upon me shaked their heads.

<sub>25</sub> Help me, O Lord my God<span> * </span>O save me according to thy mercy.

<sub>26</sub> And they shall know, how that this is thy hand<span> * </span>and that thou, Lord, hast done it.

<sub>27</sub> Though they curse, yet bless thou<span> * </span>and let them be confounded that rise up against me; but let thy servant rejoice.

<sub>28</sub> Let mine adversaries be clothed with shame<span> * </span>and let them cover themselves with their own confusion, as with a cloke.

<sub>29</sub> As for me, I will give great thanks unto the Lord with my mouth<span> * </span>and praise him among the multitude.

<sub>30</sub> For he shall stand at the right hand of the poor<span> * </span>to save his soul from the unrighteous judges.

