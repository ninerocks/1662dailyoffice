#### *Psalm 132. Memento, Domine*
LORD, remember David<span> * </span>and all his trouble;

<sub>2</sub> How he sware unto the Lord<span> * </span>and vowed a vow unto the Almighty God of Jacob;

<sub>3</sub> I will not come within the tabernacle of mine house<span> * </span>nor climb up into my bed;

<sub>4</sub> I will not suffer mine eyes to sleep, nor mine eye-lids to slumber<span> * </span>neither the temples of my head to take any rest;

<sub>5</sub> Until I find out a place for the temple of the Lord<span> * </span>an habitation for the mighty God of Jacob.

<sub>6</sub> Lo, we heard of the same at Ephrata<span> * </span>and found it in the wood.

<sub>7</sub> We will go into his tabernacle<span> * </span>and fall low on our knees before his footstool.

<sub>8</sub> Arise, O Lord, into thy resting-place<span> * </span>thou, and the ark of thy strength.

<sub>9</sub> Let thy priests be clothed with righteousness<span> * </span>and let thy saints sing with joyfulness.

<sub>10</sub> For thy servant David's sake<span> * </span>turn not away the presence of thine Anointed.

<sub>11</sub> The Lord hath made a faithful oath unto David<span> * </span>and he shall not shrink from it;

<sub>12</sub> Of the fruit of thy body<span> * </span>shall I set upon thy seat.

<sub>13</sub> If thy children will keep my covenant, and my testimonies that I shall learn them<span> * </span>their children also shall sit upon thy seat for evermore.

<sub>14</sub> For the Lord hath chosen Sion to be an habitation for himself<span> * </span>he hath longed for her.

<sub>15</sub> This shall be my rest for ever<span> * </span>here will I dwell, for I have a delight therein.

<sub>16</sub> I will bless her victuals with increase<span> * </span>and will satisfy her poor with bread.

<sub>17</sub> I will deck her priests with health<span> * </span>and her saints shall rejoice and sing.

<sub>18</sub> There shall I make the horn of David to flourish<span> * </span>I have ordained a lantern for mine Anointed.

<sub>19</sub> As for his enemies, I shall clothe them with shame<span> * </span>but upon himself shall his crown flourish.

#### *Psalm 133. Ecce, quam bonum!*
BEHOLD, how good and joyful a thing it is<span> * </span>brethren, to dwell together in unity!

<sub>2</sub> It is like the precious ointment upon the head, that ran down unto the beard<span> * </span>even unto Aaron's beard, and went down to the skirts of his clothing.

<sub>3</sub> Like as the dew of Hermon<span> * </span>which fell upon the hill of Sion.

<sub>4</sub> For there the Lord promised his blessing<span> * </span>and life for evermore.

#### *Psalm 134. Ecce nunc*
BEHOLD now, praise the Lord<span> * </span>all ye servants of the Lord;

<sub>2</sub> Ye that by night stand in the house of the Lord<span> * </span>even in the courts of the house of our God.

<sub>3</sub> Lift up your hands in the sanctuary<span> * </span>and praise the Lord.

<sub>4</sub> The Lord that made heaven and earth<span> * </span>give thee blessing out of Sion.

#### *Psalm 135. Laudate Nomen*
OPRAISE the Lord, laud ye the Name of the Lord<span> * </span>praise it, O ye servants of the Lord;

<sub>2</sub> Ye that stand in the house of the Lord<span> * </span>in the courts of the house of our God.

<sub>3</sub> O praise the Lord, for the Lord is gracious<span> * </span>O sing praises unto his Name, for it is lovely.

<sub>4</sub> For why? the Lord hath chosen Jacob unto himself<span> * </span>and Israel for his own possession.

<sub>5</sub> For I know that the Lord is great<span> * </span>and that our Lord is above all gods.

<sub>6</sub> Whatsoever the Lord pleased, that did he in heaven and in earth<span> * </span>and in the sea, and in all deep places.

<sub>7</sub> He bringeth forth the clouds from the ends of the world<span> * </span>and sendeth forth lightnings with the rain, bringing the winds out of his treasures.

<sub>8</sub> He smote the first-born of Egypt<span> * </span>both of man and beast.

<sub>9</sub> He hath sent tokens and wonders into the midst of thee, O thou land of Egypt<span> * </span>upon Pharaoh, and all his servants.

<sub>10</sub> He smote divers nations<span> * </span>and slew mighty kings;

<sub>11</sub> Sehon king of the Amorites, and Og the king of Basan<span> * </span>and all the kingdoms of Canaan;

<sub>12</sub> And gave their land to be an heritage<span> * </span>even an heritage unto Israel his people.

<sub>13</sub> Thy Name, O Lord, endureth for ever<span> * </span>so doth thy memorial, O Lord, from one generation to another.

<sub>14</sub> For the Lord will avenge his people<span> * </span>and be gracious unto his servants.

<sub>15</sub> As for the images of the heathen, they are but silver and gold<span> * </span>the work of men's hands.

<sub>16</sub> They have mouths, and speak not<span> * </span>eyes have they, but they see not.

<sub>17</sub> They have ears, and yet they hear not<span> * </span>neither is there any breath in their mouths.

<sub>18</sub> They that make them are like unto them<span> * </span>and so are all they that put their trust in them.

<sub>19</sub> Praise the Lord, ye house of Israel<span> * </span>praise the Lord, ye house of Aaron.

<sub>20</sub> Praise the Lord, ye house of Levi<span> * </span>ye that fear the Lord, praise the Lord.

<sub>21</sub> Praised be the Lord out of Sion<span> * </span>who dwelleth at Jerusalem.

