#### *Psalm 93. Dominus regnavit*
THE Lord is King, and hath put on glorious apparel<span> * </span>the Lord hath put on his apparel, and girded himself with strength.

<sub>2</sub> He hath made the round world so sure<span> * </span>that it cannot be moved.

<sub>3</sub> Ever since the world began hath thy seat been prepared<span> * </span>thou art from everlasting.

<sub>4</sub> The floods are risen, O Lord, the floods have lift up their voice<span> * </span>the floods lift up their waves.

<sub>5</sub> The waves of the sea are mighty, and rage horribly<span> * </span>but yet the Lord, who dwelleth on high, is mightier.

<sub>6</sub> Thy testimonies, O Lord, are very sure<span> * </span>holiness becometh thine house for ever.

#### *Psalm 94. Deus ultionum*
OLORD God, to whom vengeance belongeth<span> * </span>thou God, to whom vengeance belongeth, shew thyself.

<sub>2</sub> Arise, thou Judge of the world<span> * </span>and reward the proud after their deserving.

<sub>3</sub> Lord, how long shall the ungodly<span> * </span>how long shall the ungodly triumph?

<sub>4</sub> How long shall all wicked doers speak so disdainfully<span> * </span>and make such proud boasting?

<sub>5</sub> They smite down thy people, O Lord<span> * </span>and trouble thine heritage.

<sub>6</sub> They murder the widow and the stranger<span> * </span>and put the fatherless to death.

<sub>7</sub> And yet they say, Tush, the Lord shall not see<span> * </span>neither shall the God of Jacob regard it.

<sub>8</sub> Take heed, ye unwise among the people<span> * </span>O ye fools, when will ye understand?

<sub>9</sub> He that planted the ear, shall he not hear<span> * </span>or he that made the eye, shall he not see?

<sub>10</sub> Or he that nurtureth the heathen<span> * </span>it is he that teacheth man knowledge, shall not he punish?

<sub>11</sub> The Lord knoweth the thoughts of man<span> * </span>that they are but vain.

<sub>12</sub> Blessed is the man whom thou chastenest, O Lord<span> * </span>and teachest him in thy law;

<sub>13</sub> That thou mayest give him patience in time of adversity<span> * </span>until the pit be digged up for the ungodly.

<sub>14</sub> For the Lord will not fail his people<span> * </span>neither will he forsake his inheritance;

<sub>15</sub> Until righteousness turn again unto judgement<span> * </span>all such as are true in heart shall follow it.

<sub>16</sub> Who will rise up with me against the wicked<span> * </span>or who will take my part against the evil-doers?

<sub>17</sub> If the Lord had not helped me<span> * </span>it had not failed but my soul had been put to silence.

<sub>18</sub> But when I said, My foot hath slipt<span> * </span>thy mercy, O Lord, held me up.

<sub>19</sub> In the multitude of the sorrows that I had in my heart<span> * </span>thy comforts have refreshed my soul.

<sub>20</sub> Wilt thou have any thing to do with the stool of wickedness<span> * </span>which imagineth mischief as a law?

<sub>21</sub> They gather them together against the soul of the righteous<span> * </span>and condemn the innocent blood.

<sub>22</sub> But the Lord is my refuge<span> * </span>and my God is the strength of my confidence.

<sub>23</sub> He shall recompense them their wickedness, and destroy them in their own malice<span> * </span>yea, the Lord our God shall destroy them.

