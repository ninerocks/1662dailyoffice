#### *Psalm 136. Confitemini*
OGIVE thanks unto the LORD, for he is gracious<span> * </span>and his mercy endureth for ever.

<sub>2</sub> O give thanks unto the God of all gods<span> * </span>for his mercy endureth for ever.

<sub>3</sub> O thank the Lord of all lords<span> * </span>for his mercy endureth for ever.

<sub>4</sub> Who only doeth great wonders<span> * </span>for his mercy endureth for ever.

<sub>5</sub> Who by his excellent wisdom made the heavens<span> * </span>for his mercy endureth for ever.

<sub>6</sub> Who laid out the earth above the waters<span> * </span>for his mercy endureth for ever.

<sub>7</sub> Who hath made great lights<span> * </span>for his mercy endureth for ever;

<sub>8</sub> The sun to rule the day<span> * </span>for his mercy endureth for ever;

<sub>9</sub> The moon and the stars to govern the night<span> * </span>for his mercy endureth for ever.

<sub>10</sub> Who smote Egypt with their first-born<span> * </span>for his mercy endureth for ever;

<sub>11</sub> And brought out Israel from among them<span> * </span>for his mercy endureth for ever;

<sub>12</sub> With a mighty hand, and stretched out arm<span> * </span>for his mercy endureth for ever.

<sub>13</sub> Who divided the Red sea in two parts<span> * </span>for his mercy endureth for ever;

<sub>14</sub> And made Israel to go through the midst of it<span> * </span>for his mercy endureth for ever.

<sub>15</sub> But as for Pharaoh and his host, he overthrew them in the Red sea<span> * </span>for his mercy endureth for ever.

<sub>16</sub> Who led his people through the wilderness<span> * </span>for his mercy endureth for ever.

<sub>17</sub> Who smote great kings<span> * </span>for his mercy endureth for ever;

<sub>18</sub> Yea, and slew mighty kings<span> * </span>for his mercy endureth for ever;

<sub>19</sub> Sehon king of the Amorites<span> * </span>for his mercy endureth for ever;

<sub>20</sub> And Og the king of Basan<span> * </span>for his mercy endureth for ever;

<sub>21</sub> And gave away their land for an heritage<span> * </span>for his mercy endureth for ever;

<sub>22</sub> Even for an heritage unto Israel his servant<span> * </span>for his mercy endureth for ever.

<sub>23</sub> Who remembered us when we were in trouble<span> * </span>for his mercy endureth for ever;

<sub>24</sub> And hath delivered us from our enemies<span> * </span>for his mercy endureth for ever.

<sub>25</sub> Who giveth food to all flesh<span> * </span>for his mercy endureth for ever.

<sub>26</sub> O give thanks unto the God of heaven<span> * </span>for his mercy endureth for ever.

<sub>27</sub> O give thanks unto the Lord of lords<span> * </span>for his mercy endureth for ever.

#### *Psalm 137. Super flumina*
BY THE waters of Babylon we sat down and wept<span> * </span>when we remembered thee, O Sion.

<sub>2</sub> As for our harps, we hanged them up<span> * </span>upon the trees that are therein.

<sub>3</sub> For they that led us away captive required of us then a song, and melody in our heaviness<span> * </span>Sing us one of the songs of Sion.

<sub>4</sub> How shall we sing the Lord's song<span> * </span>in a strange land?

<sub>5</sub> If I forget thee, O Jerusalem<span> * </span>let my right hand forget her cunning.

<sub>6</sub> If I do not remember thee, let my tongue cleave to the roof of my mouth<span> * </span>yea, if I prefer not Jerusalem in my mirth.

<sub>7</sub> Remember the children of Edom, O Lord, in the day of Jerusalem<span> * </span>how they said, Down with it, down with it, even to the ground.

<sub>8</sub> O daughter of Babylon , wasted with misery<span> * </span>yea, happy shall he be that rewardeth thee, as thou hast served us.

<sub>9</sub> Blessed shall he be that taketh thy children<span> * </span>and throweth them against the stones.

#### *Psalm 138. Confitebor tibi*
IWILL give thanks unto thee, O Lord, with my whole heart<span> * </span>even before the gods will I sing praise unto thee.

<sub>2</sub> I will worship toward thy holy temple, and praise thy Name, because of thy loving-kindness and truth<span> * </span>for thou hast magnified thy Name and thy word above all things.

<sub>3</sub> When I called upon thee, thou heardest me<span> * </span>and enduedst my soul with much strength.

<sub>4</sub> All the kings of the earth shall praise thee, O Lord<span> * </span>for they have heard the words of thy mouth.

<sub>5</sub> Yea, they shall sing in the ways of the Lord<span> * </span>that great is the glory of the Lord.

<sub>6</sub> For though the Lord be high, yet hath he respect unto the lowly<span> * </span>as for the proud, he beholdeth them afar off.

<sub>7</sub> Though I walk in the midst of trouble, yet shalt thou refresh me<span> * </span>thou shalt stretch forth thy hand upon the furiousness of mine enemies, and thy right hand shall save me.

<sub>8</sub> The lord shall make good his loving-kindness toward me<span> * </span>yea, thy mercy, O Lord, endureth for ever; despise not then the works of thine own hands.

