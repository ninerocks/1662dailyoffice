#### *Psalm 114. In exitu Israel*
WHEN Israel came out of Egypt<span> * </span>and the house of Jacob from among the strange people,

<sub>2</sub> Judah was his sanctuary<span> * </span>and Israel his dominion.

<sub>3</sub> The sea saw that, and fled<span> * </span>Jordan was driven back.

<sub>4</sub> The mountains skipped like rams<span> * </span>and the little hills like young sheep.

<sub>5</sub> What aileth thee, O thou sea, that thou fleddest<span> * </span>and thou Jordan, that thou wast driven back?

<sub>6</sub> Ye mountains, that ye skipped like rams<span> * </span>and ye little hills, like young sheep?

<sub>7</sub> Tremble, thou earth, at the presence of the Lord<span> * </span>at the presence of the God of Jacob;

<sub>8</sub> Who turned the hard rock into a standing water<span> * </span>and the flint-stone into a springing well.

#### *Psalm 115. Non nobis, Domine*
NOT unto us, O Lord, not unto us, but unto thy Name give the praise<span> * </span>for thy loving mercy and for thy truth's sake.

<sub>2</sub> Wherefore shall the heathen say<span> * </span>Where is now their God?

<sub>3</sub> As for our God, he is in heaven<span> * </span>he hath done whatsoever pleased him.

<sub>4</sub> Their idols are silver and gold<span> * </span>even the work of men's hands.

<sub>5</sub> They have mouths, and speak not<span> * </span>eyes have they, and see not.

<sub>6</sub> They have ears, and hear not<span> * </span>noses have they, and smell not.

<sub>7</sub> They have hands, and handle not; feet have they, and walk not<span> * </span>neither speak they through their throat.

#### *8. They that make them are like unto them ; and so are all such as put their trust in them.*
<sub>9</sub> But thou, house of Israel, trust thou in the Lord<span> * </span>he is their succour and defence.

<sub>10</sub> Ye house of Aaron, put your trust in the Lord<span> * </span>he is their helper and defender.

<sub>11</sub> Ye that fear the Lord, put your trust in the Lord<span> * </span>he is their helper and defender.

<sub>12</sub> The Lord hath been mindful of us, and he shall bless us<span> * </span>even he shall bless the house of Israel, he shall bless the house of Aaron.

<sub>13</sub> He shall bless them that fear the Lord<span> * </span>both small and great.

<sub>14</sub> The Lord shall increase you more and more<span> * </span>you and your children.

<sub>15</sub> Ye are the blessed of the Lord<span> * </span>who made heaven and earth.

<sub>16</sub> All the whole heavens are the Lord's<span> * </span>the earth hath he given to the children of men.

<sub>17</sub> The dead praise not thee, O Lord<span> * </span>neither all they that go down into silence.

<sub>18</sub> But we will praise the Lord<span> * </span>from this time forth for evermore. Praise the Lord.

