#### *Psalm 12. Salvum me fac*
HELP me, Lord, for there is not one godly man left<span> * </span>for the faithful are minished from among the children of men,

<sub>2</sub> They talk of vanity every one with his neighbour<span> * </span>they do but flatter with their lips, and dissemble in their double heart.

<sub>3</sub> The Lord shall root out all deceitful lips<span> * </span>and the tongue that speaketh proud things;

<sub>4</sub> Which have said, With our tongue will we prevail<span> * </span>we are they that ought to speak , who is lord over us?

<sub>5</sub> Now for the comfortless trouble's sake of the needy<span> * </span>and because of the deep sighing of the poor,

<sub>6</sub> I will up, saith the Lord<span> * </span>and will help every one from him that swelleth against him, and will set him at rest.

<sub>7</sub> The words of the Lord are pure words<span> * </span>even as the silver, which from the earth is tried, and purified seven times in the fire.

<sub>8</sub> Thou shalt keep them, O Lord<span> * </span>thou shalt preserve him from this generation for ever.

<sub>9</sub> The ungodly walk on every side<span> * </span>when they are exalted, the children of men are put to rebuke.

#### *Psalm 13. Usque quo, Domine?*
HOW long wilt thou forget me, O Lord, for ever<span> * </span>how long wilt thou hide thy face from me?

<sub>2</sub> How long shall I seek counsel in my soul, and be so vexed in my heart<span> * </span>how long shall mine enemies triumph over me?

<sub>3</sub> Consider, and hear me, O Lord my God<span> * </span>lighten mine eyes, that I sleep not in death.

<sub>4</sub> Lest mine enemy say, I have prevailed against him<span> * </span>for if I be cast down, they that trouble me will rejoice at it.

<sub>5</sub> But my trust is in thy mercy<span> * </span>and my heart is joyful in thy salvation.

<sub>6</sub> I will sing of the Lord, because he hath dealt so lovingly with me<span> * </span>yea, I will praise the Name of the Lord most Highest.

#### *Psalm 14. Dixit insipiens*
THE fool hath said in his heart<span> * </span>There is no God.

<sub>2</sub> They are corrupt, and become abominable in their doings<span> * </span>there is none that doeth good, no not one.

<sub>3</sub> The Lord looked down from heaven upon the children of men<span> * </span>to see if there were any that would understand, and seek after God.

<sub>4</sub> But they are all gone out of the way, they are altogether become abominable<span> * </span>there is none that doeth good, no not one.

<sub>5</sub> Their throat is an open sepulchre, with their tongues have they deceived<span> * </span>the poison of asps is under their lips.

<sub>6</sub> Their mouth is full of cursing and bitterness<span> * </span>their feet are swift to shed blood.

#### *7. Destruction and unhappiness is in their ways, and the way of peace have they not known ; there is no fear of God before their eyes.*
<sub>8</sub> Have they no knowledge, that they are all such workers of mischief<span> * </span>eating up my people as it were bread, and call not upon the Lord?

<sub>9</sub> There were they brought in great fear, even where no fear was<span> * </span>for God is in the generation of the righteous.

<sub>10</sub> As for you, ye have made a mock at the counsel of the poor<span> * </span>because he putteth his trust in the Lord.

<sub>11</sub> Who shall give salvation unto Israel out of Sion? When the Lord turneth the captivity of his people<span> * </span>then shall Jacob rejoice, and Israel shall be glad.

