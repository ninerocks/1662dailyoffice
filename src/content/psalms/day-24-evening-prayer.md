#### *Psalm 119.*
#### *Beati immaculati*
BLESSED are those that are undefiled in the way<span> * </span>and walk in the law of the Lord.

<sub>2</sub> Blessed are they that keep his testimonies<span> * </span>and seek him with their whole heart.

<sub>3</sub> For they who do no wickedness<span> * </span>walk in his ways.

<sub>4</sub> Thou hast charged<span> * </span>that we shall diligently keep thy commandments.

<sub>5</sub> O that my ways were made so direct<span> * </span>that I might keep thy statutes!

<sub>6</sub> So shall I not be confounded<span> * </span>while I have respect unto all thy commandments.

<sub>7</sub> I will thank thee with an unfeigned heart<span> * </span>when I shall have learned the judgements of thy righteousness.

<sub>8</sub> I will keep thy ceremonies<span> * </span>O forsake me not utterly.

#### *In quo corriget?*
WHEREWITHAL shall a young man cleanse his way<span> * </span>even by ruling himself after thy word.

<sub>10</sub> With my whole heart have I sought thee<span> * </span>O let me not go wrong out of thy commandments.

<sub>11</sub> Thy words have I hid within my heart<span> * </span>that I should not sin against thee.

<sub>12</sub> Blessed art thou, O Lord<span> * </span>O teach me thy statutes.

<sub>13</sub> With my lips have I been telling<span> * </span>of all the judgements of thy mouth.

<sub>14</sub> I have had as great delight in the way of thy testimonies<span> * </span>as in all manner of riches.

<sub>15</sub> I will talk of thy commandments<span> * </span>and have respect unto thy ways.

<sub>16</sub> My delight shall be in thy statutes<span> * </span>and I will not forget thy word.

#### *Retribue servo tuo*
ODO well unto thy servant<span> * </span>that I may live, and keep thy word.

<sub>18</sub> Open thou mine eyes<span> * </span>that I may see the wondrous things of thy law.

<sub>19</sub> I am a stranger upon earth<span> * </span>O hide not thy commandments from me.

<sub>20</sub> My soul breaketh out for the very fervent desire<span> * </span>that it hath alway unto thy judgements.

<sub>21</sub> Thou hast rebuked the proud<span> * </span>and cursed are they that do err from thy commandments.

<sub>22</sub> O turn from me shame and rebuke<span> * </span>for I have kept thy testimonies.

<sub>23</sub> Princes also did sit and speak against me<span> * </span>but thy servant is occupied in thy statutes.

<sub>24</sub> For thy testimonies are my delight<span> * </span>and my counsellors.

#### *Adhaesit pavimento*
MY SOUL cleaveth to the dust<span> * </span>O quicken thou me, according to thy word.

<sub>26</sub> I have acknowledged my ways, and thou heardest me<span> * </span>O teach me thy statutes.

<sub>27</sub> Make me to understand the way of thy commandments<span> * </span>and so shall I talk of thy wondrous works.

<sub>28</sub> My soul melteth away for very heaviness<span> * </span>comfort thou me according unto thy word.

<sub>29</sub> Take from me the way of lying<span> * </span>and cause thou me to make much of thy law.

<sub>30</sub> I have chosen the way of truth<span> * </span>and thy judgements have I laid before me.

<sub>31</sub> I have stuck unto thy testimonies<span> * </span>O Lord, confound me not.

<sub>32</sub> I will run the way of thy commandments<span> * </span>when thou hast set my heart at liberty.

