#### *Psalm 105. Confitemini Domino*
OGIVE thanks unto the Lord, and call upon his Name<span> * </span>tell the people what things he hath done.

<sub>2</sub> O let your songs be of him, and praise him<span> * </span>and let your talking be of all his wondrous works.

<sub>3</sub> Rejoice in his holy Name<span> * </span>let the heart of them rejoice that seek the Lord.

<sub>4</sub> Seek the Lord and his strength<span> * </span>seek his face evermore.

<sub>5</sub> Remember the marvellous works that he hath done<span> * </span>his wonders, and the judgements of his mouth.

<sub>6</sub> O ye seed of Abraham his servant<span> * </span>ye children of Jacob his chosen.

<sub>7</sub> He is the Lord our God<span> * </span>his judgements are in all the world.

<sub>8</sub> He hath been alway mindful of his covenant and promise<span> * </span>that he made to a thousand generations;

<sub>9</sub> Even the covenant that he made with Abraham<span> * </span>and the oath that he sware unto Isaac;

<sub>10</sub> And appointed the same unto Jacob for a law<span> * </span>and to Israel for an everlasting testament;

<sub>11</sub> Saying, Unto thee will I give the land of Canaan<span> * </span>the lot of your inheritance;

<sub>12</sub> When there were yet but a few of them<span> * </span>and they strangers in the land;

<sub>13</sub> What time as they went from one nation to another<span> * </span>from one kingdom to another people;

<sub>14</sub> He suffered no man to do them wrong<span> * </span>but reproved even kings for their sakes;

<sub>15</sub> Touch not mine Anointed<span> * </span>and do my prophets no harm.

<sub>16</sub> Moreover, he called for a dearth upon the land<span> * </span>and destroyed all the provision of bread.

<sub>17</sub> But he had sent a man before them<span> * </span>even Joseph, who was sold to be a bond-servant;

<sub>18</sub> Whose feet they hurt in the stocks<span> * </span>the iron entered into his soul;

<sub>19</sub> Until the time came that his cause was known<span> * </span>the word of the Lord tried him.

<sub>20</sub> The king sent, and delivered him<span> * </span>the prince of the people let him go free.

<sub>21</sub> He made him lord also of his house<span> * </span>and ruler of all his substance;

<sub>22</sub> That he might inform his princes after his will<span> * </span>and teach his senators wisdom.

<sub>23</sub> Israel also came into Egypt<span> * </span>and Jacob was a stranger in the land of Ham.

<sub>24</sub> And he increased his people exceedingly<span> * </span>and made them stronger than their enemies;

<sub>25</sub> Whose heart turned, so that they hated his people<span> * </span>and dealt untruly with his servants.

<sub>26</sub> Then sent he Moses his servant<span> * </span>and Aaron whom he had chosen.

<sub>27</sub> And these shewed his tokens among them<span> * </span>and wonders in the land of Ham.

<sub>28</sub> He sent darkness, and it was dark<span> * </span>and they were not obedient unto his word.

<sub>29</sub> He turned their waters into blood<span> * </span>and slew their fish.

<sub>30</sub> Their land brought forth frogs<span> * </span>yea, even in their kings' chambers.

<sub>31</sub> He spake the word, and there came all manner of flies<span> * </span>and lice in all their quarters.

<sub>32</sub> He gave them hail-stones for rain<span> * </span>and flames of fire in their land.

<sub>33</sub> He smote their vines also and fig-trees<span> * </span>and destroyed the trees that were in their coasts.

<sub>34</sub> He spake the word, and the grasshoppers came, and caterpillars innumerable<span> * </span>and did eat up all the grass in their land, and devoured the fruit of their ground.

<sub>35</sub> He smote all the first-born in their land<span> * </span>even the chief of all their strength.

<sub>36</sub> He brought them forth also with silver and gold<span> * </span>there was not one feeble person among their tribes.

<sub>37</sub> Egypt was glad at their departing<span> * </span>for they were afraid of them.

<sub>38</sub> He spread out a cloud to be a covering<span> * </span>and fire to give light in the night-season.

<sub>39</sub> At their desire he brought quails<span> * </span>and he filled them with the bread of heaven.

<sub>40</sub> He opened the rock of stone, and the waters flowed out<span> * </span>so that rivers ran in the dry places.

<sub>41</sub> For why? he remembered his holy promise<span> * </span>and Abraham his servant.

#### *42. And he brought forth his people with joy: and his chosen with gladness;*
<sub>43</sub> And gave them the lands of the heathen<span> * </span>and they took the labours of the people in possession;

<sub>44</sub> That they might keep his statutes<span> * </span>and observe his laws.

