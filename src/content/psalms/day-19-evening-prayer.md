#### *Psalm 98. Cantate Domino*
OSING unto the Lord a new song<span> * </span>for he hath done marvellous things.

<sub>2</sub> With his own right hand, and with his holy arm<span> * </span>hath he gotten himself the victory.

<sub>3</sub> The Lord declared his salvation<span> * </span>his righteousness hath he openly shewed in the sight of the heathen.

<sub>4</sub> He hath remembered his mercy and truth toward the house of Israel<span> * </span>and all the ends of the world have seen the salvation of our God.

<sub>5</sub> Shew yourselves joyful unto the Lord, all ye lands<span> * </span>sing, rejoice, and give thanks.

<sub>6</sub> Praise the Lord upon the harp<span> * </span>sing to the harp with a psalm of thanksgiving.

<sub>7</sub> With trumpets also and shawms<span> * </span>O shew yourselves joyful before the Lord the King.

<sub>8</sub> Let the sea make a noise, and all that therein is<span> * </span>the round world, and they that dwell therein.

<sub>9</sub> Let the floods clap their hands, and let the hills be joyful together before the Lord<span> * </span>for he is come to judge the earth.

<sub>10</sub> With righteousness shall he judge the world<span> * </span>and the people with equity.

#### *Psalm 99. Dominus regnavit*
THE Lord is King, be the people never so unpatient<span> * </span>he sitteth between the cherubims, be the earth never so unquiet.

<sub>2</sub> The Lord is great in Sion<span> * </span>and high above all people.

<sub>3</sub> They shall give thanks unto thy Name<span> * </span>which is great, wonderful, and holy.

#### *4. The King's power loveth judgement; thou hast prepared equity: thou hast executed judgement and righteousness in Jacob.*
<sub>5</sub> O magnify the Lord our God<span> * </span>and fall down before his footstool, for he is holy.

<sub>6</sub> Moses and Aaron among his priests, and Samuel among such as call upon his Name<span> * </span>these called upon the Lord, and he heard them.

<sub>7</sub> He spake unto them out of the cloudy pillar<span> * </span>for they kept his testimonies, and the law that he gave them.

<sub>8</sub> Thou heardest them, O Lord our God<span> * </span>thou forgavest them, O God, and punishedst their own inventions.

<sub>9</sub> O magnify the Lord our God, and worship him upon his holy hill<span> * </span>for the Lord our God is holy.

#### *Psalm 100. Jubilate Deo*
OBE joyful in the Lord, all ye lands<span> * </span>serve the Lord with gladness, and come before his presence with a song.

<sub>2</sub> Be ye sure that the Lord he is God<span> * </span>it is he that hath made us, and not we ourselves; we are his people, and the sheep of his pasture.

<sub>3</sub> O go your way into his gates with thanksgiving, and into his courts with praise<span> * </span>be thankful unto him, and speak good of his Name.

<sub>4</sub> For the Lord is gracious, his mercy is everlasting<span> * </span>and his truth endureth from generation to generation.

#### *Psalm 101. Misericordiam et judicium*
MY SONG shall be of mercy and judgement<span> * </span>unto thee, O Lord, will I sing.

<sub>2</sub> O let me have understanding<span> * </span>in the way of godliness.

<sub>3</sub> When wilt thou come unto me<span> * </span>I will walk in my house with a perfect heart.

<sub>4</sub> I will take no wicked thing in hand; I hate the sins of unfaithfulness<span> * </span>there shall no such cleave unto me.

<sub>5</sub> A froward heart shall depart from me<span> * </span>I will not know a wicked person.

<sub>6</sub> Whoso privily slandereth his neighbour<span> * </span>him will I destroy.

<sub>7</sub> Whoso hath also a proud look and high stomach<span> * </span>I will not suffer him.

<sub>8</sub> Mine eyes look upon such as are faithful in the land<span> * </span>that they may dwell with me.

<sub>9</sub> Whoso leadeth a godly life<span> * </span>he shall be my servant.

<sub>10</sub> There shall no deceitful person dwell in my house<span> * </span>he that telleth lies shall not tarry in my sight.

<sub>11</sub> I shall soon destroy all the ungodly that are in the land<span> * </span>that I may root out all wicked doers from the city of the Lord.

