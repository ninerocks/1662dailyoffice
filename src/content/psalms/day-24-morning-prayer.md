#### *Psalm 116. Dilexi, quoniam*
IAM well pleased<span> * </span>that the Lord hath heard the voice of my prayer;

<sub>2</sub> That he hath inclined his ear unto me<span> * </span>therefore will I call upon him as long as I live.

<sub>3</sub> The snares of death compassed me round about<span> * </span>and the pains of hell gat hold upon me.

<sub>4</sub> I shall find trouble and heaviness, and I will call upon the Name of the Lord<span> * </span>O Lord, I beseech thee, deliver my soul.

<sub>5</sub> Gracious is the Lord, and righteous<span> * </span>yea, our God is merciful.

<sub>6</sub> The Lord preserveth the simple<span> * </span>I was in misery, and he helped me.

<sub>7</sub> Turn again then unto thy rest, O my soul<span> * </span>for the Lord hath rewarded thee.

<sub>8</sub> And why? thou hast delivered my soul from death<span> * </span>mine eyes from tears, and my feet from falling.

<sub>9</sub> I will walk before the Lord<span> * </span>in the land of the living.

<sub>10</sub> I believed, and therefore will I speak; but I was sore troubled<span> * </span>I said in my haste, All men are liars.

<sub>11</sub> What reward shall I give unto the Lord<span> * </span>for all the benefits that he hath done unto me?

<sub>12</sub> I will receive the cup of salvation<span> * </span>and call upon the Name of the Lord.

<sub>13</sub> I will pay my vows now in the presence of all his people<span> * </span>right dear in the sight of the Lord is the death of his saints.

<sub>14</sub> Behold, O Lord, how that I am thy servant<span> * </span>I am thy servant, and the son of thine handmaid; thou hast broken my bonds in sunder.

<sub>15</sub> I will offer to thee the sacrifice of thanksgiving<span> * </span>and will call upon the Name of the Lord.

<sub>16</sub> I will pay my vows unto the Lord, in the sight of all his people<span> * </span>in the courts of the Lord's house, even in the midst of thee, O Jerusalem. Praise the Lord.

#### *Psalm 117. Laudate Dominum*
OPRAISE the Lord, all ye heathen<span> * </span>praise him, all ye nations.

<sub>2</sub> For his merciful kindness is ever more and more towards us<span> * </span>and the truth of the Lord endureth for ever. Praise the Lord.

#### *Psalm 118. Confitemini Domino*
OGIVE thanks unto the Lord, for he is gracious<span> * </span>because his mercy endureth for ever.

<sub>2</sub> Let Israel now confess that he is gracious<span> * </span>and that his mercy endureth for ever.

<sub>3</sub> Let the house of Aaron now confess<span> * </span>that his mercy endureth for ever.

<sub>4</sub> Yea, let them now that fear the Lord confess<span> * </span>that his mercy endureth for ever.

<sub>5</sub> I called upon the Lord in trouble<span> * </span>and the Lord heard me at large.

<sub>6</sub> The Lord is on my side<span> * </span>I will not fear what man doeth unto me.

<sub>7</sub> The Lord taketh my part with them that help me<span> * </span>therefore shall I see my desire upon mine enemies.

<sub>8</sub> It is better to trust in the Lord<span> * </span>than to put any confidence in man.

<sub>9</sub> It is better to trust in the Lord<span> * </span>than to put any confidence in princes.

<sub>10</sub> All nations compassed me round about<span> * </span>but in the Name of the Lord will I destroy them.

<sub>11</sub> They kept me in on every side, they kept me in, I say, on every side<span> * </span>but in the Name of the Lord will I destroy them.

<sub>12</sub> They came about me like bees, and are extinct even as the fire among the thorns<span> * </span>for in the Name of the Lord I will destroy them.

<sub>13</sub> Thou hast thrust sore at me, that I might fall<span> * </span>but the Lord was my help.

<sub>14</sub> The Lord is my strength, and my song<span> * </span>and is become my salvation.

<sub>15</sub> The voice of joy and health is in the dwellings of the righteous<span> * </span>the right hand of the Lord bringeth mighty things to pass.

<sub>16</sub> The right hand of the Lord hath the pre-eminence<span> * </span>the right hand of the Lord bringeth mighty things to pass.

<sub>17</sub> I shall not die, but live<span> * </span>and declare the works of the Lord.

<sub>18</sub> The Lord hath chastened and corrected me<span> * </span>but he hath not given me over unto death.

<sub>19</sub> Open me the gates of righteousness<span> * </span>that I may go into them, and give thanks unto the Lord.

<sub>20</sub> This is the gate of the Lord<span> * </span>the righteous shall enter into it.

<sub>21</sub> I will thank thee, for thou hast heard me<span> * </span>and art become my salvation.

<sub>22</sub> The same stone which the builders refused<span> * </span>is become the head-stone in the corner.

<sub>23</sub> This is the Lord's doing<span> * </span>and it is marvellous in our eyes.

<sub>24</sub> This is the day which the Lord hath made<span> * </span>we will rejoice and be glad in it.

<sub>25</sub> Help me now, O Lord<span> * </span>O Lord, send us now prosperity.

<sub>26</sub> Blessed be he that cometh in the Name of the Lord<span> * </span>we have wished you good luck , ye that are of the house of the Lord.

<sub>27</sub> God is the Lord who hath shewed us light<span> * </span>bind the sacrifice with cords, yea, even unto the horns of the altar.

<sub>28</sub> Thou art my God, and I will thank thee<span> * </span>thou art my God, and I will praise thee.

<sub>29</sub> O give thanks unto the Lord, for he is gracious<span> * </span>and his mercy endureth for ever.

