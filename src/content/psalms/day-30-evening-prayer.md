#### *Psalm 147. Laudate Dominum*
OPRAISE the Lord, for it is a good thing to sing praises unto our God<span> * </span>yea, a joyful and pleasant thing it is to be thankful.

<sub>2</sub> The Lord doth build up Jerusalem<span> * </span>and gather together the out-casts of Israel.

<sub>3</sub> He healeth those that are broken in heart<span> * </span>and giveth medicine to heal their sickness.

<sub>4</sub> He telleth the number of the stars<span> * </span>and calleth them all by their names.

<sub>5</sub> Great is our Lord, and great is his power<span> * </span>yea, and his wisdom is infinite.

<sub>6</sub> The Lord setteth up the meek<span> * </span>and bringeth the ungodly down to the ground.

<sub>7</sub> O sing unto the Lord with thanksgiving<span> * </span>sing praises upon the harp unto our God;

<sub>8</sub> Who covereth the heaven with clouds, and prepareth rain for the earth<span> * </span>and maketh the grass to grow upon the mountains, and herb for the use of men;

<sub>9</sub> Who giveth fodder unto the cattle<span> * </span>and feedeth the young ravens that call upon him.

<sub>10</sub> He hath no pleasure in the strength of an horse<span> * </span>neither delighteth he in any man's legs.

<sub>11</sub> But the Lord's delight is in them that fear him<span> * </span>and put their trust in his mercy.

<sub>12</sub> Praise the Lord, O Jerusalem<span> * </span>praise thy God, O Sion.

<sub>13</sub> For he hath made fast the bars of thy gates<span> * </span>and hath blessed thy children within thee.

<sub>14</sub> He maketh peace in thy borders<span> * </span>and filleth thee with the flour of wheat.

<sub>15</sub> He sendeth forth his commandment upon earth<span> * </span>and his word runneth very swiftly.

<sub>16</sub> He giveth snow like wool<span> * </span>and scattereth the hoar-frost like ashes.

<sub>17</sub> He casteth forth his ice like morsels<span> * </span>who is able to abide his frost?

<sub>18</sub> He sendeth out his word, and melteth them<span> * </span>he bloweth with his wind, and the waters flow.

<sub>19</sub> He sheweth his word unto Jacob<span> * </span>his statutes and ordinances unto Israel.

<sub>20</sub> He hath not dealt so with any nation<span> * </span>neither have the heathen knowledge of his laws.

#### *Psalm 148. Laudate Dominum*
OPRAISE the Lord of heaven<span> * </span>praise him in the height.

<sub>2</sub> Praise him, all ye angels of his<span> * </span>praise him, all his host.

<sub>3</sub> Praise him, sun and moon<span> * </span>praise him, all ye stars and light.

<sub>4</sub> Praise him, all ye heavens<span> * </span>and ye waters that are above the heavens.

<sub>5</sub> Let them praise the Name of the Lord<span> * </span>for he spake the word, and they were made; he commanded, and they were created.

<sub>6</sub> He hath made them fast for ever and ever<span> * </span>he hath given them a law which shall not be broken.

<sub>7</sub> Praise the Lord upon earth<span> * </span>ye dragons, and all deeps;

<sub>8</sub> Fire and hail, snow and vapours<span> * </span>wind and storm, fulfilling his word;

<sub>9</sub> Mountains and all hills<span> * </span>fruitful trees and all cedars;

<sub>10</sub> Beasts and all cattle<span> * </span>worms and feathered fowls;

<sub>11</sub> Kings of the earth and all people<span> * </span>princes and all judges of the world;

<sub>12</sub> Young men and maidens, old men and children, praise the Name of the Lord<span> * </span>for his Name only is excellent, and his praise above heaven and earth.

<sub>13</sub> He shall exalt the horn of his people; all his saints shall praise him<span> * </span>even the children of Israel, even the people that serveth him.

#### *Psalm 149. Cantate Domino*
OSING unto the Lord a new song<span> * </span>let the congregation of saints praise him.

<sub>2</sub> Let Israel rejoice in him that made him<span> * </span>and let the children of Sion be joyful in their King.

<sub>3</sub> Let them praise his Name in the dance<span> * </span>let them sing praises unto him with tabret and harp.

<sub>4</sub> For the Lord hath pleasure in his people<span> * </span>and helpeth the meek-hearted.

<sub>5</sub> Let the saints be joyful with glory<span> * </span>let them rejoice in their beds.

<sub>6</sub> Let the praises of God be in their mouth<span> * </span>and a two-edged sword in their hands;

<sub>7</sub> To be avenged of the heathen<span> * </span>and to rebuke the people;

<sub>8</sub> To bind their kings in chains<span> * </span>and their nobles with links of iron.

<sub>9</sub> That they may be avenged of them, as it is written<span> * </span>Such honour have all his saints.

#### *Psalm 150. Laudate Dominum*
OPRAISE God in his holiness<span> * </span>praise him in the firmament of his power.

<sub>2</sub> Praise him in his noble acts<span> * </span>praise him according to his excellent greatness.

<sub>3</sub> Praise him in the sound of the trumpet<span> * </span>praise him upon the lute and harp.

<sub>4</sub> Praise him in the cymbals and dances<span> * </span>praise him upon the strings and pipe.

<sub>5</sub> Praise him upon the well-tuned cymbals<span> * </span>praise him upon the loud cymbals.

<sub>6</sub> Let every thing that hath breath<span> * </span>praise the Lord.

