#### *Psalm 73. Quam bonus Israel!*
TRULY God is loving unto Israel<span> * </span>even unto such as are of a clean heart.

<sub>2</sub> Nevertheless, my feet were almost gone<span> * </span>my treadings had well-nigh slipt.

<sub>3</sub> And why? I was grieved at the wicked<span> * </span>I do also see the ungodly in such prosperity.

<sub>4</sub> For they are in no peril of death<span> * </span>but are lusty and strong.

<sub>5</sub> They come in no misfortune like other folk<span> * </span>neither are they plagued like other men.

<sub>6</sub> And this is the cause that they are so holden with pride<span> * </span>and overwhelmed with cruelty.

<sub>7</sub> Their eyes swell with fatness<span> * </span>and they do even what they lust.

<sub>8</sub> They corrupt other, and speak of wicked blasphemy<span> * </span>their talking is against the most High.

<sub>9</sub> For they stretch forth their mouth unto the heaven<span> * </span>and their tongue goeth through the world.

<sub>10</sub> Therefore fall the people unto them<span> * </span>and thereout suck they no small advantage.

<sub>11</sub> Tush, say they, how should God perceive it<span> * </span>is there knowledge in the most High?

<sub>12</sub> Lo, these are the ungodly, these prosper in the world, and these have riches in possession<span> * </span>and I said, Then have I cleansed my heart in vain, and washed mine hands in innocency.

<sub>13</sub> All the day long have I been punished<span> * </span>and chastened every morning.

<sub>14</sub> Yea, and I had almost said even as they<span> * </span>but lo, then I should have condemned the generation of thy children.

<sub>15</sub> Then thought I to understand this<span> * </span>but it was too hard for me,

<sub>16</sub> Until I went into the sanctuary of God<span> * </span>then understood I the end of these men;

<sub>17</sub> Namely, how thou dost set them in slippery places<span> * </span>and castest them down, and destroyest them.

<sub>18</sub> O how suddenly do they consume<span> * </span>perish, and come to a fearful end!

<sub>19</sub> Yea, even like as a dream when one awaketh<span> * </span>so shalt thou make their image to vanish out of the city.

<sub>20</sub> Thus my heart was grieved<span> * </span>and it went even through my reins.

<sub>21</sub> So foolish was I, and ignorant<span> * </span>even as it were a beast before thee.

<sub>22</sub> Nevertheless, I am alway by thee<span> * </span>for thou hast holden me by my right hand.

<sub>23</sub> Thou shalt guide me with thy counsel<span> * </span>and after that receive me with glory.

<sub>24</sub> Whom have I in heaven but thee<span> * </span>and there is none upon earth that I desire in comparison of thee.

<sub>25</sub> My flesh and my heart faileth<span> * </span>but God is the strength of my heart, and my portion for ever.

<sub>26</sub> For lo, they that forsake thee shall perish<span> * </span>thou hast destroyed all them that commit fornication against thee.

<sub>27</sub> But it is good for me to hold me fast by God, to put my trust in the Lord God<span> * </span>and to speak of all thy works in the gates of the daughter of Sion.

#### *Psalm 74. Ut quid, Deus?*
OGOD, wherefore art thou absent from us so long<span> * </span>why is thy wrath so hot against the sheep of thy pasture?

<sub>2</sub> O think upon thy congregation<span> * </span>whom thou hast purchased and redeemed of old.

<sub>3</sub> Think upon the tribe of thine inheritance<span> * </span>and mount Sion, wherein thou hast dwelt.

<sub>4</sub> Lift up thy feet, that thou mayest utterly destroy every enemy<span> * </span>which hath done evil in thy sanctuary.

<sub>5</sub> Thine adversaries roar in the midst of thy congregations<span> * </span>and set up their banners for tokens.

<sub>6</sub> He that hewed timber afore out of the thick trees<span> * </span>was known to bring it to an excellent work.

<sub>7</sub> But now they break down all the carved work thereof<span> * </span>with axes and hammers.

<sub>8</sub> They have set fire upon thy holy places<span> * </span>and have defiled the dwelling-place of thy Name, even unto the ground.

<sub>9</sub> Yea, they said in their hearts, Let us make havock of them altogether<span> * </span>thus have they burnt up all the houses of God in the land.

<sub>10</sub> We see not our tokens, there is not one prophet more<span> * </span>no, not one is there among us, that understandeth any more.

<sub>11</sub> O God, how long shall the adversary do this dishonour<span> * </span>how long shall the enemy blaspheme thy Name, for ever?

<sub>12</sub> Why withdrawest thou thy hand<span> * </span>why pluckest thou not thy right hand out of thy bosom to consume the enemy?

<sub>13</sub> For God is my King of old<span> * </span>the help that is done upon earth he doeth it himself.

<sub>14</sub> Thou didst divide the sea through thy power<span> * </span>thou brakest the heads of the dragons in the waters.

<sub>15</sub> Thou smotest the heads of Leviathan in pieces<span> * </span>and gavest him to be meat for the people in the wilderness.

<sub>16</sub> Thou broughtest out fountains and waters out of the hard rocks<span> * </span>thou driedst up mighty waters.

<sub>17</sub> The day is thine, and the night is thine<span> * </span>thou hast prepared the light and the sun.

<sub>18</sub> Thou hast set all the borders of the earth<span> * </span>thou hast made summer and winter.

<sub>19</sub> Remember this, O Lord, how the enemy hath rebuked<span> * </span>and how the foolish people hath blasphemed thy Name.

<sub>20</sub> O deliver not the soul of thy turtle-dove unto the multitude of the enemies<span> * </span>and forget not the congregation of the poor for ever.

<sub>21</sub> Look upon the covenant<span> * </span>for all the earth is full of darkness and cruel habitations.

<sub>22</sub> O let not the simple go away ashamed<span> * </span>but let the poor and needy give praise unto thy Name.

<sub>23</sub> Arise, O God, maintain thine own cause<span> * </span>remember how the foolish man blasphemeth thee daily.

<sub>24</sub> Forget not the voice of thine enemies<span> * </span>the presumption of them that hate thee increaseth ever more and more.

