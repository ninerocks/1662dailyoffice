#### *Psalm 30. Exaltabo te, Domine*
IWILL magnify thee, O Lord, for thou hast set me up<span> * </span>and not made my foes to triumph over me.

<sub>2</sub> O Lord my God, I cried unto thee<span> * </span>and thou hast healed me.

<sub>3</sub> Thou, Lord, hast brought my soul out of hell<span> * </span>thou hast kept my life from them that go down to the pit.

<sub>4</sub> Sing praises unto the Lord, O ye saints of his<span> * </span>and give thanks unto him for a remembrance of his holiness.

<sub>5</sub> For his wrath endureth but the twinkling of an eye, and in his pleasure is life<span> * </span>heaviness may endure for a night, but joy cometh in the morning.

<sub>6</sub> And in my prosperity I said, I shall never be removed<span> * </span>thou, Lord, of thy goodness hast made my hill so strong.

<sub>7</sub> Thou didst turn thy face from me<span> * </span>and I was troubled.

<sub>8</sub> Then cried I unto thee, O Lord<span> * </span>and gat me to my Lord right humbly.

<sub>9</sub> What profit is there in my blood<span> * </span>when I go down to the pit?

<sub>10</sub> Shall the dust give thanks unto thee<span> * </span>or shall it declare thy truth?

<sub>11</sub> Hear, O Lord, and have mercy upon me<span> * </span>Lord, be thou my helper.

<sub>12</sub> Thou hast turned my heaviness into joy<span> * </span>thou hast put off my sackcloth, and girded me with gladness.

<sub>13</sub> Therefore shall every good man sing of thy praise without ceasing<span> * </span>O my God, I will give thanks unto thee for ever.

#### *Psalm 31. In te, Domine, speravi*
IN THEE, O Lord, have I put my trust<span> * </span>let me never be put to confusion, deliver me in thy righteousness.

<sub>2</sub> Bow down thine ear to me<span> * </span>make haste to deliver me.

<sub>3</sub> And be thou my strong rock, and house of defence<span> * </span>that thou mayest save me.

<sub>4</sub> For thou art my strong rock, and my castle<span> * </span>be thou also my guide, and lead me for thy Name's sake.

<sub>5</sub> Draw me out of the net that they have laid privily for me<span> * </span>for thou art my strength.

<sub>6</sub> Into thy hands I commend my spirit<span> * </span>for thou hast redeemed me, O Lord, thou God of truth.

<sub>7</sub> I have hated them that hold of superstitious vanities<span> * </span>and my trust hath been in the Lord.

<sub>8</sub> I will be glad and rejoice in thy mercy<span> * </span>for thou hast considered my trouble, and hast known my soul in adversities.

<sub>9</sub> Thou hast not shut me up into the hand of the enemy<span> * </span>but hast set my feet in a large room.

<sub>10</sub> Have mercy upon me, O Lord , for I am in trouble<span> * </span>and mine eye is consumed for very heaviness; yea, my soul and my body.

<sub>11</sub> For my life is waxen old with heaviness<span> * </span>and my years with mourning.

<sub>12</sub> My strength faileth me, because of mine iniquity<span> * </span>and my bones are consumed.

<sub>13</sub> I became a reproof among all mine enemies, but especially among my neighbours<span> * </span>and they of mine acquaintance were afraid of me; and they that did see me without conveyed themselves from me.

<sub>14</sub> I am clean forgotten, as a dead man out of mind<span> * </span>I am become like a broken vessel.

<sub>15</sub> For I have heard the blasphemy of the multitude<span> * </span>and fear is on every side, while they conspire together against me, and take their counsel to take away my life.

<sub>16</sub> But my hope hath been in thee, O Lord<span> * </span>I have said, Thou art my God.

<sub>17</sub> My time is in thy hand; deliver me from the hand of mine enemies<span> * </span>and from them that persecute me.

<sub>18</sub> Shew thy servant the light of thy countenance<span> * </span>and save me for thy mercy's sake.

<sub>19</sub> Let me not be confounded, O Lord, for I have called upon thee<span> * </span>let the ungodly be put to confusion, and be put to silence in the grave.

<sub>20</sub> Let the lying lips be put to silence<span> * </span>which cruelly, disdainfully, and despitefully, speak against the righteous.

<sub>21</sub> O how plentiful is thy goodness, which thou hast laid up for them that fear thee<span> * </span>and that thou hast prepared for them that put their trust in thee, even before the sons of men!

<sub>22</sub> Thou shalt hide them privily by thine own presence from the provoking of all men<span> * </span>thou shalt keep them secretly in thy tabernacle from the strife of tongues.

<sub>23</sub> Thanks be to the Lord<span> * </span>for he hath shewed me marvellous great kindness in a strong city.

<sub>24</sub> And when I made haste, I said<span> * </span>I am cast out of the sight of thine eyes.

<sub>25</sub> Nevertheless, thou heardest the voice of my prayer<span> * </span>when I cried unto thee.

<sub>26</sub> O love the Lord, all ye his saints<span> * </span>for the Lord preserveth them that are faithful, and plenteously rewardeth the proud doer.

<sub>27</sub> Be strong, and he shall establish your heart<span> * </span>all ye that put your trust in the Lord.

