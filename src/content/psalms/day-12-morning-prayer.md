#### *Psalm 62. Nonne Deo?*
MY SOUL truly waiteth still upon God<span> * </span>for of him cometh my salvation.

<sub>2</sub> He verily is my strength and my salvation<span> * </span>he is my defence, so that I shall not greatly fall.

<sub>3</sub> How long will ye imagine mischief against every man<span> * </span>ye shall be slain all the sort of you; yea, as a tottering wall shall ye be, and like a broken hedge.

<sub>4</sub> Their device is only how to put him out whom God will exalt<span> * </span>their delight is in lies; they give good words with their mouth, but curse with their heart.

<sub>5</sub> Nevertheless, my soul, wait thou still upon God<span> * </span>for my hope is in him.

<sub>6</sub> He truly is my strength and my salvation<span> * </span>he is my defence, so that I shall not fall.

<sub>7</sub> In God is my health, and my glory<span> * </span>the rock of my might, and in God is my trust.

<sub>8</sub> O put your trust in him alway, ye people<span> * </span>pour out your hearts before him, for God is our hope.

<sub>9</sub> As for the children of men, they are but vanity<span> * </span>the children of men are deceitful upon the weights, they are altogether lighter than vanity itself.

<sub>10</sub> O trust not in wrong and robbery, give not yourselves unto vanity<span> * </span>if riches increase, set not your heart upon them.

<sub>11</sub> God spake once, and twice I have also heard the same<span> * </span>that power belongeth unto God;

<sub>12</sub> And that thou, Lord, art merciful<span> * </span>for thou rewardest every man according to his work.

#### *Psalm 63. Deus, Deus meus*
OGOD, thou art my God<span> * </span>early will I seek thee.

<sub>2</sub> My soul thirsteth for thee, my flesh also longeth after thee<span> * </span>in a barren and dry land where no water is.

<sub>3</sub> Thus have I looked for thee in holiness<span> * </span>that I might behold thy power and glory.

<sub>4</sub> For thy loving-kindness is better than the life itself<span> * </span>my lips shall praise thee.

<sub>5</sub> As long as I live will I magnify thee on this manner<span> * </span>and lift up my hands in thy Name.

<sub>6</sub> My soul shall be satisfied, even as it were with marrow and fatness<span> * </span>when my mouth praiseth thee with joyful lips.

<sub>7</sub> Have I not remembered thee in my bed<span> * </span>and thought upon thee when I was waking?

<sub>8</sub> Because thou hast been my helper<span> * </span>therefore under the shadow of thy wings will I rejoice.

<sub>9</sub> My soul hangeth upon thee<span> * </span>thy right hand hath upholden me.

<sub>10</sub> These also that seek the hurt of my soul<span> * </span>they shall go under the earth.

<sub>11</sub> Let them fall upon the edge of the sword<span> * </span>that they may be a portion for foxes.

<sub>12</sub> But the King shall rejoice in God; all they also that swear by him shall be commended<span> * </span>for the mouth of them that speak lies shall be stopped.

#### *Psalm 64. Exaudi, Deus*
HEAR my voice, O God, in my prayer<span> * </span>preserve my life from fear of the enemy.

<sub>2</sub> Hide me from the gathering together of the froward<span> * </span>and from the insurrection of wicked doers;

<sub>3</sub> Who have whet their tongue like a sword<span> * </span>and shoot out their arrows, even bitter words;

<sub>4</sub> That they may privily shoot at him that is perfect<span> * </span>suddenly do they hit him, and fear not.

<sub>5</sub> They encourage themselves in mischief<span> * </span>and commune among themselves how they may lay snares, and say that no man shall see them.

<sub>6</sub> They imagine wickedness, and practise it<span> * </span>that they keep secret among themselves, every man in the deep of his heart.

<sub>7</sub> But God shall suddenly shoot at them with a swift arrow<span> * </span>that they shall be wounded.

<sub>8</sub> Yea, their own tongues shall make them fall<span> * </span>insomuch that whoso seeth them shall laugh them to scorn.

<sub>9</sub> And all men that see it shall say, This hath God done<span> * </span>for they shall perceive that it is his work.

<sub>10</sub> The righteous shall rejoice in the Lord, and put his trust in him<span> * </span>and all they that are true of heart shall be glad.

