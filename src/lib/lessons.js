
export default {
  jan: {
    1: {
      special: 'Circumcision of our Lord',
      morning: {
        1: 'GEN 17',
        2: 'ROM 9',
      },
      evening: {
        1: 'DEU 10:12-22',
        2: 'COL 2',
      },
    },
    2: {
      morning: {
        1: 'GEN 1',
        2: 'MAT 1',
      },
      evening: {
        1: 'GEN 2',
        2: 'ROM 1',
      },
    },
    3: {
      morning: {
        1: 'GEN 3',
        2: 'MAT 2',
      },
      evening: {
        1: 'GEN 4',
        2: 'ROM 2',
      },
    },
    4: {
      morning: {
        1: 'GEN 5',
        2: 'MAT 3',
      },
      evening: {
        1: 'GEN 6',
        2: 'ROM 3',
      },
    },
    5: {
      morning: {
        1: 'GEN 7',
        2: 'MAT 4',
      },
      evening: {
        1: 'GEN 8',
        2: 'ROM 4',
      },
    },
    6: {
      special: 'Epiphany of our Lord',
      morning: {
        1: 'ISA 60',
        2: 'LUK 3:1-22',
      },
      evening: {
        1: 'ISA 49',
        2: 'JOH 2:1-11',
      },
    },
    7: {
      morning: {
        1: 'GEN 9',
        2: 'MAT 5',
      },
      evening: {
        1: 'GEN 12',
        2: 'ROM 5',
      },
    },
    8: {
      morning: {
        1: 'GEN 13',
        2: 'MAT 6',
      },
      evening: {
        1: 'GEN 14',
        2: 'ROM 6',
      },
    },
    9: {
      morning: {
        1: 'GEN 15',
        2: 'MAT 7',
      },
      evening: {
        1: 'GEN 16',
        2: 'ROM 7',
      },
    },
    10: {
      morning: {
        1: 'GEN 17',
        2: 'MAT 8',
      },
      evening: {
        1: 'GEN 18',
        2: 'ROM 8',
      },
    },
    11: {
      morning: {
        1: 'GEN 19',
        2: 'MAT 9',
      },
      evening: {
        1: 'GEN 20',
        2: 'ROM 9',
      },
    },
    12: {
      morning: {
        1: 'GEN 21',
        2: 'MAT 10',
      },
      evening: {
        1: 'GEN 22',
        2: 'ROM 10',
      },
    },
    13: {
      morning: {
        1: 'GEN 23',
        2: 'MAT 11',
      },
      evening: {
        1: 'GEN 24',
        2: 'ROM 11',
      },
    },
    14: {
      morning: {
        1: 'GEN 25',
        2: 'MAT 12',
      },
      evening: {
        1: 'GEN 26',
        2: 'ROM 12',
      },
    },
    15: {
      morning: {
        1: 'GEN 27',
        2: 'MAT 13',
      },
      evening: {
        1: 'GEN 28',
        2: 'ROM 13',
      },
    },
    16: {
      morning: {
        1: 'GEN 29',
        2: 'MAT 14',
      },
      evening: {
        1: 'GEN 30',
        2: 'ROM 14',
      },
    },
    17: {
      morning: {
        1: 'GEN 31',
        2: 'MAT 15',
      },
      evening: {
        1: 'GEN 32',
        2: 'ROM 15',
      },
    },
    18: {
      morning: {
        1: 'GEN 33',
        2: 'MAT 16',
      },
      evening: {
        1: 'GEN 34',
        2: 'ROM 16',
      },
    },
    19: {
      morning: {
        1: 'GEN 35',
        2: 'MAT 17',
      },
      evening: {
        1: 'GEN 37',
        2: '1CO 1',
      },
    },
    20: {
      morning: {
        1: 'GEN 38',
        2: 'MAT 18',
      },
      evening: {
        1: 'GEN 39',
        2: '1CO 2',
      },
    },
    21: {
      morning: {
        1: 'GEN 40',
        2: 'MAT 19',
      },
      evening: {
        1: 'GEN 41',
        2: '1CO 3',
      },
    },
    22: {
      morning: {
        1: 'GEN 42',
        2: 'MAT 20',
      },
      evening: {
        1: 'GEN 43',
        2: '1CO 4',
      },
    },
    23: {
      morning: {
        1: 'GEN 44',
        2: 'MAT 21',
      },
      evening: {
        1: 'GEN 45',
        2: '1CO 5',
      },
    },
    24: {
      morning: {
        1: 'GEN 46',
        2: 'MAT 22',
      },
      evening: {
        1: 'GEN 47',
        2: '1CO 6',
      },
    },
    25: {
      special: 'Conversion of St. Paul',
      morning: {
        1: 'WIS 5',
        2: 'ACT 22:1-21',
      },
      evening: {
        1: 'WIS 6',
        2: 'ACT 26',
      },
    },
    26: {
      morning: {
        1: 'GEN 48',
        2: 'MAT 23',
      },
      evening: {
        1: 'GEN 49',
        2: '1CO 7',
      },
    },
    27: {
      morning: {
        1: 'GEN 50',
        2: 'MAT 24',
      },
      evening: {
        1: 'EXO 1',
        2: '1CO 8',
      },
    },
    28: {
      morning: {
        1: 'EXO 2',
        2: 'MAT 25',
      },
      evening: {
        1: 'EXO 3',
        2: '1CO 9',
      },
    },
    29: {
      morning: {
        1: 'EXO 4',
        2: 'MAT 26',
      },
      evening: {
        1: 'EXO 5',
        2: '1CO 10',
      },
    },
    30: {
      morning: {
        1: 'EXO 6:1-13',
        2: 'MAT 27',
      },
      evening: {
        1: 'EXO 7',
        2: '1CO 11',
      },
    },
    31: {
      morning: {
        1: 'EXO 8',
        2: 'MAT 28',
      },
      evening: {
        1: 'EXO 9',
        2: '1CO 12',
      },
    },
  },
  feb: {
    1: {
      morning: {
        1: 'EXO 10',
        2: 'MAR 1',
      },
      evening: {
        1: 'EXO 11',
        2: '1CO 13',
      },
    },
    2: {
      morning: {
        special: 'Purification of Mary the Blessed Virgin',
        1: 'WIS 9',
        2: 'MAR 2',
      },
      evening: {
        1: 'WIS 12',
        2: '1CO 14',
      },
    },
    3: {
      morning: {
        1: 'EXO 12',
        2: 'MAR 3',
      },
      evening: {
        1: 'EXO 13',
        2: '1CO 15',
      },
    },
    4: {
      morning: {
        1: 'EXO 14',
        2: 'MAR 4',
      },
      evening: {
        1: 'EXO 15',
        2: '1CO 16',
      },
    },
    5: {
      morning: {
        1: 'EXO 16',
        2: 'MAR 5',
      },
      evening: {
        1: 'EXO 17',
        2: '2CO 1',
      },
    },
    6: {
      morning: {
        1: 'EXO 18',
        2: 'MAR 6',
      },
      evening: {
        1: 'EXO 19',
        2: '2CO 2',
      },
    },
    7: {
      morning: {
        1: 'EXO 20',
        2: 'MAR 7',
      },
      evening: {
        1: 'EXO 21',
        2: '2CO 3',
      },
    },
    8: {
      morning: {
        1: 'EXO 22',
        2: 'MAR 8',
      },
      evening: {
        1: 'EXO 23',
        2: '2CO 4',
      },
    },
    9: {
      morning: {
        1: 'EXO 24',
        2: 'MAR 9',
      },
      evening: {
        1: 'EXO 32',
        2: '2CO 5',
      },
    },
    10: {
      morning: {
        1: 'EXO 33',
        2: 'MAR 10',
      },
      evening: {
        1: 'EXO 34',
        2: '2CO 6',
      },
    },
    11: {
      morning: {
        1: 'LEV 18',
        2: 'MAR 11',
      },
      evening: {
        1: 'LEV 19',
        2: '2CO 7',
      },
    },
    12: {
      morning: {
        1: 'LEV 20',
        2: 'MAR 12',
      },
      evening: {
        1: 'LEV 26',
        2: '2CO 8',
      },
    },
    13: {
      morning: {
        1: 'NUM 11',
        2: 'MAR 13',
      },
      evening: {
        1: 'NUM 12',
        2: '2CO 9',
      },
    },
    14: {
      morning: {
        1: 'NUM 13',
        2: 'MAR 14',
      },
      evening: {
        1: 'NUM 14',
        2: '2CO  10',
      },
    },
    15: {
      morning: {
        1: 'NUM 16',
        2: 'MAR 15',
      },
      evening: {
        1: 'NUM 17',
        2: '2CO 11',
      },
    },
    16: {
      morning: {
        1: 'NUM 20',
        2: 'MAR 16',
      },
      evening: {
        1: 'NUM 21',
        2: '2CO 12',
      },
    },
    17: {
      morning: {
        1: '22',
        2: 'LUK 1:1-38',
      },
      evening: {
        1: 'NUM 23',
        2: '2CO 13',
      },
    },
    18: {
      morning: {
        1: 'NUM 24',
        2: 'LUK 1:39-80',
      },
      evening: {
        1: 'NUM 25',
        2: 'GAL 1',
      },
    },
    19: {
      morning: {
        1: 'NUM 27',
        2: 'LUK 2',
      },
      evening: {
        1: 'NUM 30',
        2: 'GAL 2',
      },
    },
    20: {
      morning: {
        1: 'NUM 31',
        2: 'LUK 3',
      },
      evening: {
        1: 'NUM 32',
        2: 'GAL 3',
      },
    },
    21: {
      morning: {
        1: 'NUM 35',
        2: 'LUK 4',
      },
      evening: {
        1: 'NUM 36',
        2: 'GAL 4',
      },
    },
    22: {
      morning: {
        1: 'DEU 1',
        2: 'LUK 5',
      },
      evening: {
        1: 'DEU 2',
        2: 'GAL 5',
      },
    },
    23: {
      morning: {
        1: 'DEU 3',
        2: 'LUK 6',
      },
      evening: {
        1: 'DEU 4',
        2: 'GAL 6',
      },
    },
    24: {
      morning: {
        special: 'St. Matthias, A. & M.',
        1: 'WIS 19',
        2: 'MAR 7',
      },
      evening: {
        1: 'SIR 1',
        2: 'EPH 1',
      },
    },
    25: {
      morning: {
        1: 'DEU 5',
        2: 'LUK 8',
      },
      evening: {
        1: 'DEU 6',
        2: 'EPH 2',
      },
    },
    26: {
      morning: {
        1: 'DEU 7',
        2: 'LUK 9',
      },
      evening: {
        1: 'DEU 8',
        2: 'EPH 3',
      },
    },
    27: {
      morning: {
        1: 'DEU 9',
        2: 'LUK 10',
      },
      evening: {
        1: 'DEU 10',
        2: 'EPH 4',
      },
    },
    28: {
      morning: {
        1: 'DEU 11',
        2: 'LUK 11',
      },
      evening: {
        1: 'DEU 12',
        2: 'EPH 5',
      },
    },
    29: {
      morning: {
        1: 'DEU 13',
        2: 'MAT 7',
      },
      evening: {
        1: 'DEU 14',
        2: 'ROM 12',
      },
    },
  },
  mar: {
    1: {
      morning: {
        1: 'DEU 15',
        2: 'LUK 12',
      },
      evening: {
        1: 'DEU 16',
        2: 'EPH 6',
      },
    },
    2: {
      morning: {
        1: 'DEU 17',
        2: 'LUK 13',
      },
      evening: {
        1: 'DEU 18',
        2: 'PHI 1',
      },
    },
    3: {
      morning: {
        1: 'DEU 19',
        2: 'LUK 14',
      },
      evening: {
        1: 'DEU 20',
        2: 'PHI 2',
      },
    },
    4: {
      morning: {
        1: 'DEU 21',
        2: 'LUK 15',
      },
      evening: {
        1: 'DEU 22',
        2: 'PHI 3',
      },
    },
    5: {
      morning: {
        1: 'DEU 24',
        2: 'LUK 16',
      },
      evening: {
        1: 'DEU 25',
        2: 'PHI 4',
      },
    },
    6: {
      morning: {
        1: 'DEU 26',
        2: 'LUK 17',
      },
      evening: {
        1: 'DEU 27',
        2: 'COL 1',
      },
    },
    7: {
      morning: {
        1: 'DEU 28',
        2: 'LUK 18',
      },
      evening: {
        1: 'DEU 29',
        2: 'COL 2',
      },
    },
    8: {
      morning: {
        1: 'DEU 30',
        2: 'LUK 19',
      },
      evening: {
        1: 'DEU 31',
        2: 'COL 3',
      },
    },
    9: {
      morning: {
        1: 'DEU 32',
        2: 'LUK 20',
      },
      evening: {
        1: 'DEU 33',
        2: 'COL 4',
      },
    },
    10: {
      morning: {
        1: 'DEU 34',
        2: 'LUK 21',
      },
      evening: {
        1: 'JOS 1',
        2: '1TH 1',
      },
    },
    11: {
      morning: {
        1: 'JOS 2',
        2: 'LUK 22',
      },
      evening: {
        1: 'JOS 3',
        2: '1TH 2',
      },
    },
    12: {
      morning: {
        1: 'JOS 4',
        2: 'LUKE 23',
      },
      evening: {
        1: 'JOS 5',
        2: '1TH 3',
      },
    },
    13: {
      morning: {
        1: 'JOS 6',
        2: 'LUK 24',
      },
      evening: {
        1: 'JOS 7',
        2: '1TH 4',
      },
    },
    14: {
      morning: {
        1: 'JOS 8',
        2: 'JOH 1',
      },
      evening: {
        1: 'JOS 9',
        2: '1TH 5',
      },
    },
    15: {
      morning: {
        1: 'JOS 10',
        2: 'JOH 2',
      },
      evening: {
        1: 'JOS 23',
        2: '2TH 1',
      },
    },
    16: {
      morning: {
        1: 'JOS 24',
        2: 'JOH 3',
      },
      evening: {
        1: 'JUD 1',
        2: '2TH 2',
      },
    },
    17: {
      morning: {
        1: 'JUD 2',
        2: 'JOH 4',
      },
      evening: {
        1: 'JUD 3',
        2: '2TH 3',
      },
    },
    18: {
      morning: {
        1: 'JUD 4',
        2: 'JOH 5',
      },
      evening: {
        1: 'JUD 5',
        2: '1TI 1',
      },
    },
    19: {
      morning: {
        1: 'JUD 6',
        2: 'JOH 6',
      },
      evening: {
        1: 'JUD 7',
        2: '1TI 2,TI 3',
      },
    },
    20: {
      morning: {
        1: 'JUD 8',
        2: 'JOH 7',
      },
      evening: {
        1: 'JUD 9',
        2: '1TI 4',
      },
    },
    21: {
      morning: {
        1: 'JUD 10',
        2: 'JOH 8',
      },
      evening: {
        1: 'JUD 11',
        2: '1TI 5',
      },
    },
    22: {
      morning: {
        1: 'JUD 12',
        2: 'JOH 9',
      },
      evening: {
        1: 'JUD 13',
        2: '1TI 6',
      },
    },
    23: {
      morning: {
        1: 'JUD 14',
        2: 'JOH 10',
      },
      evening: {
        1: 'JUD 15',
        2: '2TI 1',
      },
    },
    24: {
      morning: {
        1: 'JUD 16',
        2: 'JOH 11',
      },
      evening: {
        1: 'JUD 17',
        2: '2TI 2',
      },
    },
    25: {
      special: 'Annunciation of Mary',
      morning: {
        1: 'SIR 2',
        2: 'JOH 12',
      },
      evening: {
        1: 'SIR 3',
        2: '2TI 3',
      },
    },
    26: {
      morning: {
        1: 'JUD 18',
        2: 'JOH 13',
      },
      evening: {
        1: 'JUD 19',
        2: '2TI 4',
      },
    },
    27: {
      morning: {
        1: 'JUD 20',
        2: 'JOH 14',
      },
      evening: {
        1: 'JUD 21',
        2: 'TIT 1',
      },
    },
    28: {
      morning: {
        1: 'RUT 1',
        2: 'JOH 15',
      },
      evening: {
        1: 'RUT 2',
        2: 'TIT 2,TIT 3',
      },
    },
    29: {
      morning: {
        1: 'RUT 3',
        2: 'JOH 16',
      },
      evening: {
        1: 'RUT 4',
        2: 'PHM',
      },
    },
    30: {
      morning: {
        1: '1SA 1',
        2: 'JOH 17',
      },
      evening: {
        1: '1SA 2',
        2: 'HEB 1',
      },
    },
    31: {
      morning: {
        1: '1SA 3',
        2: 'LUK 18',
      },
      evening: {
        1: '1SA 4',
        2: 'HEB 2',
      },
    },
  },
  apr: {
    1: {
      morning: {
        1: '1SA 5',
        2: 'JOH 19',
      },
      evening: {
        1: '1SA 6',
        2: 'HEB 3',
      },
    },
    2: {
      morning: {
        1: '1SA 7',
        2: 'JOH 20',
      },
      evening: {
        1: '1SA 8',
        2: 'HEB 4',
      },
    },
    3: {
      morning: {
        1: '1SA 9',
        2: 'JOH 21',
      },
      evening: {
        1: '1SA 10',
        2: 'HEB 5',
      },
    },
    4: {
      morning: {
        1: '1SA 11',
        2: 'ACT 1',
      },
      evening: {
        1: '1SA 12',
        2: 'HEB 6',
      },
    },
    5: {
      morning: {
        1: '1SA 13',
        2: 'ACT 2',
      },
      evening: {
        1: '1SA 14',
        2: 'HEB 7',
      },
    },
    6: {
      morning: {
        1: '1SA 15',
        2: 'ACT 3',
      },
      evening: {
        1: '1SA 16',
        2: 'HEB 8',
      },
    },
    7: {
      morning: {
        1: '1SA 17',
        2: 'ACT 4',
      },
      evening: {
        1: '1SA 18',
        2: 'HEB 9',
      },
    },
    8: {
      morning: {
        1: '1SA 19',
        2: 'ACT 5',
      },
      evening: {
        1: '1SA 20',
        2: 'HEB 10',
      },
    },
    9: {
      morning: {
        1: '1SA 21',
        2: 'ACT 6',
      },
      evening: {
        1: '1SA 22',
        2: 'HEB 11',
      },
    },
    10: {
      morning: {
        1: '1SA 23',
        2: 'ACT 7',
      },
      evening: {
        1: '1SA 24',
        2: 'HEB 12',
      },
    },
    11: {
      morning: {
        1: '1SA 25 ',
        2: 'ACT 8',
      },
      evening: {
        1: '1SA 26',
        2: 'HEB 13',
      },
    },
    12: {
      morning: {
        1: '1SA 27',
        2: 'ACT 9',
      },
      evening: {
        1: '1SA 28',
        2: 'JAM 1',
      },
    },
    13: {
      morning: {
        1: '1SA 29',
        2: 'ACT 10',
      },
      evening: {
        1: '1SA 30',
        2: 'JAM 2',
      },
    },
    14: {
      morning: {
        1: '1SA 31',
        2: 'ACT 11',
      },
      evening: {
        1: '2SA 1',
        2: 'JAM 3',
      },
    },
    15: {
      morning: {
        1: '2SA 2',
        2: 'ACT 12',
      },
      evening: {
        1: '2SA 3',
        2: 'JAM 4',
      },
    },
    16: {
      morning: {
        1: '2SA 4',
        2: 'ACT 13',
      },
      evening: {
        1: '2SA 5',
        2: 'JAM 5',
      },
    },
    17: {
      morning: {
        1: '2SA 6',
        2: 'ACT 14',
      },
      evening: {
        1: '2SA 7',
        2: '1PE 1',
      },
    },
    18: {
      morning: {
        1: '2SA 8',
        2: 'ACT 15',
      },
      evening: {
        1: '2SA 9',
        2: '1PE 2',
      },
    },
    19: {
      morning: {
        1: '2SA 10',
        2: 'ACT 16',
      },
      evening: {
        1: '2SA 11',
        2: '1PE 3',
      },
    },
    20: {
      morning: {
        1: '2SA 12',
        2: 'ACT 17',
      },
      evening: {
        1: '2SA 13',
        2: '1PE 4',
      },
    },
    21: {
      morning: {
        1: '2SA 14',
        2: 'ACT 18',
      },
      evening: {
        1: '2SA 15',
        2: '1PE 5',
      },
    },
    22: {
      morning: {
        1: '2SA 16',
        2: 'ACT 19',
      },
      evening: {
        1: '2SA 17',
        2: '2PE 1',
      },
    },
    23: {
      morning: {
        1: '2SA 18',
        2: 'ACT 20',
      },
      evening: {
        1: '2SA 19',
        2: '2PE 2',
      },
    },
    24: {
      morning: {
        1: '2SA 20',
        2: 'ACT 21',
      },
      evening: {
        1: '2SA 21',
        2: '2PE 3',
      },
    },
    25: {
      morning: {
        special: 'St. Mark, E. & M.',
        1: 'SIR 4',
        2: 'ACT 22',
      },
      evening: {
        1: 'SIR 5',
        2: '1JO 1',
      },
    },
    26: {
      morning: {
        1: '2SA 22',
        2: 'ACT 23',
      },
      evening: {
        1: '2SA 23',
        2: '1JO 2',
      },
    },
    27: {
      morning: {
        1: '2SA 24',
        2: 'ACT 24',
      },
      evening: {
        1: '1KI 1',
        2: '1JO 3',
      },
    },
    28: {
      morning: {
        1: '1KI 2',
        2: 'ACT 25',
      },
      evening: {
        1: '1KI 3',
        2: '1JO 4',
      },
    },
    29: {
      morning: {
        1: '1KI 4',
        2: 'ACT 26',
      },
      evening: {
        1: '1KI 5',
        2: '1JO 5',
      },
    },
    30: {
      morning: {
        1: '1KI 6',
        2: 'ACT 27',
      },
      evening: {
        1: '1KI 7',
        2: '2JO,3JO',
      },
    },
  },
  may: {
    1: {
      special: 'St. Phillip & St. James, A. & M.',
      morning: {
        1: 'SIR 7',
        2: 'JOH 1:43-51',
      },
      evening: {
        1: 'SIR 9',
        2: 'JUD',
      },
    },
    2: {
      morning: {
        1: '1KI 8',
        2: 'ACT 28',
      },
      evening: {
        1: '1KI 9',
        2: 'ROM 1',
      },
    },
    3: {
      morning: {
        1: '1KI 10',
        2: 'MAT 1',
      },
      evening: {
        1: '1KI 11',
        2: 'ROM 2',
      },
    },
    4: {
      morning: {
        1: '1KI 12',
        2: 'MAT 2',
      },
      evening: {
        1: '1KI 13',
        2: 'ROM 3',
      },
    },
    5: {
      morning: {
        1: '1KI 14',
        2: 'MAT 3',
      },
      evening: {
        1: '1KI 15',
        2: 'ROM 4',
      },
    },
    6: {
      morning: {
        1: '1KI 16',
        2: 'MAT 4',
      },
      evening: {
        1: '1KI 17',
        2: 'ROM 5',
      },
    },
    7: {
      morning: {
        1: '1KI 18',
        2: 'MAT 5',
      },
      evening: {
        1: '1KI 19',
        2: 'ROM 6',
      },
    },
    8: {
      morning: {
        1: '1KI 20',
        2: 'NAT 6',
      },
      evening: {
        1: '1KI 21',
        2: 'ROM 7',
      },
    },
    9: {
      morning: {
        1: '1KI 22',
        2: 'MAT 7',
      },
      evening: {
        1: '2KI 1',
        2: 'ROM 8',
      },
    },
    10: {
      morning: {
        1: '2KI 2',
        2: 'MAT 8',
      },
      evening: {
        1: '2KI 3',
        2: 'ROM 9',
      },
    },
    11: {
      morning: {
        1: '2KI 4',
        2: 'MAT 9',
      },
      evening: {
        1: '2KI 5',
        2: 'ROM 10',
      },
    },
    12: {
      morning: {
        1: '2KI 6',
        2: 'MAT 10',
      },
      evening: {
        1: '2KI 7',
        2: 'ROM 11',
      },
    },
    13: {
      morning: {
        1: '2KI 8',
        2: 'MAT 11',
      },
      evening: {
        1: '2KI 9',
        2: 'ROM 12',
      },
    },
    14: {
      morning: {
        1: '2KI 10',
        2: 'MAT 12',
      },
      evening: {
        1: '2KI 11',
        2: 'ROM 12',
      },
    },
    15: {
      morning: {
        1: '2KI 12',
        2: 'MAT 13',
      },
      evening: {
        1: '2KI 13',
        2: 'ROM 14',
      },
    },
    16: {
      morning: {
        1: '2KI 14',
        2: 'MAT 14',
      },
      evening: {
        1: '2KI 15',
        2: 'ROM 15',
      },
    },
    17: {
      morning: {
        1: '2KI 16',
        2: 'MAT 15',
      },
      evening: {
        1: '2KI 17',
        2: 'ROM 16',
      },
    },
    18: {
      morning: {
        1: '2KI 18',
        2: 'MAT 16',
      },
      evening: {
        1: '2KI 19',
        2: '1CO 1',
      },
    },
    19: {
      morning: {
        1: '2KI 20',
        2: 'MAT 17',
      },
      evening: {
        1: '2KI 21',
        2: '1CO 2',
      },
    },
    20: {
      morning: {
        1: '2KI 22',
        2: 'MAT 18',
      },
      evening: {
        1: '2KI 23',
        2: '1CO 3',
      },
    },
    21: {
      morning: {
        1: '2KI 24',
        2: 'MAT 19',
      },
      evening: {
        1: '2KI 25',
        2: '1CO 4',
      },
    },
    22: {
      morning: {
        1: 'EZR 1',
        2: 'MAT 20',
      },
      evening: {
        1: 'EZR 3',
        2: '1CO 5',
      },
    },
    23: {
      morning: {
        1: 'EZR 4',
        2: 'MAT 21',
      },
      evening: {
        1: 'EZR 5',
        2: '1CO 6',
      },
    },
    24: {
      morning: {
        1: 'EZR 6',
        2: 'MAT 22',
      },
      evening: {
        1: 'EZR 7',
        2: '1CO 7',
      },
    },
    25: {
      morning: {
        1: 'EZR 8',
        2: 'MAT 23',
      },
      evening: {
        1: 'NEH 1',
        2: '1CO 8',
      },
    },
    26: {
      morning: {
        1: 'NEH 2',
        2: 'MAT 24',
      },
      evening: {
        1: 'NEH 4',
        2: '1CO 9',
      },
    },
    27: {
      morning: {
        1: 'NEH 5',
        2: 'MAT 25',
      },
      evening: {
        1: 'NEH 6',
        2: '1CO 10',
      },
    },
    28: {
      morning: {
        1: 'NEH 8',
        2: 'MAT 26',
      },
      evening: {
        1: 'NEH 9',
        2: '1CO 11',
      },
    },
    29: {
      morning: {
        1: 'NEH 10',
        2: 'MAT 27',
      },
      evening: {
        1: 'NEH 13',
        2: '1CO 12',
      },
    },
    30: {
      morning: {
        1: 'EST 1',
        2: 'MAT 28',
      },
      evening: {
        1: 'EST 2',
        2: '1CO 13',
      },
    },
    31: {
      morning: {
        1: 'EST 3',
        2: 'MAR 1',
      },
      evening: {
        1: 'EST 4',
        2: '1CO 14',
      },
    },
  },
  jun: {
    1: {
      morning: {
        1: 'EST 5',
        2: 'MAR 2',
      },
      evening: {
        1: 'EST 6',
        2: '1CO 15',
      },
    },
    2: {
      morning: {
        1: 'EST 7',
        2: 'MAR 3',
      },
      evening: {
        1: 'EST 8',
        2: '1CO 16',
      },
    },
    3: {
      morning: {
        1: 'EST 9',
        2: 'MAR 4',
      },
      evening: {
        1: 'JOB 1',
        2: '2CO 1',
      },
    },
    4: {
      morning: {
        1: 'JOB 2',
        2: 'MAR 5',
      },
      evening: {
        1: 'JOB 3',
        2: '2CO 2',
      },
    },
    5: {
      morning: {
        1: 'JOB 4',
        2: 'MAR 6',
      },
      evening: {
        1: 'JOB 5',
        2: '2CO 3',
      },
    },
    6: {
      morning: {
        1: 'JOB 6',
        2: 'MAR 7',
      },
      evening: {
        1: 'JOB 7',
        2: '2CO 4',
      },
    },
    7: {
      morning: {
        1: 'JOB 8',
        2: 'MAR 8',
      },
      evening: {
        1: 'JOB 9',
        2: '2CO 5',
      },
    },
    8: {
      morning: {
        1: 'JOB 10',
        2: 'MAR 9',
      },
      evening: {
        1: 'JOB 11',
        2: '2CO 6',
      },
    },
    9: {
      morning: {
        1: 'JOB 12',
        2: 'MAR 10',
      },
      evening: {
        1: 'JOB 13',
        2: '2CO 7',
      },
    },
    10: {
      morning: {
        1: 'JOB 14',
        2: 'MAR 11',
      },
      evening: {
        1: 'JOB 15',
        2: '2CO 8',
      },
    },
    11: {
      special: "St. Barnabas, A. & M.",
      morning: {
        1: 'SIR 10',
        2: 'ACT 14',
      },
      evening: {
        1: 'SIR 14',
        2: 'ACT 15:1-35',
      },
    },
    12: {
      morning: {
        1: 'JOB 16',
        2: 'MAR 12',
      },
      evening: {
        1: 'JOB 17,JOB 18',
        2: '2CO 9',
      },
    },
    13: {
      morning: {
        1: 'JOB 19',
        2: 'MAR 13',
      },
      evening: {
        1: 'JOB 20',
        2: '2CO 10',
      },
    },
    14: {
      morning: {
        1: 'JOB 21',
        2: 'MAR 14',
      },
      evening: {
        1: 'JOB 22',
        2: '2CO 11',
      },
    },
    15: {
      morning: {
        1: 'JOB 23',
        2: 'MAR 15',
      },
      evening: {
        1: 'JOB 24,JOB 25',
        2: '2CO 12',
      },
    },
    16: {
      morning: {
        1: 'JOB 26,JOB 27',
        2: 'MAR 16',
      },
      evening: {
        1: 'JOB 28',
        2: '2CO 13',
      },
    },
    17: {
      morning: {
        1: 'JOB 29',
        2: 'LUK 1',
      },
      evening: {
        1: 'JOB 30',
        2: 'GAL 1',
      },
    },
    18: {
      morning: {
        1: 'JOB 31',
        2: 'LUK 2',
      },
      evening: {
        1: 'JOB 32',
        2: 'GAL 2',
      },
    },
    19: {
      morning: {
        1: 'JOB 33',
        2: 'LUK 3',
      },
      evening: {
        1: 'JOB 34',
        2: 'GAL 3',
      },
    },
    20: {
      morning: {
        1: 'JOB 35',
        2: 'LUK 4',
      },
      evening: {
        1: 'JOB 36',
        2: 'GAL 4',
      },
    },
    21: {
      morning: {
        1: 'JOB 37',
        2: 'LUK 5',
      },
      evening: {
        1: 'JOB 38',
        2: 'GAL 5',
      },
    },
    22: {
      morning: {
        1: 'JOB 39',
        2: 'LUK 6',
      },
      evening: {
        1: 'JOB 40',
        2: 'GAL 6',
      },
    },
    23: {
      morning: {
        1: 'JOB 41',
        2: 'LUK 7',
      },
      evening: {
        1: 'JOB 42',
        2: 'EPH 1',
      },
    },
    24: {
      special: "Nativity of St. John the Baptist",
      morning: {
        1: 'MAL 3',
        2: 'MATT 3',
      },
      evening: {
        1: 'MAL 4',
        2: 'MAT 14:1-12',
      },
    },
    25: {
      morning: {
        1: 'PRO 1',
        2: 'LUK 8',
      },
      evening: {
        1: 'PRO 2',
        2: 'EPH 2',
      },
    },
    26: {
      morning: {
        1: 'PRO 3',
        2: 'LUK 9',
      },
      evening: {
        1: 'PRO 4',
        2: 'EPH 3',
      },
    },
    27: {
      morning: {
        1: 'PRO 5',
        2: 'LUK 10',
      },
      evening: {
        1: 'PRO 6',
        2: 'EPH 4',
      },
    },
    28: {
      morning: {
        1: 'PRO 7',
        2: 'LUK 11',
      },
      evening: {
        1: 'PRO 8',
        2: 'EPH 5',
      },
    },
    29: {
      special: "St. Peter, A. & M.",
      morning: {
        1: 'SIR 15',
        2: 'ACT 3',
      },
      evening: {
        1: 'SIR 19',
        2: 'ACT 4',
      },
    },
    30: {
      morning: {
        1: 'PRO 9',
        2: 'LUK 12',
      },
      evening: {
        1: 'PRO 10',
        2: 'EPH 6',
      },
    },
  },
  jul: {
    1: {
      morning: {
        1: 'PRO 11',
        2: 'LUK 13',
      },
      evening: {
        1: 'PRO 12',
        2: 'PHI 1',
      },
    },
    2: {
      morning: {
        1: 'PRO 13',
        2: 'LUK 14',
      },
      evening: {
        1: 'PRO 14',
        2: 'PHI 2',
      },
    },
    3: {
      morning: {
        1: 'PRO 15',
        2: 'LUK 15',
      },
      evening: {
        1: 'PRO 16',
        2: 'PHI 3',
      },
    },
    4: {
      morning: {
        1: 'PRO 17',
        2: 'LUK 16',
      },
      evening: {
        1: 'PRO 18',
        2: 'PHI 4',
      },
    },
    5: {
      morning: {
        1: 'PRO 19',
        2: 'LUK 17',
      },
      evening: {
        1: 'PRO 20',
        2: 'COL 1',
      },
    },
    6: {
      morning: {
        1: 'PRO 21',
        2: 'LUK 18',
      },
      evening: {
        1: 'PRO 22',
        2: 'COL 2',
      },
    },
    7: {
      morning: {
        1: 'PRO 23',
        2: 'LUK 19',
      },
      evening: {
        1: 'PRO 24',
        2: 'COL 3',
      },
    },
    8: {
      morning: {
        1: 'PRO 25',
        2: 'LUK 20',
      },
      evening: {
        1: 'PRO 26',
        2: 'COL 4',
      },
    },
    9: {
      morning: {
        1: 'PRO 27',
        2: 'LUK 21',
      },
      evening: {
        1: 'PRO 28',
        2: '1TH 1',
      },
    },
    10: {
      morning: {
        1: 'PRO 29',
        2: 'LUK 22',
      },
      evening: {
        1: 'PRO 31',
        2: '1TH 2',
      },
    },
    11: {
      morning: {
        1: 'ECC 1',
        2: 'LUK 23',
      },
      evening: {
        1: 'ECC 2',
        2: '1TH 3',
      },
    },
    12: {
      morning: {
        1: 'ECC 3',
        2: 'LUK 24',
      },
      evening: {
        1: 'ECC 4',
        2: '1TH 4',
      },
    },
    13: {
      morning: {
        1: 'ECC 5',
        2: 'JOH 1',
      },
      evening: {
        1: 'ECC 6',
        2: '1TH 5',
      },
    },
    14: {
      morning: {
        1: 'ECC 7',
        2: 'JOH 2',
      },
      evening: {
        1: 'ECC 8',
        2: '2TH 1',
      },
    },
    15: {
      morning: {
        1: 'ECC 9',
        2: 'JOH 3',
      },
      evening: {
        1: 'ECC 10',
        2: '2TH 2',
      },
    },
    16: {
      morning: {
        1: 'ECC 11',
        2: 'JOH 4',
      },
      evening: {
        1: 'ECC 12',
        2: '2TH 3',
      },
    },
    17: {
      morning: {
        1: 'JER 1',
        2: 'JOH 5',
      },
      evening: {
        1: 'JER 2',
        2: '1TI 1',
      },
    },
    18: {
      morning: {
        1: 'JER 3',
        2: 'JOH 6',
      },
      evening: {
        1: 'JER 4',
        2: '1TI 2,TI 3',
      },
    },
    19: {
      morning: {
        1: 'JER 5',
        2: 'JOH 7',
      },
      evening: {
        1: 'JER 6',
        2: '1TI 4',
      },
    },
    20: {
      morning: {
        1: 'JER 7',
        2: 'JOH 8',
      },
      evening: {
        1: 'JER 8',
        2: '1TI 5',
      },
    },
    21: {
      morning: {
        1: 'JER 9',
        2: 'JOH 9',
      },
      evening: {
        1: 'JER 10',
        2: '1TI 6',
      },
    },
    22: {
      morning: {
        1: 'JER 11',
        2: 'JOH 10',
      },
      evening: {
        1: 'JER 12',
        2: '2TI 1',
      },
    },
    23: {
      morning: {
        1: 'JER 13',
        2: 'JOH 11',
      },
      evening: {
        1: 'JER 14',
        2: '2TI 2',
      },
    },
    24: {
      morning: {
        1: 'JER 15',
        2: 'JOH 12',
      },
      evening: {
        1: 'JER 16',
        2: '2TI 3',
      },
    },
    25: {
      special: "St. James, A. & M.",
      morning: {
        1: 'SIR 21',
        2: '',
      },
      evening: {
        1: 'SIR 22',
        2: '',
      },
    },
    26: {
      morning: {
        1: 'JER 17',
        2: 'JOH 14',
      },
      evening: {
        1: 'JER 18',
        2: 'TIT 1',
      },
    },
    27: {
      morning: {
        1: 'JER 19',
        2: 'JOH 15',
      },
      evening: {
        1: 'JER 20',
        2: 'TIT 2,TIT 3',
      },
    },
    28: {
      morning: {
        1: 'JER 21',
        2: 'JOH 16',
      },
      evening: {
        1: 'JER 22',
        2: 'PHM',
      },
    },
    29: {
      morning: {
        1: 'JER 23',
        2: 'JOH 17',
      },
      evening: {
        1: 'JER 24',
        2: 'HEB 1',
      },
    },
    30: {
      morning: {
        1: 'JER 25',
        2: 'JOH 18',
      },
      evening: {
        1: 'JER 26',
        2: 'HEB 2',
      },
    },
    31: {
      morning: {
        1: 'JER 27',
        2: 'JOH 19',
      },
      evening: {
        1: 'JER 28',
        2: 'HEB 3',
      },
    },
  },
  aug: {
    1: {
      morning: {
        1: 'JER 29',
        2: 'JOH 20',
      },
      evening: {
        1: 'JER 30',
        2: 'HEB 4',
      },
    },
    2: {
      morning: {
        1: 'JER 31',
        2: 'JOH 21',
      },
      evening: {
        1: 'JER 32',
        2: 'HEB 5',
      },
    },
    3: {
      morning: {
        1: 'JER 33',
        2: 'ACT 1',
      },
      evening: {
        1: 'JER 34',
        2: 'HEB 6',
      },
    },
    4: {
      morning: {
        1: 'JER 35',
        2: 'ACT 2',
      },
      evening: {
        1: 'JER 36',
        2: 'HEB 7',
      },
    },
    5: {
      morning: {
        1: 'JER 37',
        2: 'ACT 3',
      },
      evening: {
        1: 'JER 38',
        2: 'HEB 8',
      },
    },
    6: {
      morning: {
        1: 'JER 39',
        2: 'ACT 4',
      },
      evening: {
        1: 'JER 40',
        2: 'HEB 9',
      },
    },
    7: {
      morning: {
        1: 'JER 41',
        2: 'ACT 5',
      },
      evening: {
        1: 'JER 42',
        2: 'HEB 10',
      },
    },
    8: {
      morning: {
        1: 'JER 43',
        2: 'ACT 6',
      },
      evening: {
        1: 'JER 44',
        2: 'HEB 11',
      },
    },
    9: {
      morning: {
        1: 'JER 45,JER 46',
        2: 'ACT 7',
      },
      evening: {
        1: 'JER 47',
        2: 'HEB 12',
      },
    },
    10: {
      morning: {
        1: 'JER 48',
        2: 'ACT 8',
      },
      evening: {
        1: 'JER 49',
        2: 'HEB 13',
      },
    },
    11: {
      morning: {
        1: 'JER 50',
        2: 'ACT 9',
      },
      evening: {
        1: 'JER 51',
        2: 'JAM 1',
      },
    },
    12: {
      morning: {
        1: 'JER 52',
        2: 'ACT 10',
      },
      evening: {
        1: 'LAM 1',
        2: 'JAM 2',
      },
    },
    13: {
      morning: {
        1: 'LAM 2',
        2: 'ACT 11',
      },
      evening: {
        1: 'LAM 3',
        2: 'JAM 3',
      },
    },
    14: {
      morning: {
        1: 'LAM 4',
        2: 'ACT 12',
      },
      evening: {
        1: 'LAM 5',
        2: 'JAM 4',
      },
    },
    15: {
      morning: {
        1: 'EZE 2',
        2: 'ACT 13',
      },
      evening: {
        1: 'EZE 3',
        2: 'JAM 5',
      },
    },
    16: {
      morning: {
        1: 'EZE 6',
        2: 'ACT 14',
      },
      evening: {
        1: 'EZE 7',
        2: '1PE 1',
      },
    },
    17: {
      morning: {
        1: 'EZE 13',
        2: 'ACT 15',
      },
      evening: {
        1: 'EZE 14',
        2: '1PE 2',
      },
    },
    18: {
      morning: {
        1: 'EZE 18',
        2: 'ACT 16',
      },
      evening: {
        1: 'EZE 33',
        2: '1PE 3',
      },
    },
    19: {
      morning: {
        1: 'EZE 34',
        2: 'ACT 17',
      },
      evening: {
        1: 'DAN 1',
        2: '1PE 4',
      },
    },
    20: {
      morning: {
        1: 'DAN 2',
        2: 'ACT 18',
      },
      evening: {
        1: 'DAN 3',
        2: '1PE 5',
      },
    },
    21: {
      morning: {
        1: 'DAN 4',
        2: 'ACT 19',
      },
      evening: {
        1: 'DAN 5',
        2: '2PE 1',
      },
    },
    22: {
      morning: {
        1: 'DAN 6',
        2: 'ACT 20',
      },
      evening: {
        1: 'DAN 7',
        2: '2PE 2',
      },
    },
    23: {
      morning: {
        1: 'DAN 8',
        2: 'ACT 21',
      },
      evening: {
        1: 'DAN 9',
        2: '2PE 3',
      },
    },
    24: {
      morning: {
        1: 'SIR 24',
        2: 'ACT 22',
      },
      evening: {
        1: 'SIR 29',
        2: '1JO 1',
      },
    },
    25: {
      morning: {
        1: 'DAN 10',
        2: 'ACT 23',
      },
      evening: {
        1: 'DAN 11',
        2: '1JO 2',
      },
    },
    26: {
      morning: {
        1: 'DAN 12',
        2: 'ACT 24',
      },
      evening: {
        1: 'HOS 1',
        2: '1JO 3',
      },
    },
    27: {
      morning: {
        1: 'HOS 2,HOS 3',
        2: 'ACT 25',
      },
      evening: {
        1: 'HOS 4',
        2: '1JO 4',
      },
    },
    28: {
      morning: {
        1: 'HOS 5,HOS 6',
        2: 'ACT 26',
      },
      evening: {
        1: 'HOS 7',
        2: '1JO 5',
      },
    },
    29: {
      morning: {
        1: 'HOS 8',
        2: 'ACT 27',
      },
      evening: {
        1: 'HOS 9',
        2: '2JO,3JO',
      },
    },
    30: {
      morning: {
        1: 'HOS 10',
        2: 'ACT 28',
      },
      evening: {
        1: 'HOS 11',
        2: 'JUD',
      },
    },
    31: {
      morning: {
        1: 'HOS 12',
        2: 'MAT 1',
      },
      evening: {
        1: 'HOS 13',
        2: 'ROM 1',
      },
    },
  },
  sep: {
    1: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    2: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    3: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    4: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    5: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    6: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    7: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    8: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    9: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    10: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    11: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    12: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    13: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    14: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    15: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    16: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    17: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    18: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    19: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    20: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    21: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    22: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    23: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    24: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    25: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    26: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    27: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    28: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    29: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    30: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
  },
  oct: {
    1: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    2: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    3: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    4: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    5: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    6: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    7: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    8: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    9: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    10: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    11: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    12: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    13: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    14: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    15: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    16: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    17: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    18: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    19: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    20: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    21: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    22: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    23: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    24: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    25: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    26: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    27: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    28: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    29: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    30: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    31: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
  },
  nov: {
    1: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    2: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    3: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    4: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    5: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    6: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    7: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    8: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    9: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    10: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    11: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    12: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    13: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    14: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    15: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    16: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    17: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    18: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    19: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    20: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    21: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    22: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    23: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    24: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    25: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    26: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    27: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    28: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    29: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    30: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
  },
  dec: {
    1: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    2: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    3: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    4: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    5: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    6: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    7: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    8: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    9: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    10: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    11: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    12: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    13: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    14: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    15: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    16: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    17: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    18: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    19: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    20: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    21: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    22: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    23: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    24: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    25: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    26: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    27: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    28: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    29: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    30: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
    31: {
      morning: {
        1: '',
        2: '',
      },
      evening: {
        1: '',
        2: '',
      },
    },
  },
};
