export const BOOL = 'BOOL';
export const ENUM = 'ENUM';
export const INTRO_RANDOM = 'INTRO_RANDOM';
export const INTRO_ALL = 'INTRO_ALL';
export const MODE_AUTO = 'MODE_AUTO';
export const MODE_DARK = 'MODE_DARK';
export const MODE_LIGHT = 'MODE_LIGHT';
export const LS_KEY = '1662DO.settings';

export const schema = {
  includePriest: {
    default: true,
    type: BOOL,
  },
  
  alone: {
    default: false,
    type: BOOL,
  },

  introSentences: {
    default: INTRO_RANDOM,
    type: ENUM,
    options: [INTRO_RANDOM, INTRO_ALL],
  },

  mode: {
    default: MODE_AUTO,
    type: ENUM,
    options: [MODE_AUTO, MODE_DARK, MODE_LIGHT],
  },

  fontSize: {
    default: 14,
    type: ENUM,
    options: [14,16,18,20,22,24,26,28,30],
  },

  royalPrayers: {
    default: true,
    type: BOOL,
  },

  governmentPrayers: {
    default: false,
    type: BOOL,
  },

  autoReload: {
    default: true,
    type: BOOL,
  },

  eveningStartsAt: {
    default: 16,
  }
};

export function getSettings() {
  const mySettings = JSON.parse(localStorage.getItem(LS_KEY)) || { usingDefaultSettings: true };
  return { ...getDefaultSettings(), ...mySettings };
}

export function setSettings(settings) {
  settings.usingDefaultSettings = false;
  const json = JSON.stringify(settings);
  localStorage.setItem(LS_KEY, json);
  document.cookie = `${LS_KEY}=${json}`;
}

export function getDefaultSettings() {
  const settings = {
    usingDefaultSettings: true,
  };
  Object.keys(schema).forEach((key) => settings[key] = schema[key].default);
  return settings;
}

export default {
  schema,
  getSettings,
  setSettings,
  getDefaultSettings,
};
