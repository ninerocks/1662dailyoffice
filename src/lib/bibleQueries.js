import bible from '../data/kjv+apocrypha.json';
import dailyPsalms from './dailyPsalms';
import lessons from './lessons';
import { getMonth } from './utils';

const MORNING = 'morning';
const EVENING = 'evening';

export function getMorningPsalms(date) {
  return getPsalms(date, MORNING);
}

export function getEveningPsalms(date) {
  return getPsalms(date, EVENING);
}

export function getPsalms(date, which) {
  const day = date.getDate();
  const refs = dailyPsalms[day][which];
  return refs.map((ref) => getScripture(`PSA ${ref}`));
}

export function getScripture(ref) {
  console.log(ref);
  if (!ref) {
    return null;
  }
  const parts = ref.split(',').map(p => p.trim());
  const scripture = parts.map((part) => {
    const match = part.match(/([A-Z1-3]{3})(\s+(\d+)(:(.+))?)?/);
    if (!match) {
      throw new Error(`Invalid reference given: ${ref}`);
    }
    const [,bookKey,,chapter,,versesRef] = match;
    const verses = getVerses(bible[bookKey].chapter[chapter || 1].verse, versesRef);
    return {
      book: bookMap[bookKey],
      chapter,
      versesRef,
      verses,
    };
  });

  // if (scripture.length === 1) return scripture[0];
  return scripture;
}

function getVerses(data, ref) {
  if (!ref) {
    return Object.entries(data).map(([num, text]) => ({ num, text }));
  }

  let [start, finish] = ref.split('-').map(r => r.trim());
  if (!finish) finish = start;
  return Array.from({length: (finish + 1) - start}, (x, i) => i + start).map((v) => ({ num: v, text: data[v] }));
}

export function getMorningLessons(date) {
  return getLessons(date, MORNING);
}

export function getEveningLessons(date) {
  return getLessons(date, EVENING);
}

export function getLessons(date, which) {
  const day = date.getDate();
  const month = getMonth(date);
  const lesson1 = getScripture(lessons[month][day][which]['1'])[0];
  const lesson2 = getScripture(lessons[month][day][which]['2'])[0];
  console.log("lesson 1", lesson1);
  console.log("lesson 2", lesson2);
  return {
    lesson1,
    lesson2,
  };
}

export const bookMap = {
  GEN: 'Genesis',
  EXO: 'Exodus',
  LEV: 'Leviticus',
  NUM: 'Numbers',
  DEU: 'Deuteronomy' ,
  JOS: 'Joshua',
  JDG: 'Judges',
  RUT: 'Ruth',
  '1SA': '1 Samuel',
  '2SA': '2 Samuel',
  '1KI': '1 Kings',
  '2KI': '2 Kings',
  '1CH': '1 Chronicles',
  '2CH': '2 Chronicles',
  EZR: 'Ezra',
  NEH: 'Nehemiah',
  EST: 'Esther',
  JOB: 'Job',
  PSA: 'Psalms',
  PRO: 'Proverbs',
  ECC: 'Ecclesiastes',
  SOL: 'Song of Solomon',
  ISA: 'Isaiah',
  JER: 'Jeremiah',
  LAM: 'Lamentations',
  EZE: 'Ezekiel',
  DAN: 'Daniel',
  HOS: 'Hosea',
  JOE: 'Joel',
  AMO: 'Amos',
  OBA: 'Obediah',
  JON: 'Jonah',
  MIC: 'Micah',
  NAH: 'Nahum',
  HAB: 'Habakkuk',
  ZEP: 'Zephaniah',
  HAG: 'Haggai',
  ZEC: 'Zechariah',
  MAL: 'Malachi',
  TOB: 'Tobit',
  JDT: 'Judith',
  ESG: 'Additions to Esther',
  WIS: 'Wisdom of Solomon',
  SIR: 'Sirach',
  BAR: 'Baruch',
  PRA: 'Prayer of Azariah',
  SUS: 'Susanna',
  BEL: 'Bel and the Dragon',
  '1MA': '1 Maccabees',
  '2MA': '2 Maccabees',
  '1ES': '1 Esdras',
  '2ES': '2 Esdras',
  PRM: 'The Prayer of Manasseh',
  MAT: 'Matthew',
  MAR: 'Mark',
  LUK: 'Luke',
  JOH: 'John',
  ACT: 'Acts',
  ROM: 'Romans',
  '1CO': '1 Corinthians',
  '2CO': '2 Corinthians',
  GAL: 'Galations',
  EPH: 'Ephesians',
  PHI: 'Philippians',
  COL: 'Colossians',
  '1TH': '1 Thessalonians',
  '2TH': '2 Thessalonians',
  '1TI': '1 Timothy',
  '2TI': '2 Timothy',
  TIT: 'Titus',
  PHM: 'Philemon',
  HEB: 'Hebrews',
  JAM: 'James',
  '1PE': '1 Peter',
  '2PE': '2 Peter',
  '1JO': '1 John',
  '2JO': '2 John',
  '3JO': '3 John',
  JUD: 'Jude',
  REV: 'Revelation',
};
