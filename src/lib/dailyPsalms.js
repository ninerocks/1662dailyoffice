export default {
  1: {
    morning: [1,2,3,4,5],
    evening: [6,7,8],
  },
  2: {
    morning: [9,10,11],
    evening: [12,13,14],
  },
  3: {
    morning: [15,16,17],
    evening: [18],
  },
  4: {
    morning: [19,20,21],
    evening: [22,23],
  },
  5: {
    morning: [24,25,26],
    evening: [27,28,29],
  },
  6: {
    morning: [30,31],
    evening: [32,33,34],
  },
  7: {
    morning: [35,36],
    evening: [37],
  },
  8: {
    morning: [38,39,40],
    evening: [41,42,43],
  },
  9: {
    morning: [44,45,46],
    evening: [47,48,49],
  },
  10: {
    morning: [50,51,52],
    evening: [53,54,55],
  },
  11: {
    morning: [56,57,58],
    evening: [59,60,61],
  },
  12: {
    morning: [62,63,64],
    evening: [65,66,67],
  },
  13: {
    morning: [68],
    evening: [69,70],
  },
  14: {
    morning: [71,72],
    evening: [73,74],
  },
  15: {
    morning: [75,76,77],
    evening: [78],
  },
  16: {
    morning: [79,80,81],
    evening: [82,83,84,85],
  },
  17: {
    morning: [86,87,88],
    evening: [89],
  },
  18: {
    morning: [90,91,92],
    evening: [93,94],
  },
  19: {
    morning: [95,96,97],
    evening: [98,99,100,101],
  },
  20: {
    morning: [102,103],
    evening: [104],
  },
  21: {
    morning: [105],
    evening: [106],
  },
  22: {
    morning: [107],
    evening: [108,109],
  },
  23: {
    morning: [110,111,112,113],
    evening: [114,115],
  },
  24: {
    morning: [116,117,118],
    evening: ['119:1-32'],
  },
  25: {
    morning: ['119:32-72'],
    evening: ['119:73-104'],
  },
  26: {
    morning: ['119:105-144'],
    evening: ['119:145-176'],
  },
  27: {
    morning: [120,121,122,123,124,125],
    evening: [126,127,128,129,130,131],
  },
  28: {
    morning: [132,133,134,135],
    evening: [136,137,138],
  },
  29: {
    morning: [139,140,141],
    evening: [142,143],
  },
  30: {
    morning: [144,145,146],
    evening: [147,148,149,150],
  },
};
