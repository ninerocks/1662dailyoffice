import defaultSettings, { getSettings, setSettings, INTRO_RANDOM } from './settings';

let reloadInterval;

export function initUI() {
  window.x_dateLoaded = new Date();
  const settings = getSettings();
  
	if (settings.usingDefaultSettings) {
		document.getElementById('first-visit-blurb').style.display = 'block';
	}
  
  const sentences = document.querySelectorAll('#intro-sentences p');
  if (settings.introSentences === INTRO_RANDOM) {
    const index = Math.floor(Math.random() * sentences.length);
    sentences[index].style.display = 'block';
  } else {
    sentences.forEach(el => el.style.display = 'block');
  }
  
  if (!settings.alone) {
    document.querySelectorAll('.hide-when-alone:not(.priest-only)').forEach(el => el.style.display = 'block');
  }

  if (settings.includePriest) {
    document.querySelectorAll('.priest-only').forEach(el => el.style.display = 'block');
  }

  if (!settings.royalPrayers) {
    document.querySelectorAll('.royal-prayers').forEach(el => el.style.display = 'none');
  }

  if (!settings.governmentPrayers) {
    document.querySelectorAll('.gov-prayers').forEach(el => el.style.display = 'none');
  }

  if (settings.autoReload && !reloadInterval) {
    // Set a timer to check every hour if we need to display a new daily office
    console.log('creating auto-reload interval');
    reloadInterval = window.setInterval(() => {
      console.log('running auto-reload check');
      const now = new Date();
      if (
        now.getFullYear() !== window.x_dateLoaded.getFullYear() ||
        now.getMonth() !== window.x_dateLoaded.getMonth() ||
        now.getDate() !== window.x_dateLoaded.getDate() ||
        (window.x_dateLoaded.getHours() < settings.eveningStartsAt && now.getHours() >= settings.eveningStartsAt)) {
          console.log('triggering auto-reload');
          loadDailyOffice();
          return;
      }
      console.log('auto-reload not required');
    }, 60_000);
  }
}

export function loadDailyOffice(forDate = null) {
  const settings = getSettings();
  const date = forDate ? new Date(forDate) : new Date();
  console.log('loading daily office for ', date);
  const which = date.getHours() < settings.eveningStartsAt ? 'morning' : 'evening';
  const url = `/${which}/${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;
  window.location.href = url;
}

export function formatVerse(verse) {
  let line = `<span><sub class="verse-num" aria-hidden="true">${verse.num}</sub> ${verse.text.replaceAll(/\[([a-z ]+)\]/gi, '<em>$1</em>')}&nbsp;</span>`;
  line = line.replace(/(^.+?)¶/, "</p><p>$1");
  return line;
}

const months = [
  'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'
];
export function getMonth(date) {
  return months[date.getMonth()];
}