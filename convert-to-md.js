import { open } from 'node:fs/promises';

const map = {
  GEN: 'Genesis',
  EXO: 'Exodus',
  LEV: 'Leviticus',
  NUM: 'Numbers',
  DEU: 'Deuteronomy' ,
  JOS: 'Joshua',
  JDG: 'Judges',
  RUT: 'Ruth',
  '1SA': '1 Samuel',
  '2SA': '2 Samuel',
  '1KI': '1 Kings',
  '2KI': '2 Kings',
  '1CH': '1 Chronicles',
  '2CH': '2 Chronicles',
  EZR: 'Ezra',
  NEH: 'Nehemiah',
  EST: 'Esther',
  JOB: 'Job',
  PSA: 'Psalms',
  PRO: 'Proverbs',
  ECC: 'Ecclesiastes',
  SOL: 'Song of Solomon',
  ISA: 'Isaiah',
  JER: 'Jeremiah',
  LAM: 'Lamentations',
  EZE: 'Ezekiel',
  DAN: 'Daniel',
  HOS: 'Hosea',
  JOE: 'Joel',
  AMO: 'Amos',
  OBA: 'Obediah',
  JON: 'Jonah',
  MIC: 'Micah',
  NAH: 'Nahum',
  HAB: 'Habakkuk',
  ZEP: 'Zephaniah',
  HAG: 'Haggai',
  ZEC: 'Zechariah',
  MAL: 'Malachi',
  TOB: 'Tobit',
  JDT: 'Judith',
  ESG: 'Additions to Esther',
  WIS: 'Wisdom of Solomon',
  SIR: 'Sirach',
  BAR: 'Baruch',
  PRA: 'Prayer of Azariah',
  SUS: 'Susanna',
  BEL: 'Bel and the Dragon',
  '1MA': '1 Maccabees',
  '2MA': '2 Maccabees',
  '1ES': '1 Esdras',
  '2ES': '2 Esdras',
  PRM: 'The Prayer of Manasseh',
  MAT: 'Matthew',
  MAR: 'Mark',
  LUK: 'Luke',
  JOH: 'John',
  ACT: 'Acts',
  ROM: 'Romans',
  '1CO': '1 Corinthians',
  '2CO': '2 Corinthians',
  GAL: 'Galations',
  EPH: 'Ephesians',
  PHI: 'Philippians',
  COL: 'Colossians',
  '1TH': '1 Thessalonians',
  '2TH': '2 Thessalonians',
  '1TI': '1 Timothy',
  '2TI': '2 Timothy',
  TIT: 'Titus',
  PHM: 'Philemon',
  HEB: 'Hebrews',
  JAM: 'James',
  '1PE': '1 Peter',
  '2PE': '2 Peter',
  '1JO': '1 John',
  '2JO': '2 John',
  '3JO': '3 John',
  JUD: 'Jude',
  REV: 'Revelation',
};

// const file = await open('./src/data/eng-kjv_vpl.txt');

// convertToMd();
// convertToJson();
convertCoverdaleToMd();

async function convertCoverdaleToMd() {
  const file = await open('./src/data/coverdale-psalms.txt');

  let fd;

  for await (let line of file.readLines()) {
    if (line.length > 1) {
      const matches = line.match(/^Day \d+\./);
      if (matches) {
        if (fd) {
          await fd.close();
        }
        const name = `${line.toLowerCase().replace(/\.$/, '').replaceAll(/[\s.]+/g, '-')}.md`;
        fd = await open(`./src/content/psalms/${name}`, 'wx');
        continue;
      }
      
      const matches2 = line.match(/^(Psalm \d+)(.+)/);
      if (matches2) {
        line = `#### *${matches2[0]}*`;
      } else if (line.indexOf(' : ') > 0) {
        line = line.replace(' : ', '<span> * </span>').replace(/^(\d+)./, '<sub>$1</sub>') + '\n';
      } else {
        line = `#### *${line}*`
      }
    }

    if (fd) await fd.write(`${line}\n`);
  }

  if (fd) fd.close();
}

async function convertToJson() {
  let currentBook;
  let currentChapter;

  const json = {};

  for await (const line of file.readLines()) {
    const matches = line.match(/^([a-z0-9]+) (\d+):(\d+) (.+)/i);
    if (matches) {
      const [, book, chapter, verse, text] = matches;
      
      if (currentBook !== book) {
        json[book] = {
          name: map[book],
          chapter: {},
        }

        currentBook = book;
      }

      if (!json[book].chapter[chapter]) json[book].chapter[chapter] = {
        verse: {},
      };

      // let line = `<sub>${verse}</sub> ${text.replaceAll(/\[([a-z ]+)\]/gi, '<em>$1</em>')}`;
      // line = line.replace(/([^\s]+)\s+¶/, "\n\n$1");

      json[book].chapter[chapter].verse[verse] = text;
    } else {
      console.log('no match: ', line);
    }
  }

  const fd = await open(`./src/data/kjv+apocrypha.json`, 'wx');
  fd.write(`${JSON.stringify(json)}\n`);
}

async function convertToMd() {
  let newFile;
  let currentName = '';

  for await (const line of file.readLines()) {
    const matches = line.match(/^([a-z0-9]+) (\d+):(\d+) (.+)/i);
    if (matches) {
      const [, book, chapter, verse, text] = matches;
      const name = `${map[book].replace(' ', '-')}_ch${chapter}.md`;
      if (newFile && currentName !== name) {
        await newFile.close();
        newFile = undefined;
      }

      if (!newFile) {
        currentName = name;
        newFile = await open(`./KJV/${name}`, 'wx');
        await newFile.write(`## ${map[book]}\n### Chapter ${chapter}\n`);
      }
      
      let line = `<sub>${verse}</sub> ${text.replaceAll(/\[([a-z ]+)\]/gi, '*$1*')}`;
      line = line.replace(/([^\s]+)\s+¶/, "\n\n$1");
      await newFile.write(`${line}\n`);
    } else {
      console.log('no match: ', line);
    }
  }
}

